<?php
session_start();
require_once('../setup/connect.php');
require_once("../../assets/libs/BrowserDetection/lib/BrowserDetection.php");

error_reporting(E_ALL);
ini_set('display_errors','On');
if($_SERVER['REQUEST_METHOD'] == 'POST')
{
  //start sql
  $sql = "SELECT * FROM staff_users WHERE Email='".$_SESSION['user_email']."' && status='active'";
     if($query = mysqli_query($dbc,$sql))
     {
         if(mysqli_num_rows($query) > 0)
         {
             while($row = mysqli_fetch_array($query))
             {
                 $dep_name = mysqli_fetch_array(mysqli_query($dbc,"SELECT * FROM departments WHERE department_id='".$row['DepartmentCode']."'"));
                 $_SESSION['email'] = $row['Email'];
                 $_SESSION['staff_id'] = $row['EmpNo'];
                 $_SESSION['name'] = $row['Name'];
                 $_SESSION['department'] = $dep_name['department_name'];
                 $_SESSION['department_code'] = $row['DepartmentCode'];
                 $_SESSION['designation'] = $row['designation'];
                 $_SESSION['access_level'] = $row['access_level'];


                 $time_signed_in  = date('Y/m/d H:i:s');
                 $_SESSION['time_signed_in'] = $time_signed_in;
                 $ip_address = file_get_contents('https://api.ipify.org');
                 $date_recorded = date('Y/m/d');


                 //insert into sign in logs

                 $sql_insert = "INSERT INTO sign_in_logs(email,name,time_signed_in,ip_address,date_recorded)
                                 VALUES

                                 ('".$_SESSION['email']."','".$_SESSION['name']."','".$_SESSION['time_signed_in']."','".$ip_address."','".$date_recorded."')
                                 ";
                 mysqli_query($dbc,$sql_insert) or die(exit("failed"));

                 //insert into activity logs
                 $browser = new Wolfcast\BrowserDetection();
                 $browser_name = $browser->getName();
                 $platform_name = $browser->getPlatformVersion();

                 $action_reference = "Logged into the system on " . $platform_name. " using ". $browser_name;
                 $action_name = "Logged in";
                 $action_icon = "far fa-sign-in text-success";
                 $page_id = "login-link";
                 $time_recorded = $time_signed_in;

                  $sql_log = mysqli_query($dbc,"INSERT INTO activity_logs
                               (email,action_name,action_reference,action_icon,page_id,time_recorded)
                         VALUES
                         ('".$_SESSION['email']."','".$action_name."','".$action_reference."',
                         '".$action_icon."','".$page_id."','".$time_recorded."')"
                  );

             }
             exit('success');
         }
         else
         {

             exit('invalid');
         }
     }
  //end sql
}


?>
