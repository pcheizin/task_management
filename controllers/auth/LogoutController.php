<?php
session_start();
require_once('../setup/connect.php');
$time_signed_out  = date('Y/m/d H:i:s');
$sql = "UPDATE sign_in_logs
            SET
            time_signed_out='".$time_signed_out."'
            WHERE email='".$_SESSION['email']."'
            && time_signed_in='".$_SESSION['time_signed_in']."'";


if(isset($_SESSION['test_user']) == true)
{


  $query = mysqli_query($dbc,$sql);
  if($query)
  {
      if(session_destroy())
      {
        echo "success";
      }
      else
      {
        echo("failed");
      }

  }
  else
  {
      echo mysqli_error($dbc);
  }
}
else
{
  //insert into activity logs
  $action_reference = "Logged out of the system";
  $action_name = "Logged out";
  $action_icon = "far fa-sign-out text-danger";
  $page_id = "log-out-link";
  $time_recorded = $time_signed_out;

  $sql_log = mysqli_query($dbc,"INSERT INTO activity_logs
                  (email,action_name,action_reference,action_icon,page_id,time_recorded)
                      VALUES
              ('".$_SESSION['email']."','".$action_name."','".$action_reference."',
                      '".$action_icon."','".$page_id."','".$time_recorded."')"
               );

  $query = mysqli_query($dbc,$sql);
  if($query && $sql_log)
  {

    // Unset all of the session variables.
    $_SESSION = array();

    // If it's desired to kill the session, also delete the session cookie.
    // Note: This will destroy the session, and not just the session data!
    if (ini_get("session.use_cookies")) {
        $params = session_get_cookie_params();
        setcookie(session_name(), '', time() - 42000,
            $params["path"], $params["domain"],
            $params["secure"], $params["httponly"]
        );
    }

    // Finally, destroy the session.
      if(session_destroy())
      {
        echo "success";
      }
      else
      {
        echo("failed");
      }

  }
  else
  {
      echo mysqli_error($dbc);
  }
}



?>
