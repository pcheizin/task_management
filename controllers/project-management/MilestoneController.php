<?php
require_once('../setup/connect.php');
session_start();

if($_SERVER['REQUEST_METHOD'] == 'POST')
{

  //if its milestone creation
  if(isset($_POST['add-milestone']))
  {
  //  $department= mysqli_real_escape_string($dbc,strip_tags($_POST['department_name']));
    $milestone_name = mysqli_real_escape_string($dbc,strip_tags($_POST['milestone_name']));
    $start_date = mysqli_real_escape_string($dbc,strip_tags($_POST['start_date']));
    $end_date= mysqli_real_escape_string($dbc,strip_tags($_POST['end_date']));
    $duration = mysqli_real_escape_string($dbc,strip_tags($_POST['duration']));

    $recorded_by = $_SESSION['name'];

      //insert records to pm_milestones

      /* set autocommit to off */
      mysqli_autocommit($dbc, FALSE);
      $sql_insert = "
                    INSERT INTO pm_milestones
                            (milestone_name,start_date,end_date,duration,recorded_by)
                    VALUES
                            ( '".$milestone_name."', '".$start_date."', '".$end_date."',
                            '".$duration."', '".$recorded_by."')
                  ";

      $insert_project = mysqli_query($dbc,$sql_insert);

    //log the action
    $action_reference = "Added a Milestone for the project with id  milestone name: ".$milestone_name;
    $action_name = "Milestone Creation";
    $action_icon = "fad fa-map-signs text-success";
    $page_id = "monitor-projects-link";
    $time_recorded = date('Y/m/d H:i:s');

    $sql_log = mysqli_query($dbc,"INSERT INTO activity_logs
                    (email,action_name,action_reference,action_icon,page_id,time_recorded)
                        VALUES
                ('".$_SESSION['email']."','".$action_name."','".$action_reference."',
                        '".$action_icon."','".$page_id."','".$time_recorded."')"
                 );

                 foreach ($_POST['activity_name'] as $row=>$selectedOption)
                 {
                   $select_last_id_sql = mysqli_query($dbc,"SELECT task_id,time_recorded FROM pm_activities ORDER BY
                                                         id DESC LIMIT 1") or die("failed");
                   $id_row = mysqli_fetch_array($select_last_id_sql);
                   $id = $id_row['task_id'];
                   $int = (int) filter_var($id, FILTER_SANITIZE_NUMBER_INT);
                   $int = $int+1;

                   $task_id = "TASK".$int;
                   $milestone = mysqli_fetch_array(mysqli_query($dbc,"SELECT * FROM pm_milestones ORDER BY id DESC LIMIT 1"));
                   $milestone_id = $milestone['id'];
                   $activity_name = mysqli_real_escape_string($dbc,$_POST['activity_name'][$row]);
                   $activity_start_date = mysqli_real_escape_string($dbc,$_POST['activity_start_date'][$row]);
                   $activity_end_date = mysqli_real_escape_string($dbc,$_POST['activity_end_date'][$row]);


                   $date1=date_create($activity_start_date);
                   $date2=date_create($activity_end_date);
                   $diff=date_diff($date2,$date1);

                   $duration =  $diff->format("%a");
                   $recorded_by = $_SESSION['name'];



                  /* $sql_activities = mysqli_query($dbc,"UPDATE pm_activities SET
                                                               milestone_id='$milestone_id',
                                                               activity_name='$activity_name',
                                                               start_date='$activity_start_date',
                                                               end_date='$activity_end_date',
                                                               duration='$duration',
                                                               recorded_by='$recorded_by'
                                                         WHERE task_id='$task_id'
                                            ") or die (mysqli_error($dbc));
*/
                  $sql_activities = mysqli_query($dbc,"INSERT INTO pm_activities
                                                          (task_id,milestone_id,activity_name,start_date,end_date,duration,recorded_by)
                                                        VALUES
                                                        ('".$task_id."','".$milestone_id."','".$activity_name."','".$activity_start_date."',
                                                          '".$activity_end_date."','".$duration."','".$recorded_by."')
                                                ") or die (mysqli_error($dbc));



                  $total_activities = mysqli_num_rows(mysqli_query($dbc,"SELECT id FROM pm_activities WHERE milestone_id='".$milestone_id."'"));
                  mysqli_query($dbc,"UPDATE pm_milestones SET total_tasks='".$total_activities."' WHERE id='".$milestone_id."'");


                 }

  if(mysqli_commit($dbc))
  {
      exit("success");
  }

else
{
  mysqli_rollback($dbc);
  exit("failed");
}
}
  //end of add milestone


  //start of edit milestone
  else if(isset($_POST['edit_milestone']))
  {
    $id = mysqli_real_escape_string($dbc,strip_tags($_POST['id']));
    $milestone_name = mysqli_real_escape_string($dbc,strip_tags($_POST['milestone_name']));
    $start_date = mysqli_real_escape_string($dbc,strip_tags($_POST['start_date']));
    $end_date= mysqli_real_escape_string($dbc,strip_tags($_POST['end_date']));
    //$duration = mysqli_real_escape_string($dbc,strip_tags($_POST['duration']));

    $date1 = new DateTime($start_date); //inclusive
    $date2 = new DateTime($end_date); //exclusive
    $diff = $date2->diff($date1);
    $duration =  $diff->format("%a");


    $recorded_by = $_SESSION['name'];

      //insert records to pm_projects

      /* set autocommit to off */
      mysqli_autocommit($dbc, FALSE);
      $sql_update = "UPDATE pm_milestones
                            SET
                            milestone_name='$milestone_name',
                            start_date='$start_date',
                            end_date='$end_date',
                            duration='$duration',
                            recorded_by='$recorded_by.'
                    WHERE id='$id'
                  ";

      $$update_milestone = mysqli_query($dbc,$sql_update);

    //log the action
    $action_reference = "Modified a Milestone  with id " . $id . " milestone name: ".$milestone_name;
    $action_name = "Milestone Modification";
    $action_icon = "fad fa-map-signs text-success";
    $page_id = "monitor-projects-link";
    $time_recorded = date('Y/m/d H:i:s');

    $sql_log = mysqli_query($dbc,"INSERT INTO activity_logs
                    (email,action_name,action_reference,action_icon,page_id,time_recorded)
                        VALUES
                ('".$_SESSION['email']."','".$action_name."','".$action_reference."',
                        '".$action_icon."','".$page_id."','".$time_recorded."')"
                 );

      if(mysqli_commit($dbc))
      {
          exit("success");
      }

    else
    {
      mysqli_rollback($dbc);
      exit("failed");
    }


  }

  //end of edit milestone

  //start add new milestone activity
  if(isset($_POST['add_new_milestone_activity']))
  {
    $select_last_id_sql = mysqli_query($dbc,"SELECT task_id,time_recorded FROM pm_activities ORDER BY
                                          id DESC LIMIT 1") or die("failed");
    $id_row = mysqli_fetch_array($select_last_id_sql);
    $id = $id_row['task_id'];
    $int = (int) filter_var($id, FILTER_SANITIZE_NUMBER_INT);
    $int = $int+1;

    $task_id = "TASK".$int;

   $department= mysqli_real_escape_string($dbc,strip_tags($_POST['department_name']));

      $name= mysqli_real_escape_string($dbc,strip_tags($_POST['name']));
    $milestone_id = mysqli_real_escape_string($dbc,strip_tags($_POST['milestone_id']));
    $project_id = mysqli_real_escape_string($dbc,strip_tags($_POST['project_id']));
    $activity_name = mysqli_real_escape_string($dbc,$_POST['activity_name']);
    $activity_start_date = mysqli_real_escape_string($dbc,$_POST['activity_start_date']);
    $activity_end_date = mysqli_real_escape_string($dbc,$_POST['activity_end_date']);

    $recorded_by = $_SESSION['name'];

    $date1=date_create($activity_start_date);
    $date2=date_create($activity_end_date);
    $diff=date_diff($date2,$date1);

    $duration =  $diff->format("%a");
    $recorded_by = $_SESSION['name'];

   $sql_activities = mysqli_query($dbc,"INSERT INTO pm_activities
                                           (task_id,milestone_id,department,listing, activity_name,start_date,end_date,duration,recorded_by)
                                         VALUES
                                         ('".$task_id."','".$milestone_id."','".$department."','".$name."','".$activity_name."','".$activity_start_date."',
                                           '".$activity_end_date."','".$duration."','".$recorded_by."')
                                 ") or die (mysqli_error($dbc));


                                 $total_activities = mysqli_num_rows(mysqli_query($dbc,"SELECT id FROM pm_activities WHERE milestone_id='".$milestone_id."'"));
                                 mysqli_query($dbc,"UPDATE pm_milestones SET total_tasks='".$total_activities."' WHERE id='".$milestone_id."'");

                                 //log the action
                                 $action_reference = "Added an activity with the name " . $activity_name . " for the milestone with id " . $milestone_id;
                                 $action_name = "Activity Addition";
                                 $action_icon = "fad fa-plus text-primary";
                                 $page_id = "monitor-projects-link";
                                 $time_recorded = date('Y/m/d H:i:s');

                                 $sql_log = mysqli_query($dbc,"INSERT INTO activity_logs
                                                 (email,action_name,action_reference,action_icon,page_id,time_recorded)
                                                     VALUES
                                             ('".$_SESSION['email']."','".$action_name."','".$action_reference."',
                                                     '".$action_icon."','".$page_id."','".$time_recorded."')"
                                              );

   if(mysqli_commit($dbc))
    {
      exit("success");
     }
    else
    {
        mysqli_rollback($dbc);
        exit("failed");
    }

  }
  //end add new milestone activity

  //start close milestone

  else if(isset($_POST['delete_milestone']))
  {
    $id = mysqli_real_escape_string($dbc,strip_tags($_POST['sid']));
      //insert records to pm_projects

      /* set autocommit to off */
      mysqli_autocommit($dbc, FALSE);
      $sql_delete = "DELETE FROM pm_milestones WHERE id='$id' ";

      $delete_milestone = mysqli_query($dbc,$sql_delete);

      $total_activities = mysqli_num_rows(mysqli_query($dbc,"SELECT id FROM pm_activities WHERE milestone_id='".$id."'"));
      mysqli_query($dbc,"UPDATE pm_milestones SET total_tasks='".$total_activities."' WHERE id='".$id."'");

    //log the action
    $action_reference = "Deleted a Milestone  with id " . $id;
    $action_name = "Milestone Deletion";
    $action_icon = "fad fa-map-signs text-danger";
    $page_id = "monitor-projects-link";
    $time_recorded = date('Y/m/d H:i:s');

    $sql_log = mysqli_query($dbc,"INSERT INTO activity_logs
                    (email,action_name,action_reference,action_icon,page_id,time_recorded)
                        VALUES
                ('".$_SESSION['email']."','".$action_name."','".$action_reference."',
                        '".$action_icon."','".$page_id."','".$time_recorded."')"
                 );

      if(mysqli_commit($dbc))
      {
          exit("success");
      }

    else
    {
      mysqli_rollback($dbc);
      exit("failed");
    }
  }
  //end close milestone






}

//END OF POST REQUEST


?>
