<?php
require_once('../setup/connect.php');
session_start();

if($_SERVER['REQUEST_METHOD'] == 'POST')
{

  //if its contract price
  if(isset($_POST['add_project-milestone-payment']))
  {
    $milestone_payment_id = mysqli_real_escape_string($dbc,strip_tags($_POST['milestone_payment_id']));
    $project_id = mysqli_real_escape_string($dbc,strip_tags($_POST['project_id']));
    $payment_milestone = mysqli_real_escape_string($dbc,strip_tags($_POST['payment_milestone']));
    $attached_project_milestone = mysqli_real_escape_string($dbc,strip_tags($_POST['attached_project_milestone']));
    $payment_clause = mysqli_real_escape_string($dbc,strip_tags($_POST['payment_clause']));
    $payment_due = mysqli_real_escape_string($dbc,strip_tags($_POST['payment_due']));
    $recorded_by = $_SESSION['name'];

      /* set autocommit to off */
      mysqli_autocommit($dbc, FALSE);

      //check if payment due exists 100

      $sum_sql = mysqli_fetch_array(mysqli_query($dbc,"SELECT SUM(payment_due) AS payment_due FROM pm_milestone_payment0 WHERE project_id='".$project_id."'"));
      $sum = $sum_sql['payment_due'];
      $total_due = $sum + $payment_due ;

      if($total_due > 100 )
      {
        exit("exceeded-due");
      }

      $sql_insert_milestone_payment = mysqli_query($dbc,  "INSERT INTO pm_milestone_payment0
                            (milestone_payment_id,project_id,attached_project_milestone_id,payment_milestone,payment_clause,payment_due, recorded_by)
                    VALUES
                            ('".$milestone_payment_id."','".$project_id."','".$attached_project_milestone."','".$payment_milestone."','".$payment_clause."' ,'".$payment_due."','".$recorded_by."')
                  ") or die (mysqli_error($dbc));

      foreach ($_POST['anticipated_cost'] as $row=>$selectedOption)
      {
        $anticipated_cost = mysqli_real_escape_string($dbc,$_POST['anticipated_cost'][$row]);
        $anticipated_currency = mysqli_real_escape_string($dbc,$_POST['anticipated_currency'][$row]);
        $anticipated_budget_line = mysqli_real_escape_string($dbc,$_POST['anticipated_budget_line'][$row]);
        $budget_id = mysqli_real_escape_string($dbc,$_POST['budget_id'][$row]);
        $amount = mysqli_real_escape_string($dbc,$_POST['budget'][$row]);

        $sql_insert_contract_price = mysqli_query($dbc,  "INSERT INTO pm_milestone_payment1
                              (milestone_payment_id,project_id,budget_id,budget_line,anticipated_cost,currency, recorded_by)
                      VALUES
                              ('".$milestone_payment_id."','".$project_id."','".$budget_id."','".$anticipated_budget_line."',
                                 '".$anticipated_cost."','".$anticipated_currency."','".$recorded_by."')
                    ") or die (mysqli_error($dbc));

      }


    //log the action
    $action_reference = "Added a Milestone Payment: " . $milestone_payment_id . " for the project id ".$project_id;
    $action_name = "Project Milestone Payment";
    $action_icon = "fal fa-money-check-edit-alt text-success";
    $page_id = "project-payments-tab";
    $time_recorded = date('Y/m/d H:i:s');

    $sql_log = mysqli_query($dbc,"INSERT INTO activity_logs
                    (email,action_name,action_reference,action_icon,page_id,time_recorded)
                        VALUES
                ('".$_SESSION['email']."','".$action_name."','".$action_reference."',
                        '".$action_icon."','".$page_id."','".$time_recorded."')"
                 );

      if(mysqli_commit($dbc))
      {
          exit("success");
      }

    else
    {
      mysqli_rollback($dbc);
      exit("failed");
    }



  }
  //start of modify payment milestone
  else if(isset($_POST['edit_milestone_payment']))
  {
      $id= mysqli_real_escape_string($dbc,strip_tags($_POST['id']));
      $milestone_payment_id = mysqli_real_escape_string($dbc,strip_tags($_POST['milestone_payment_id']));
      $attached_project_milestone = mysqli_real_escape_string($dbc,strip_tags($_POST['attached_project_milestone']));
      $payment_milestone = mysqli_real_escape_string($dbc,strip_tags($_POST['payment_milestone']));
      $payment_clause = mysqli_real_escape_string($dbc,strip_tags($_POST['payment_clause']));
      $payment_due = mysqli_real_escape_string($dbc,strip_tags($_POST['payment_due']));
      $recorded_by = $_SESSION['name'];

        /* set autocommit to off */
        mysqli_autocommit($dbc, FALSE);

        //check if payment due exists 100
        $project_row = mysqli_fetch_array(mysqli_query($dbc,"SELECT project_id FROM pm_milestone_payment0 WHERE id ='".$id."'"));
        $project_id = $project_row['project_id'];
        $sum_sql = mysqli_fetch_array(mysqli_query($dbc,"SELECT SUM(payment_due) AS payment_due FROM pm_milestone_payment0 WHERE project_id='".$project_id."' && id !='".$id."'"));
        $sum = $sum_sql['payment_due'];
        $total_due = $sum + $payment_due ;

        if($total_due > 100 )
        {
          exit("exceeded-due");
        }

      $sql_modify_payment_milestone = "UPDATE pm_milestone_payment0 SET
                                        payment_milestone ='$payment_milestone',
                                        attached_project_milestone_id='$attached_project_milestone',
                                        payment_clause='$payment_clause',
                                        payment_due='$payment_due'

                                  WHERE id='$id'
                              ";

      mysqli_query($dbc,$sql_modify_payment_milestone);

    //log the action
    $action_reference = "Modified milestone payment with id: " . $id . ",  description ".$payment_milestone;
    $action_name = "milestone payment Modification";
    $action_icon = "fal fa-money-check-edit-alt text-warning";
    $page_id = "project-payments-tab";
    $time_recorded = date('Y/m/d H:i:s');

    $sql_log = mysqli_query($dbc,"INSERT INTO activity_logs
                    (email,action_name,action_reference,action_icon,page_id,time_recorded)
                        VALUES
                ('".$_SESSION['email']."','".$action_name."','".$action_reference."',
                        '".$action_icon."','".$page_id."','".$time_recorded."')"
                 );

      if(mysqli_commit($dbc))
      {
          exit("success");
      }

    else
    {
      mysqli_rollback($dbc);
      exit("failed");
    }
  }

  }

  //end of add contract price


?>
