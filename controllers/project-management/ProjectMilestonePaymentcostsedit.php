<?php
session_start();
require_once('../setup/connect.php');
if(!empty($_POST))
{
    foreach($_POST as $field_name => $val)
    {
        //clean post values
        $field_userid = mysqli_real_escape_string($dbc,strip_tags($field_name));
        $val =mysqli_real_escape_string($dbc,strip_tags($val));
        //from the fieldname:user_id we need to get user_id
        $split_data = explode(':', $field_userid);
        $user_id = $split_data[1];
        $field_name = $split_data[0];

        $milestone_id_sql = mysqli_fetch_array(mysqli_query($dbc,"SELECT attached_project_milestone_id FROM pm_milestone_payment0
                                                                            WHERE
                                                                            milestone_payment_id IN
                                                                            (SELECT milestone_payment_id FROM pm_milestone_payment1 WHERE
                                                                            id='".$user_id."')"));


       $milestone_payment_id_sql = mysqli_fetch_array(mysqli_query($dbc,"SELECT milestone_payment_id FROM pm_milestone_payment1 WHERE id='".$user_id."'"));

       $milestone_payment_id = $milestone_payment_id_sql['milestone_payment_id'];

       $project_milestone_id_sql = mysqli_fetch_array(mysqli_query($dbc,"SELECT attached_project_milestone_id FROM pm_milestone_payment0
                                                                                      WHERE milestone_payment_id='".$milestone_payment_id."'"));

       $project_milestone_id = $project_milestone_id_sql['attached_project_milestone_id'];


        if(!empty($user_id) && !empty($field_name) && !empty($val))
        {
          //check if it is an internal and external budget, if, then drop and insert
            //update the values
            $update = mysqli_query($dbc,"UPDATE pm_milestone_payment1 SET $field_name = '$val' WHERE id = '$user_id'");
              if(mysqli_affected_rows($dbc) > 0)
              {
                //update
                $sql = mysqli_query($dbc,"UPDATE pm_milestones SET payment_status='initiated' WHERE id ='".$project_milestone_id."'");

                //log the action
                $action_reference = "Modified milestone payment with id: " . $user_id;
                $action_name = "milestone payment Modification";
                $action_icon = "fad fa-money-check-edit-alt text-success";
                $page_id = "project-payments-tab";
                $time_recorded = date('Y/m/d H:i:s');

                $sql_log = mysqli_query($dbc,"INSERT INTO activity_logs
                                (email,action_name,action_reference,action_icon,page_id,time_recorded)
                                    VALUES
                            ('".$_SESSION['email']."','".$action_name."','".$action_reference."',
                                    '".$action_icon."','".$page_id."','".$time_recorded."')"
                             );

                echo "Updated";
              }
              else
              {
                echo mysqli_error($dbc);
              }

        } else {
            echo "Invalid Requests";
        }
    }
} else {
    echo "Invalid Requests";
}
?>
