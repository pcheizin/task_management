<?php
require_once('../setup/connect.php');
session_start();

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
  if(isset($_REQUEST["sid"]))
  {
    $delete = "DELETE FROM pm_project_documents WHERE id='".$_POST['sid']."'";
  	if($query = mysqli_query($dbc,$delete))
      {
          if($affected = mysqli_affected_rows($dbc) ==1)
          {
            //log the action
            $action_reference = "Removed a project file with id " . $_POST['sid'];
            $action_name = "Project File Removal";
            $action_icon = "far fa-file-times text-danger";
            $page_id = "monitor-projects-link";
            $time_recorded = date('Y/m/d H:i:s');

            $sql_log = mysqli_query($dbc,"INSERT INTO activity_logs
                            (email,action_name,action_reference,action_icon,page_id,time_recorded)
                                VALUES
                        ('".$_SESSION['email']."','".$action_name."','".$action_reference."',
                                '".$action_icon."','".$page_id."','".$time_recorded."')"
                         );

             echo "success";
          }
          else
          {
              echo "failed";
          }

      }
      else
      {
          echo "Failed";
      }
  }

}



?>
