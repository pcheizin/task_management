<?php
require_once('../setup/connect.php');
session_start();

if($_SERVER['REQUEST_METHOD'] == 'POST')
{

  //if its milestone creation
  if(isset($_POST['add_resource']))
  {
      /* set autocommit to off */

      mysqli_autocommit($dbc, FALSE);

                 foreach ($_POST['resource_name'] as $row=>$selectedOption)
                 {
                   $select_last_id_sql = mysqli_query($dbc,"SELECT resource_id,time_recorded FROM pm_resources ORDER BY
                                                         time_recorded DESC LIMIT 1") or die("failed");
                   $id_row = mysqli_fetch_array($select_last_id_sql);
                   $id = $id_row['resource_id'];
                   $int = (int) filter_var($id, FILTER_SANITIZE_NUMBER_INT);
                   $int = $int+1;

                   $resource_id = "RES".$int;
                   $task_id = mysqli_real_escape_string($dbc,strip_tags($_POST['task_id']));
                   $project_id = mysqli_real_escape_string($dbc,strip_tags($_POST['project_id']));

                   $resource_name = mysqli_real_escape_string($dbc,$_POST['resource_name'][$row]);
                   $recorded_by = $_SESSION['name'];



                   $sql_resources = mysqli_query($dbc,"INSERT INTO pm_resources
                                                          (resource_id,activity_id,project_id,resource_name,recorded_by)
                                                      VALUES
                                                      ('".$resource_id."','".$task_id."','".$project_id."','".$resource_name."','".$recorded_by."')
                                            ") or die (mysqli_error($dbc));


                 }

                 //log the action
                 $action_reference = "Added a Resource for the task id " . $task_id;
                 $action_name = "Resource Creation";
                 $action_icon = "fal fa-users-medical text-success";
                 $page_id = "project-resource-plan-tab";
                 $time_recorded = date('Y/m/d H:i:s');

                 $sql_log = mysqli_query($dbc,"INSERT INTO activity_logs
                                 (email,action_name,action_reference,action_icon,page_id,time_recorded)
                                     VALUES
                             ('".$_SESSION['email']."','".$action_name."','".$action_reference."',
                                     '".$action_icon."','".$page_id."','".$time_recorded."')"
                              );

      if(mysqli_commit($dbc))
      {
          exit("success");
      }

    else
    {
      mysqli_rollback($dbc);
      exit("failed");
    }
}
else if (isset($_POST['delete_resource']))
{
  mysqli_autocommit($dbc, FALSE);

  $resource_id = mysqli_real_escape_string($dbc,strip_tags($_POST['sid']));

  $delete = mysqli_query($dbc,"DELETE FROM pm_resources WHERE resource_id='".$resource_id."'");

  //log the action
  $action_reference = "Deleted a Resource with the id " . $resource_id;
  $action_name = "Resource Removal";
  $action_icon = "fas fa-user-slash text-danger";
  $page_id = "project-resource-plan-tab";
  $time_recorded = date('Y/m/d H:i:s');

  $sql_log = mysqli_query($dbc,"INSERT INTO activity_logs
                  (email,action_name,action_reference,action_icon,page_id,time_recorded)
                      VALUES
              ('".$_SESSION['email']."','".$action_name."','".$action_reference."',
                      '".$action_icon."','".$page_id."','".$time_recorded."')"
               );


  if(mysqli_commit($dbc))
  {
    exit("success");
  }
  else
  {
    mysqli_rollback($dbc);
    exit("failed");
  }

}
else if (isset($_POST['add_task_status']))
{
  mysqli_autocommit($dbc, FALSE);

  $project_id = mysqli_real_escape_string($dbc,strip_tags($_POST['project_id']));
  $task_id = mysqli_real_escape_string($dbc,strip_tags($_POST['task_id']));
  $task_status = mysqli_real_escape_string($dbc,strip_tags($_POST['task_status']));
  $comments = mysqli_real_escape_string($dbc,strip_tags($_POST['task_status_comments']));
  $date_recorded = date('d-M-y');
  $recorded_by = $_SESSION['name'];

  if($task_status == "Task Received")
  {
    $color_code_class = "five";
  }
  if($task_status == "In Progress Behind Schedule")
  {
    $color_code_class = "four";
  }
  if($task_status == "Task Pending")
  {
    $color_code_class = "three";
  }
  if($task_status == "Task Completed")
  {
    $color_code_class = "two";
  }
  if($task_status == "Continous")
  {
    $color_code_class = "one";
  }
  if($task_status == "Repriotised")
  {
    $color_code_class = "one";
  }
  if($task_status == "In Progress Behind Schedule.")
  {
    $color_code_class = "five";
    $task_status = "In Progress Behind Schedule";
  }



  mysqli_query($dbc,"UPDATE pm_activity_updates SET changed='yes' WHERE task_id='".$task_id."'");

  $sql_task_update = mysqli_query($dbc,"INSERT INTO pm_activity_updates
                                         (task_id,project_id,status,color_code, comments,date_recorded,recorded_by)
                                     VALUES
                                     ('".$task_id."','".$project_id."','".$task_status."','".$color_code_class."','".$comments."','".$date_recorded."','".$recorded_by."')
                           ") or die (mysqli_error($dbc));


    $milestone_id_sql = mysqli_fetch_array(mysqli_query($dbc,"SELECT milestone_id FROM pm_activities WHERE task_id='".$task_id."'"));
    $milestone_id = $milestone_id_sql['milestone_id'];

    //select completed tasks
    $completed_tasks_sql = mysqli_query($dbc,"SELECT id FROM pm_activity_updates WHERE status='Completed' && changed='no' && task_id IN
                                                 (SELECT task_id FROM pm_activities WHERE milestone_id='".$milestone_id."')");

    $completed_tasks = mysqli_num_rows($completed_tasks_sql);

    //update the completed tasks
    mysqli_query($dbc,"UPDATE pm_milestones SET completed_tasks='".$completed_tasks."' WHERE id='".$milestone_id."'");

    //log the action
    $action_reference = "Updated the status for the task: " .$task_id ." to " .$task_status;
    $action_name = "Task Update";
    $action_icon = "fad fa-tasks text-info";
    $page_id = "project-resource-plan-tab";
    $time_recorded = date('Y/m/d H:i:s');

    $sql_log = mysqli_query($dbc,"INSERT INTO activity_logs
                               (email,action_name,action_reference,action_icon,page_id,time_recorded)
                                   VALUES
                           ('".$_SESSION['email']."','".$action_name."','".$action_reference."',
                                   '".$action_icon."','".$page_id."','".$time_recorded."')"
                            );

    if(mysqli_commit($dbc))
    {
        exit("success");
    }

  else
  {
    mysqli_rollback($dbc);
    exit("failed");
  }
}
else if (isset($_POST['delete_task']))
{
  mysqli_autocommit($dbc, FALSE);

  $task_id = mysqli_real_escape_string($dbc,strip_tags($_POST['sid']));

  //start mail

  $sql_activity_name = mysqli_fetch_array(mysqli_query($dbc,"SELECT activity_name,project_id,milestone_id FROM pm_activities WHERE task_id='".$task_id."'"));
  $activity_name = $sql_activity_name['activity_name'];
  $milestone_id = $sql_activity_name['milestone_id'];
  $project_id = $sql_activity_name['project_id'];
  $deleted_by = $_SESSION['name'];


  $milestone_name = mysqli_fetch_array(mysqli_query($dbc,"SELECT milestone_name FROM pm_milestones WHERE id='".$milestone_id."'"));
  $milestone_name = $milestone_name['milestone_name'];

  $project_name_row = mysqli_fetch_array(mysqli_query($dbc,"SELECT project_name FROM pm_projects WHERE project_id='".$project_id."'"));
  $project_name = $project_name_row['project_name'];

  $sql_activity_owner = mysqli_query($dbc,"SELECT resource_name FROM pm_resources WHERE activity_id='".$task_id."'") or die(mysqli_error($dbc));
  if(mysqli_num_rows($sql_activity_owner) > 0)
  {
    while ($owners = mysqli_fetch_array($sql_activity_owner))
    {
      $active_resource = mysqli_fetch_array(mysqli_query($dbc,"SELECT Email,Name FROM staff_users WHERE Name='".$owners['resource_name']."' "));
      $active_resource_email = $active_resource['Email'];
      $active_resource_name = $active_resource['Name'];

      $email[] = $active_resource_email;
      $name[] = $active_resource_name;
    }
    $to = implode(", ", $email);
    $name = implode(", ", $name);

  }

  $subject = 'PPRMIS Project Task Notification';
  $message = "Dear ".$name.", <br/><br/><br/>

  The activity: <u>".$activity_name."</u>, for the project ".$project_name.", under the ".$milestone_name." milestone has been deleted by ".$deleted_by."

  <br/><br/><br/>
  <i>This is an automated message, please do not reply</i>";
/*
  $send_mail = mail($to,$subject,$message,$headers);
  if($send_mail)
  {
    $date_sent = date("d-m-Y h:i:sa");
    $message = mysqli_real_escape_string($dbc,$message);
    $store_sent_mail = mysqli_query($dbc,"INSERT INTO sent_mails (sent_from,sent_to,triggered_by,message_subject,message_body,date_sent)
                                                  VALUES
                                                  ('".$headers."','".$to."','".$_SESSION['name']."','".$subject."','".$message."','".$date_sent."')"
                                   ) or die (mysqli_error($dbc));
  }
  else
  {
    exit("mail not sent");
  }

*/
  //end mail

  mysqli_query($dbc,"DELETE FROM pm_activities WHERE task_id='".$task_id."'");
  mysqli_query($dbc,"DELETE FROM pm_activity_updates WHERE task_id='".$task_id."'");
  mysqli_query($dbc,"DELETE FROM pm_resources WHERE activity_id='".$task_id."'");

  $milestone_id_sql = mysqli_fetch_array(mysqli_query($dbc,"SELECT milestone_id FROM pm_activities WHERE task_id='".$task_id."'"));
  $milestone_id = $milestone_id_sql['milestone_id'];

  //select completed tasks
  $completed_tasks_sql = mysqli_query($dbc,"SELECT id FROM pm_activity_updates WHERE status='Completed' && changed='no' && task_id IN
                                               (SELECT task_id FROM pm_activities WHERE milestone_id='".$milestone_id."')");

  $completed_tasks = mysqli_num_rows($completed_tasks_sql);

  //update the completed tasks
  mysqli_query($dbc,"UPDATE pm_milestones SET completed_tasks='".$completed_tasks."' WHERE id='".$milestone_id."'");



  //select total tasks
  $total_tasks_sql = mysqli_query($dbc,"SELECT id FROM pm_activities WHERE task_id='".$task_id."'");

  $total_tasks = mysqli_num_rows($total_tasks_sql);

  //update the completed tasks
  mysqli_query($dbc,"UPDATE pm_milestones SET total_tasks='".$total_tasks."' WHERE id='".$milestone_id."'");

  //log the action
  $action_reference = "Deleted a Task with the id " . $task_id;
  $action_name = "Resource Removal";
  $action_icon = "far fa-user-times text-danger";
  $page_id = "project-resource-plan-tab";
  $time_recorded = date('Y/m/d H:i:s');

  $sql_log = mysqli_query($dbc,"INSERT INTO activity_logs
                  (email,action_name,action_reference,action_icon,page_id,time_recorded)
                      VALUES
              ('".$_SESSION['email']."','".$action_name."','".$action_reference."',
                      '".$action_icon."','".$page_id."','".$time_recorded."')"
               );


  if(mysqli_commit($dbc))
  {
    exit("success");
  }
  else
  {
    mysqli_rollback($dbc);
    exit("failed");
  }

}







}

//END OF POST REQUEST


?>
