<?php
session_start();

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
  if(isset($_POST['add-milestone']))
  {

    require_once('../setup/connect.php');
session_start();

 require_once('../../phpmailer/PHPMailerAutoload.php');
 $mail = new phpmailer;

 $milestone_name = mysqli_real_escape_string($dbc,strip_tags($_POST['milestone_name']));
 $start_date = mysqli_real_escape_string($dbc,strip_tags($_POST['start_date']));
 $end_date= mysqli_real_escape_string($dbc,strip_tags($_POST['end_date']));
 $duration = mysqli_real_escape_string($dbc,strip_tags($_POST['duration']));

 $recorded_by = $_SESSION['name'];

$email = $_SESSION['email'];


    //$mail->isSMTP();                                            // Send using SMTP
    $mail->Host       = 'smtp.gmail.com';                    // Set the SMTP server to send through
    $mail->Port       = 587;                                    // TCP port to connect to, use 465 for `PHPMailer::ENCRYPTION_SMTPS` above
    $mail->SMTPSecure = 'tls';         // Enable TLS encryption; `PHPMailer::ENCRYPTION_SMTPS` encouraged

  //  $mail->SMTPAuth   = true;                                   // Enable SMTP authentication
    $mail->Username   = 'pcheizin@gmail.com';                     // SMTP username
    $mail->Password   = '9000@Kenya';                               // SMTP password

    //Recipients
    $mail->setFrom('nonreply@cytonn.com', 'Cytonn Task System');
    $mail->addAddress("$email", 'Cytonn Task System');     // Add a recipient
  //$mail->addAddress('danson@panoramaengineering.com');               // Name is optional
  //  $mail->addReplyTo('info@example.com', 'Information');
// $mail->addCC('moffat1@panoramaengineering.com');
  //  $mail->addBCC('bcc@example.com');

//$mail->setFrom('pcheizin@gmail.com', 'Panorama');
//$mail->addAddress(".$stock_approver.", ".$recorded_by.");     // Add a recipient

$mail->isHTML(true);                                  // Set email format to HTML
//$mail->addAttachment('../../views/stock-item/documents/panoramaLogo.jpg');
$mail->Subject = 'Cytonn Task System';

$mail->Body    = "Dear <b>".$email."</b>, <br/><br/><br/>


You Have added a Task Category <b>".$milestone_name."</b><br/>Start Date<b>".$start_date."</b><br/>End Date <b>".$end_date."</b><br/>
Duration<b>".$duration."</b>

<br/><br/><br/>
Please log in to <a href='https://cytonn.panoramaengineering.com/'>Cytonn Inventory</a> to view Details.
<br/><br/><br/><br/>
<b>This is an automated message, please do not reply</b>";

if(!$mail->send())
{
  echo 'Message not Sent';
}
else {
  echo 'Message has been sent';
}
}

if(isset($_POST['add_new_milestone_activity']))
{

  require_once('../setup/connect.php');
session_start();

require_once('../../phpmailer/PHPMailerAutoload.php');
$mail = new phpmailer;


   $department= mysqli_real_escape_string($dbc,strip_tags($_POST['department_name']));

      $name= mysqli_real_escape_string($dbc,strip_tags($_POST['name']));
    $milestone_id = mysqli_real_escape_string($dbc,strip_tags($_POST['milestone_id']));
    $project_id = mysqli_real_escape_string($dbc,strip_tags($_POST['project_id']));
    $activity_name = mysqli_real_escape_string($dbc,$_POST['activity_name']);
    $activity_start_date = mysqli_real_escape_string($dbc,$_POST['activity_start_date']);
    $activity_end_date = mysqli_real_escape_string($dbc,$_POST['activity_end_date']);


$recorded_by = $_SESSION['name'];

$email = $_SESSION['email'];


  //$mail->isSMTP();                                            // Send using SMTP
  $mail->Host       = 'smtp.gmail.com';                    // Set the SMTP server to send through
  $mail->Port       = 587;                                    // TCP port to connect to, use 465 for `PHPMailer::ENCRYPTION_SMTPS` above
  $mail->SMTPSecure = 'tls';         // Enable TLS encryption; `PHPMailer::ENCRYPTION_SMTPS` encouraged

//  $mail->SMTPAuth   = true;                                   // Enable SMTP authentication
  $mail->Username   = 'pcheizin@gmail.com';                     // SMTP username
  $mail->Password   = '9000@Kenya';                               // SMTP password

  //Recipients
  $mail->setFrom('nonreply@cytonn.com', 'Cytonn Task System');
  $mail->addAddress("$email", 'Cytonn Task System');     // Add a recipient
//$mail->addAddress('danson@panoramaengineering.com');               // Name is optional
//  $mail->addReplyTo('info@example.com', 'Information');
// $mail->addCC('moffat1@panoramaengineering.com');
//  $mail->addBCC('bcc@example.com');

//$mail->setFrom('pcheizin@gmail.com', 'Panorama');
//$mail->addAddress(".$stock_approver.", ".$recorded_by.");     // Add a recipient

$mail->isHTML(true);                                  // Set email format to HTML
//$mail->addAttachment('../../views/stock-item/documents/panoramaLogo.jpg');
$mail->Subject = 'Cytonn Task System';

$mail->Body    = "Dear <b>".$email."</b>, <br/><br/><br/>



You Have added a Task <b>".$activity_name." </b><br/>For the department<b>".$department."</b><br/> which is </b><br/>".$name."</b><br/>

</b><br/>Start Date<b>".$activity_start_date."</b><br/>End Date <b>".$activity_end_date."</b><br/>


<br/><br/><br/>
Please log in to <a href='https://cytonn.panoramaengineering.com/'>Cytonn Inventory</a> to view Details.
<br/><br/><br/><br/>
<b>This is an automated message, please do not reply</b>";

if(!$mail->send())
{
echo 'Message not Sent';
}
else {
echo 'Message has been sent';
}
}

if(isset($_POST['add_resource']))
{

    require_once('../setup/connect.php');
  session_start();

  require_once('../../phpmailer/PHPMailerAutoload.php');
  $mail = new phpmailer;

  $select_last_id_sql = mysqli_query($dbc,"SELECT resource_id,time_recorded FROM pm_resources ORDER BY
                                        time_recorded DESC LIMIT 1") or die("failed");
  $id_row = mysqli_fetch_array($select_last_id_sql);
  $resource_id = $id_row['resource_id'];

  $task_id = mysqli_real_escape_string($dbc,strip_tags($_POST['task_id']));


  $sql_activity_name = mysqli_fetch_array(mysqli_query($dbc,"SELECT activity_name,milestone_id FROM pm_activities WHERE task_id='".$task_id."'"));
  $activity_name = $sql_activity_name['activity_name'];
  $milestone_id = $sql_activity_name['milestone_id'];


  $milestone_name = mysqli_fetch_array(mysqli_query($dbc,"SELECT milestone_name FROM pm_milestones WHERE id='".$milestone_id."'"));
  $milestone_name = $milestone_name['milestone_name'];



  $sql_activity_owner = mysqli_query($dbc,"SELECT resource_name FROM pm_resources WHERE activity_id='".$task_id."'") or die(mysqli_error($dbc));
  if(mysqli_num_rows($sql_activity_owner) > 0)
  {
    while ($owners = mysqli_fetch_array($sql_activity_owner))
    {
      $resource_email = mysqli_fetch_array(mysqli_query($dbc,"SELECT Email FROM staff_users WHERE Name='".$owners['resource_name']."' "));
      $resource_email = $resource_email['Email'];

      $email[] = $resource_email;
      $name[] = $owners['resource_name'];
    }
    $to = implode(", ", $email);
    $name = implode(", ", $name);
  }
  //$mail->isSMTP();                                            // Send using SMTP
  $mail->Host       = 'smtp.gmail.com';                    // Set the SMTP server to send through
  $mail->Port       = 587;                                    // TCP port to connect to, use 465 for `PHPMailer::ENCRYPTION_SMTPS` above
  $mail->SMTPSecure = 'tls';         // Enable TLS encryption; `PHPMailer::ENCRYPTION_SMTPS` encouraged

//  $mail->SMTPAuth   = true;                                   // Enable SMTP authentication
  $mail->Username   = 'pcheizin@gmail.com';                     // SMTP username
  $mail->Password   = '9000@Kenya';                               // SMTP password

  //Recipients
  $mail->setFrom('nonreply@cytonn.com', 'Cytonn Task System');
  $mail->addAddress("$email", 'Cytonn Task System');     // Add a recipient
//$mail->addAddress('danson@panoramaengineering.com');               // Name is optional
//  $mail->addReplyTo('info@example.com', 'Information');
// $mail->addCC('moffat1@panoramaengineering.com');
//  $mail->addBCC('bcc@example.com');

//$mail->setFrom('pcheizin@gmail.com', 'Panorama');
//$mail->addAddress(".$stock_approver.", ".$recorded_by.");     // Add a recipient

$mail->isHTML(true);                                  // Set email format to HTML
//$mail->addAttachment('../../views/stock-item/documents/panoramaLogo.jpg');
$mail->Subject = 'Cytonn Task System';

$mail->Body    = "Dear <b>".$name."</b>, <br/><br/><br/>


  An Task with the description <u>".$activity_name."</u> has been added under the category".$milestone_name." .<br/><br/>

  You are receiving this email because you have been selected to handle this activity.<br/><br/>

  Please login to <a href='https://cytonn.panoramaengineering.com/'>Cytonn System</a>  to view more about the activity.

  <br/><br/><br/>
  <i>This is an automated message, please do not reply</i>";
  $send_mail = mail($to,$subject,$message,$headers);
  if(!$mail->send())
  {
  echo 'Message not Sent';
  }
  else {
  echo 'Message has been sent';
  }
}



}


//END OF POST REQUEST

 ?>
