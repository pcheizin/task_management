<?php
require_once('../setup/connect.php');
session_start();

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
  // Upload file
  $id = mysqli_real_escape_string($dbc,strip_tags($_POST['id']));
  
  $uploadDir = '../../views/project-management/documents/';
  $uploadStatus = 1;
  $uploadedFile = '';
  if(!empty($_FILES["file"]["name"])){

      // File path config
      $fileName = basename($_FILES["file"]["name"]);
      $targetFilePath = $uploadDir . $fileName;
      $fileType = pathinfo($targetFilePath, PATHINFO_EXTENSION);

      // Allow certain file formats
      $allowTypes = array('pdf', 'doc', 'docx');
      if(in_array($fileType, $allowTypes)){
          // Upload file to the server
          if(move_uploaded_file($_FILES["file"]["tmp_name"], $targetFilePath)){
              $uploadedFile = $fileName;
          }else{
              $uploadStatus = 0;
              exit('error-uploading');
          }
      }else{
          $uploadStatus = 0;
          exit('invalid-file');
      }
  }
  if($uploadStatus == 1)
  {

      //update the values
      $update = mysqli_query($dbc,"UPDATE pm_milestone_payment1 SET invoice_file = '$uploadedFile' WHERE id = $id");
        if(mysqli_affected_rows($dbc) > 0)
        {
          //log the action
          $action_reference = "Uploaded an Invoice file " . $uploadedFile;
          $action_name = "Invoice File Upload";
          $action_icon = "fad fa-file-invoice text-success";
          $page_id = "monitor-projects-link";
          $time_recorded = date('Y/m/d H:i:s');

          $sql_log = mysqli_query($dbc,"INSERT INTO activity_logs
                          (email,action_name,action_reference,action_icon,page_id,time_recorded)
                              VALUES
                      ('".$_SESSION['email']."','".$action_name."','".$action_reference."',
                              '".$action_icon."','".$page_id."','".$time_recorded."')"
                       );

          echo "success";
        }
        else
        {
          echo mysqli_error($dbc);
        }
    }


}

?>
