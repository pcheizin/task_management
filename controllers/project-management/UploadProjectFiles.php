<?php
require_once('../setup/connect.php');
session_start();

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
  if(isset($_POST['add-task-document']))
  {

    $additional_file = mysqli_real_escape_string($dbc,strip_tags($_POST['additional_file']));
    $activity_name = mysqli_real_escape_string($dbc,strip_tags($_POST['activity_name']));
      $recorded_by = $_SESSION['name'];
    // Upload file



    $uploadDir = '../../views/project-management/documents/';
    $uploadStatus = 1;
    $uploadedFile = '';
    if(!empty($_FILES["additional_file"]["name"])){

        // File path config
        $fileName = basename($_FILES["additional_file"]["name"]);
        $targetFilePath = $uploadDir . $fileName;
        $fileType = pathinfo($targetFilePath, PATHINFO_EXTENSION);

        // Allow certain file formats
        $allowTypes = array('pdf', 'doc', 'docx');
        if(in_array($fileType, $allowTypes)){
            // Upload file to the server
            if(move_uploaded_file($_FILES["additional_file"]["tmp_name"], $targetFilePath)){
                $uploadedFile = $fileName;
            }else{
                $uploadStatus = 0;
                exit('error-uploading');
            }
        }else{
            $uploadStatus = 0;
            exit('invalid-file');
        }
    }
        if($uploadStatus == 1)
        {
          //insert records to stock item


      /* set autocommit to off */
      mysqli_autocommit($dbc, FALSE);


      $insert_documents = mysqli_query($dbc,"INSERT INTO pm_task_files
        (task_id, document, recorded_by)
      VALUES ('".$activity_name."','".$additional_file."','".$recorded_by."')") or die (mysqli_error($dbc));



    //log the action
    $action_reference = "Attached Evidence Documents for the Task " . $item_name;
    $action_name = "Documents Attachments";
    $action_icon = "far fa-project-diagram text-success";
    $page_id = "stock-management-link";
    $time_recorded = date('Y/m/d H:i:s');

    $sql_log = mysqli_query($dbc,"INSERT INTO activity_logs
                    (email,action_name,action_reference,action_icon,page_id,time_recorded)
                        VALUES
                ('".$_SESSION['email']."','".$action_name."','".$action_reference."',
                        '".$action_icon."','".$page_id."','".$time_recorded."')"
                 );

      if(mysqli_commit($dbc))
      {
          exit("success");
      }

    else
    {
      mysqli_rollback($dbc);
      exit("failed");
    }

    }
    }


}


 ?>
