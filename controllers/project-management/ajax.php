<?php
session_start();
require_once('../setup/connect.php');
if(!empty($_POST))
{
    foreach($_POST as $field_name => $val)
    {
        //clean post values
        $field_userid = mysqli_real_escape_string($dbc,strip_tags($field_name));
        $val =mysqli_real_escape_string($dbc,strip_tags($val));
        //from the fieldname:user_id we need to get user_id
        $split_data = explode(':', $field_userid);
        $user_id = $split_data[1];
        $field_name = $split_data[0];

        if(!empty($user_id) && !empty($field_name) && !empty($val))
        {
          //check if it is an internal and external budget, if, then drop and insert 
            //update the values
            $update = mysqli_query($dbc,"UPDATE pm_budget SET $field_name = '$val' WHERE id = $user_id");
              if(mysqli_affected_rows($dbc) > 0)
              {
                //log the action
                $action_reference = "Updated the project budget with the id" . $user_id;
                $action_name = "Project Budget Update";
                $action_icon = "fad fa-money-check-edit-alt text-warning";
                $page_id = "monitor-projects-link";
                $time_recorded = date('Y/m/d H:i:s');

                $sql_log = mysqli_query($dbc,"INSERT INTO activity_logs
                                (email,action_name,action_reference,action_icon,page_id,time_recorded)
                                    VALUES
                            ('".$_SESSION['email']."','".$action_name."','".$action_reference."',
                                    '".$action_icon."','".$page_id."','".$time_recorded."')"
                             );

                echo "Updated";
              }
              else
              {
                echo mysqli_error($dbc);
              }

        } else {
            echo "Invalid Requests";
        }
    }
} else {
    echo "Invalid Requests";
}
?>
