<?php
session_start();
require_once('../setup/connect.php');
if($_SERVER['REQUEST_METHOD'] == "POST")
{
  $reference_no = mysqli_real_escape_string($dbc,$_POST['reference_no']);
  $department = mysqli_real_escape_string($dbc,$_POST['department']);

  $dep_hod = mysqli_fetch_array(mysqli_query($dbc, "SELECT * FROM staff_users WHERE EmpNo IN (SELECT manager_id FROM departments WHERE department_id='".$department."')"));
  $dep_email = $dep_hod['Email'];
  $dep_title = $dep_hod['designation'];

  $select_max = mysqli_query($dbc, "SELECT MAX(reference_no) AS reference_no FROM risk_management WHERE department_code='".$department."' && changed='no'");
  $select_ro = mysqli_fetch_array(mysqli_query($dbc,
                    "SELECT risk_opportunity,risk_description FROM risk_management WHERE risk_reference='".$reference_no."' && changed='no'")
                  );
  $ro = $select_ro['risk_opportunity'];
  $risk_or_opportunity = $ro;
  if($ro == "risk")
  {
    $ro = "R";
  }
  else
  {
    $ro = "O";
  }
  $description = $select_ro['risk_description'];
  $max_id = mysqli_fetch_array($select_max);
  $id = (int )$max_id['reference_no'];
  $id = $id + 1;
  $id = $id;


  $ref_no = $department."/".$ro."/".$id;

  $date_recorded = date("m/d/Y");
  $time_recorded = date("h:i:sa");

  mysqli_autocommit($dbc, FALSE);
  mysqli_query($dbc,"BEGIN TRANSACTION");

  //find duplicate risk
  /*
  $duplicate = mysqli_query($dbc, "SELECT risk_description FROM risk_management WHERE department_code='".$department."'
                                    && changed='no' && risk_description='".$description."'");
  if(mysqli_num_rows($duplicate) > 0)
  {
    exit("duplicate");
  }
  */

  $sql_select_register = mysqli_query($dbc,"SELECT * FROM risk_management WHERE risk_reference='".$reference_no."' && changed='no'") or die (mysqli_error($dbc));

  $sql_select_drivers = mysqli_query($dbc,"SELECT * FROM risk_drivers WHERE risk_reference='".$reference_no."' && changed='no'") or die (mysqli_error($dbc));

  $sql_select_update = mysqli_query($dbc,"SELECT * FROM update_risk_status WHERE reference_no='".$reference_no."' && changed='no'") or die (mysqli_error($dbc));
  //check if there's an update made
  $update_made = mysqli_num_rows($sql_select_update);

  if($update_made > 0)
  {
    while($register_row = mysqli_fetch_array($sql_select_register))
    {
      $reference_no = $register_row;
      $department_code = $department;
      $risk_opportunity = $register_row['risk_opportunity'];
      $risk_reference = $ref_no;
      $strategic_objective = $register_row['strategic_objective_id'];
      $departmental_objective = $register_row['departmental_objective_id'];
      $risk_description = addslashes($register_row['risk_description']);
      $impact =  addslashes($register_row['impact']);
      $impact_score = (int)$register_row['impact_score'];
      $likelihood_score = (int)$register_row['likelihood_score'];
      $overall_score = (int)$register_row['overall_score'];
      $person_responsible = $register_row['person_responsible'];
      $created_by = $register_row['created_by'];
      $date_recorded = $register_row['date_recorded'];
      $time_recorded = $register_row['time_recorded'];

      if($impact_score == '')
      {
        $impact_score = 0;
      }
      if($likelihood_score == '')
      {
        $likelihood_score = 0;
      }
      if($overall_score == '')
      {
        $overall_score = 0;
      }

      $insert_register = mysqli_query($dbc, "INSERT INTO risk_management
                                                  (reference_no,department_code,risk_opportunity,risk_reference,strategic_objective_id,
                                                  departmental_objective_id,risk_description,
                                                  impact,impact_score,likelihood_score,overall_score, person_responsible,
                                                  created_by,date_recorded,time_recorded)

                                              VALUES
                                              ('".$id."','".$department_code."', '".$risk_opportunity."', '".$risk_reference."',
                                                 '".$strategic_objective."','".$departmental_objective."','".$risk_description."',
                                              '".$impact."', '".$impact_score."', '".$likelihood_score."', '".$overall_score."',
                                               '".$person_responsible."','".$created_by."', '".$date_recorded."', '".$time_recorded."')
                                              ") or die (mysqli_error($dbc));
    }

    while($driver_row = mysqli_fetch_array($sql_select_drivers))
    {
      $risk_reference = $ref_no;
      $driver = addslashes($driver_row['risk_drivers']);
      $kri = addslashes($driver_row['key_risk_indicator']);
      $kri_level = addslashes($driver_row['current_kri_level']);
      $kri_threshold = addslashes($driver_row['kri_threshold']);
      $treatment = addslashes($driver_row['treatment_action']);
      $strategy = addslashes($driver_row['risk_management_strategy_undertaken']);
      $effects = addslashes($driver_row['effects_of_risk_to_authority']);
      $action = addslashes($driver_row['action_to_be_undertaken']);
      $period_from = $driver_row['period_from'];
      $quarter = $driver_row['quarter'];
      $changed = $driver_row['changed'];
      $quartely_update = $driver_row['quarterly_update'];
      $status = $driver_row['status'];
      $insert_drivers = mysqli_query($dbc,"INSERT INTO risk_drivers
                                      (risk_reference,risk_drivers,key_risk_indicator,current_kri_level,kri_threshold,treatment_action,
                                      risk_management_strategy_undertaken,effects_of_risk_to_authority,action_to_be_undertaken, period_from,
                                      quarter,changed, quarterly_update, status)
                                      VALUES
                                      ('".$risk_reference."','".$driver."','".$kri."','".$kri_level."','".$kri_threshold."',
                                      '".$treatment."','".$strategy."','".$effects."','".$action."','".$period_from."','".$quarter."',
                                      '".$changed."', '".$quartely_update."','".$status."'
                                    )
                                    ")  or die (mysqli_error($dbc));
    }

      while($update_row = mysqli_fetch_array($sql_select_update))
      {
        $dep_code = $department;
        $risk_opportunity = $update_row['risk_opportunity'];
        $reference_no = $ref_no;
        $risk_description = addslashes($update_row['risk_description']);
        $period_from = $update_row['period_from'];
        $quarter = $update_row['quarter'];
        $prior_impact_score = (int)$update_row['prior_impact_score'];
        $prior_likelihood = (int)$update_row['prior_likelihood_score'];
        $prior_overall_score = (int)$update_row['prior_overall_score'];
        $impact_score = (int)$update_row['current_impact_score'];
        $likelihood_score = (int)$update_row['current_likelihood_score'];
        $overall_score = (int)$update_row['current_overall_score'];
        $comments_updates_monitoring = addslashes($update_row['comments_updates_monitoring']);
        $created_by = $update_row['created_by'];
        $date_recorded = $update_row['date_updated'];

        if($prior_impact_score == '')
        {
          $prior_impact_score = 0;
        }
        if($prior_likelihood_score == '')
        {
          $prior_likelihood_score = 0;
        }
        if($prior_overall_score == '')
        {
          $prior_overall_score = 0;
        }

        if($impact_score == '')
        {
          $impact_score = 0;
        }
        if($likelihood_score == '')
        {
          $likelihood_score = 0;
        }
        if($overall_score == '')
        {
          $overall_score = 0;
        }
        $insert_update = mysqli_query($dbc,"INSERT INTO update_risk_status
                                     (dep_code,risk_opportunity,reference_no, risk_description, period_from, quarter,
                                      prior_impact_score, prior_likelihood_score,
                                      prior_overall_score, current_impact_score, current_likelihood_score,
                                      current_overall_score,comments_updates_monitoring,updated_by, date_updated)

                                      VALUES
                                      ('".$dep_code."','".$risk_opportunity."','".$reference_no."', '".$risk_description."', '".$period_from."',
                                        '".$quarter."','".$prior_impact_score."', '".$prior_likelihood_score."',
                                      '".$prior_overall_score."', '".$impact_score."', '".$likelihood_score."',
                                      '".$overall_score."','".$comments_updates_monitoring."','".$created_by."', '".$date_recorded."')
                                      ")  or die (mysqli_error($dbc));
      }


      //log the action
      $action_reference = "Copied  riskUpdated the activity with the id" . $activity_id;
      $action_name = "Updated Activity";
      $action_icon = "far fa-calendar-edit text-success";
      $page_id = "monitor-workplan-link";
      $time_recorded = date('Y/m/d H:i:s');

      $sql_log = mysqli_query($dbc,"INSERT INTO activity_logs
                      (email,action_name,action_reference,action_icon,page_id,time_recorded)
                          VALUES
                  ('".$_SESSION['email']."','".$action_name."','".$action_reference."',
                          '".$action_icon."','".$page_id."','".$time_recorded."')"
                   );



    //confirm if both queries run
    if($insert_register && $insert_drivers && $insert_update)
    {
      mysqli_query($dbc,"COMMIT");
      echo "success";
    }
    else
    {
      mysqli_query($dbc,"ROLLBACK");
      echo mysqli_error($dbc);
    }
  }
  else
  {
    //if no quarterly update made
    while($register_row = mysqli_fetch_array($sql_select_register))
    {
      $reference_no = $register_row;
      $department_code = $department;
      $risk_opportunity = $register_row['risk_opportunity'];
      $risk_reference = $ref_no;
      $strategic_objective = $register_row['strategic_objective_id'];
      $departmental_objective = $register_row['departmental_objective_id'];
      $risk_description = addslashes($register_row['risk_description']);
      $impact =  addslashes($register_row['impact']);
      $impact_score = (int)$register_row['impact_score'];
      $likelihood_score = (int)$register_row['likelihood_score'];
      $overall_score = (int)$register_row['overall_score'];
      $person_responsible = $register_row['person_responsible'];
      $created_by = $register_row['created_by'];
      $date_recorded = $register_row['date_recorded'];
      $time_recorded = $register_row['time_recorded'];

      if($impact_score == '')
      {
        $impact_score = 0;
      }
      if($likelihood_score == '')
      {
        $likelihood_score = 0;
      }
      if($overall_score == '')
      {
        $overall_score = 0;
      }

      $insert_register = mysqli_query($dbc, "INSERT INTO risk_management
                                                  (reference_no,department_code,risk_opportunity,risk_reference,strategic_objective_id,
                                                  departmental_objective_id,risk_description,
                                                  impact,impact_score,likelihood_score,overall_score, person_responsible,
                                                  created_by,date_recorded,time_recorded)

                                              VALUES
                                              ('".$id."','".$department_code."', '".$risk_opportunity."', '".$risk_reference."',
                                                 '".$strategic_objective."','".$departmental_objective."','".$risk_description."',
                                              '".$impact."', '".$impact_score."', '".$likelihood_score."', '".$overall_score."',
                                               '".$person_responsible."','".$created_by."', '".$date_recorded."', '".$time_recorded."')
                                              ") or die (mysqli_error($dbc));
    }

    while($driver_row = mysqli_fetch_array($sql_select_drivers))
    {
      $risk_reference = $ref_no;
      $driver = addslashes($driver_row['risk_drivers']);
      $kri = addslashes($driver_row['key_risk_indicator']);
      $kri_level = addslashes($driver_row['current_kri_level']);
      $kri_threshold = addslashes($driver_row['kri_threshold']);
      $treatment = addslashes($driver_row['treatment_action']);
      $strategy = addslashes($driver_row['risk_management_strategy_undertaken']);
      $effects = addslashes($driver_row['effects_of_risk_to_authority']);
      $action = addslashes($driver_row['action_to_be_undertaken']);
      $period_from = $driver_row['period_from'];
      $quarter = $driver_row['quarter'];
      $changed = $driver_row['changed'];
      $quartely_update = $driver_row['quarterly_update'];
      $status = $driver_row['status'];
      $insert_drivers = mysqli_query($dbc,"INSERT INTO risk_drivers
                                      (risk_reference,risk_drivers,key_risk_indicator,current_kri_level,kri_threshold,treatment_action,
                                      risk_management_strategy_undertaken,effects_of_risk_to_authority,action_to_be_undertaken, period_from,
                                      quarter,changed, quarterly_update, status)
                                      VALUES
                                      ('".$risk_reference."','".$driver."','".$kri."','".$kri_level."','".$kri_threshold."',
                                      '".$treatment."','".$strategy."','".$effects."','".$action."','".$period_from."','".$quarter."',
                                      '".$changed."', '".$quartely_update."','".$status."'
                                    )
                                    ")  or die (mysqli_error($dbc));
    }

    //log the action
    $action_reference = "Copied the " . $ro . " with the reference no ". $reference_no. " to " . $department;
    $action_name = $ro. " Copying";
    $action_icon = "fad fa-copy text-success";
    $page_id = "monitor-risks-link";
    $time_recorded = date('Y/m/d H:i:s');

    $sql_log = mysqli_query($dbc,"INSERT INTO activity_logs
                    (email,action_name,action_reference,action_icon,page_id,time_recorded)
                        VALUES
                ('".$_SESSION['email']."','".$action_name."','".$action_reference."',
                        '".$action_icon."','".$page_id."','".$time_recorded."')"
                 );
    //confirm if both queries run
    if($insert_register && $insert_drivers && $sql_log)
    {
      mysqli_query($dbc,"COMMIT");
      echo "success";
    }
    else
    {
      mysqli_query($dbc,"ROLLBACK");
      echo mysqli_error($dbc);
    }
  }





}
 ?>
