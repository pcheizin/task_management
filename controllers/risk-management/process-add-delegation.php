<?php
require_once('../setup/connect.php');
session_start();
if($_SERVER['REQUEST_METHOD'] == 'POST')
{
    //collect form fields
    $delegated_to = mysqli_real_escape_string($dbc,strip_tags($_POST['delegated_to']));
    $delegated_start_date = mysqli_real_escape_string($dbc,strip_tags($_POST['delegation_start_date']));
    $delegated_end_date = mysqli_real_escape_string($dbc,strip_tags($_POST['delegation_end_date']));
    $delegated_from_name = mysqli_real_escape_string($dbc,strip_tags($_POST['delegated_from_name']));
    $date_recorded = date("m/d/Y");
    $time_recorded = date("h:i:sa");


    // select linked email to the delegated from name
    $sql_delegated_email = mysqli_fetch_array(mysqli_query($dbc,"SELECT * FROM staff_users WHERE Name='".$delegated_from_name."'"));
    if($sql_delegated_email)
    {
      $delegated_from_email = $sql_delegated_email['Email'];
    }
    else
    {
      exit("failed");
    }
    //select linked name from staff users, and fetch all its records
    $sql_select = mysqli_fetch_array(mysqli_query($dbc,"SELECT * FROM staff_users WHERE Name='".$delegated_to."'"));
    if($sql_select)
    {
      $delegated_to_dep_code = $sql_select['DepartmentCode'];
      $delegated_to_email =  $sql_select['Email'];
    }
    else
    {
      exit("failed");
    }
    //insert values to database
    $sql_statement = "INSERT INTO delegations

                        (dep_code,delegated_from_name,delegated_from_email,delegated_to_name,delegated_to_email,
                           delegation_start_date,delegation_end_date,status, date_processed, time_processed)

                      VALUES
                          ('".$_SESSION['department_code']."','".$delegated_from_name."','".$delegated_from_email."','".$delegated_to."',
                          '".$delegated_to_email."','".$delegated_start_date."','".$delegated_end_date."','active',
                          '".$date_recorded."', '".$time_recorded."')
                        ";

//log the action
$action_reference = "Delegated Risk Approvals to " . $delegated_to;
$action_name = " Approval Delegation";
$action_icon = "fad fa-handshake text-success";
$page_id = "delegate-approvals-link";
$time_recorded = date('Y/m/d H:i:s');

$sql_log = mysqli_query($dbc,"INSERT INTO activity_logs
                      (email,action_name,action_reference,action_icon,page_id,time_recorded)
                      VALUES
                    ('".$_SESSION['email']."','".$action_name."','".$action_reference."',
                    '".$action_icon."','".$page_id."','".$time_recorded."')"
          );

    //check if query runs

    if($insert_query = mysqli_query($dbc,$sql_statement) && $sql_log)
    {

      echo "success";

    }
    else
    {
        exit ("failed");
    }
}

?>
