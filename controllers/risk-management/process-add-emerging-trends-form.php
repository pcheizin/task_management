<?php
require_once('../setup/connect.php');
session_start();
if($_SERVER['REQUEST_METHOD'] == 'POST')
{
   $dep_code = $_SESSION['department_code'];
   $factor=mysqli_real_escape_string($dbc,$_POST['factor']);
   $period= mysqli_real_escape_string($dbc,$_POST['period']);
   $quarter = mysqli_real_escape_string($dbc,$_POST['quarter']);
   $created_by = $_SESSION['name'];
   $date_recorded = date("m/d/Y");
   $time_recorded = date("h:i:sa");

   if($_SESSION['access_level'] == "director" || $_SESSION['designation'] == "Chief Executive" )
   {
     $is_corporate = "yes";
   }
   else
   {
     $is_corporate = "no";
   }

   foreach ($_POST['factor'] as $row=>$selectedOption)
   {
     $factor = mysqli_real_escape_string($dbc,$_POST['factor'][$row]);
     $external_internal = mysqli_real_escape_string($dbc,$_POST['external_internal'][$row]);
     $related_risk_event = mysqli_real_escape_string($dbc,$_POST['related_risk_event'][$row]);
     $changes_in_risk_profile = mysqli_real_escape_string($dbc,$_POST['changes_in_risk_profile'][$row]);

     //start transaction
     mysqli_query($dbc,"START TRANSACTION");

     $query = mysqli_query($dbc,"
                 INSERT INTO emerging_trends
                 (dep_code, period, quarter, factor, external_internal, related_risk_event,
                   changes_in_risk_profile,is_corporate, created_by, date_recorded, time_recorded)
                 VALUES
                 ('".$dep_code."','".$period."','".$quarter."','".$factor."',
                 '".$external_internal."','".$related_risk_event."','".$changes_in_risk_profile."',
                 '".$is_corporate."','".$created_by."','".$date_recorded."','".$time_recorded."')

             ");

   }
   //log the action
   $action_reference = "Added an Emerging Trend with the factor: " . $factor;
   $action_name = " Added Emerging Trend";
   $action_icon = "fal fa-poll text-success";
   $page_id = "emerging-trends-link";
   $time_recorded = date('Y/m/d H:i:s');

   $sql_log = mysqli_query($dbc,"INSERT INTO activity_logs
                         (email,action_name,action_reference,action_icon,page_id,time_recorded)
                         VALUES
                       ('".$_SESSION['email']."','".$action_name."','".$action_reference."',
                       '".$action_icon."','".$page_id."','".$time_recorded."')"
             );

   if($query && $sql_log)
   {
     mysqli_query($dbc,"COMMIT");
     echo "success";
   }
   else
   {
     echo mysqli_error($dbc);
   }


}
?>
