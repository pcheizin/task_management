<?php
require_once('../setup/connect.php');
session_start();
if($_SERVER['REQUEST_METHOD'] == 'POST')
{

  $created_by = $_SESSION['name'];
  $dep_code = $_SESSION['department_code'];
  $date_recorded = date("m/d/Y");
  $time_recorded = date("h:i:sa");
  $period= mysqli_real_escape_string($dbc,strip_tags($_POST['period']));
  $quarter = mysqli_real_escape_string($dbc,strip_tags($_POST['quarter']));

  if($_SESSION['access_level'] == "director" || $_SESSION['designation'] == "Chief Executive" )
  {
    $is_corporate = "yes";
  }
  else
  {
    $is_corporate = "no";
  }

  if(isset($_POST['strategies_that_worked_well']))
  {
    $strategies_that_worked_well = mysqli_real_escape_string($dbc,strip_tags($_POST['strategies_that_worked_well']));
    $sql_statement = "INSERT INTO strategies_that_worked_well

                        (strategies_that_worked_well,dep_code,period,quarter,is_corporate,created_by,date_recorded,time_recorded)

                      VALUES
                          ('".$strategies_that_worked_well."','".$dep_code."','".$period."','".$quarter."','".$is_corporate."','".$created_by."',
                            '".$date_recorded."','".$time_recorded."')
                        ";

    //check if query runs

    //log the action
    $action_reference = "Added a Strategy that worked well: " . $strategies_that_worked_well;
    $action_name = " Lessons Learnt";
    $action_icon = "fal fa-chalkboard-teacher text-success";
    $page_id = "lessons-learnt-link";
    $time_recorded = date('Y/m/d H:i:s');

    $sql_log = mysqli_query($dbc,"INSERT INTO activity_logs
                          (email,action_name,action_reference,action_icon,page_id,time_recorded)
                          VALUES
                        ('".$_SESSION['email']."','".$action_name."','".$action_reference."',
                        '".$action_icon."','".$page_id."','".$time_recorded."')"
              );

    if($insert_query = mysqli_query($dbc,$sql_statement) && $sql_log)
    {
        exit ("success");
    }
    else
    {
        exit ("failed");
    }
  }
  if(isset($_POST['strategies_that_did_not_work']))
  {
    $strategies_that_did_not_work = mysqli_real_escape_string($dbc,strip_tags($_POST['strategies_that_did_not_work']));
    $sql_statement = "INSERT INTO strategies_that_did_not_work

                        (strategies_that_did_not_work,dep_code,period,quarter,is_corporate,created_by,date_recorded,time_recorded)

                      VALUES
                          ('".$strategies_that_did_not_work."','".$dep_code."','".$period."','".$quarter."','".$is_corporate."','".$created_by."',
                            '".$date_recorded."','".$time_recorded."')
                        ";

    //check if query runs
    //log the action
    $action_reference = "Added a Strategy that did not work: " . $strategies_that_did_not_work;
    $action_name = " Lessons Learnt";
    $action_icon = "fal fa-chalkboard-teacher text-danger";
    $page_id = "lessons-learnt-link";
    $time_recorded = date('Y/m/d H:i:s');

    $sql_log = mysqli_query($dbc,"INSERT INTO activity_logs
                          (email,action_name,action_reference,action_icon,page_id,time_recorded)
                          VALUES
                        ('".$_SESSION['email']."','".$action_name."','".$action_reference."',
                        '".$action_icon."','".$page_id."','".$time_recorded."')"
              );

    if($insert_query = mysqli_query($dbc,$sql_statement))
    {
        exit ("success");
    }
    else
    {
        exit ("failed");
    }
  }
  if(isset($_POST['near_misses']))
  {
    $near_misses = mysqli_real_escape_string($dbc,strip_tags($_POST['near_misses']));
    $sql_statement = "INSERT INTO near_misses

                        (near_misses,dep_code,period,quarter,is_corporate,created_by,date_recorded,time_recorded)

                      VALUES
                          ('".$near_misses."','".$dep_code."','".$period."','".$quarter."','".$is_corporate."','".$created_by."',
                            '".$date_recorded."','".$time_recorded."')
                        ";

    //check if query runs

    //log the action
    $action_reference = "Added a near miss:  " . $near_misses;
    $action_name = " Lessons Learnt";
    $action_icon = "fal fa-chalkboard-teacher text-warning";
    $page_id = "lessons-learnt-link";
    $time_recorded = date('Y/m/d H:i:s');

    $sql_log = mysqli_query($dbc,"INSERT INTO activity_logs
                          (email,action_name,action_reference,action_icon,page_id,time_recorded)
                          VALUES
                        ('".$_SESSION['email']."','".$action_name."','".$action_reference."',
                        '".$action_icon."','".$page_id."','".$time_recorded."')"
              );

    if($insert_query = mysqli_query($dbc,$sql_statement))
    {
        exit ("success");
    }
    else
    {
        exit ("failed");
    }
  }

}

?>
