<?php
session_start();
require_once('../setup/connect.php');;

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
    $reference_no = mysqli_real_escape_string($dbc,strip_tags($_POST['reference_no']));
    $department_code = mysqli_real_escape_string($dbc,strip_tags($_POST['department_code']));
    $risk_opportunity = mysqli_real_escape_string($dbc,strip_tags($_POST['risk_opportunity']));
    $risk_or_opportunity = mysqli_real_escape_string($dbc,strip_tags($_POST['risk_or_opportunity']));
    $risk_reference = $department_code."/".$risk_or_opportunity."/".$reference_no;
    $strategic_objective = mysqli_real_escape_string($dbc,strip_tags($_POST['strategic_objective']));
    $departmental_objective = mysqli_real_escape_string($dbc,strip_tags($_POST['departmental_objective']));
    //$departmental_sub_objective =implode(" , ",$_POST['departmental_sub_objectives']);
    $risk_description = mysqli_real_escape_string($dbc,strip_tags($_POST['risk_description']));
    $impact = mysqli_real_escape_string($dbc,strip_tags($_POST['impact']));
    $impact_score = mysqli_real_escape_string($dbc,strip_tags($_POST['impact_score']));
    $likelihood_score = mysqli_real_escape_string($dbc,strip_tags($_POST['likelihood_score']));
    $overall_score = mysqli_real_escape_string($dbc,strip_tags($_POST['overall_score']));
    //$person_responsible = mysqli_real_escape_string($dbc,strip_tags($_POST['person_responsible']));
    $person_responsible = implode(" , ",$_POST['person_responsible']);
    $created_by = $_SESSION['name'];
    $date_recorded = date("m/d/Y");
    $time_recorded = date("h:i:sa"); 

    $empty_quarter_and_year = 'null';
    $empty_scores = '0';
    $comments_updates_monitoring = '';
    $status = 'approved';
    $risk_status = 'open';


    //start transaction
    mysqli_query($dbc,"START TRANSACTION");

    $sql_insert = "INSERT INTO risk_management
                    (reference_no,department_code,risk_opportunity,risk_reference,strategic_objective_id,
                    departmental_objective_id,risk_description,
                    impact,impact_score,likelihood_score,overall_score, person_responsible,
                    created_by,date_recorded,time_recorded)

                    VALUES

                    (
                    '".$reference_no."', '".$department_code."', '".$risk_opportunity."', '".$risk_reference."', '".$strategic_objective."',
                    '".$departmental_objective."','".$risk_description."',
                    '".$impact."', '".$impact_score."', '".$likelihood_score."', '".$overall_score."', '".$person_responsible."',
                    '".$created_by."', '".$date_recorded."', '".$time_recorded."'
                    )

                    "
                    ;
  $sql_update = "INSERT INTO update_risk_status
                             (dep_code,risk_opportunity,reference_no, risk_description, period_from, quarter,
                              prior_impact_score, prior_likelihood_score,
                              prior_overall_score, current_impact_score, current_likelihood_score,
                              current_overall_score,comments_updates_monitoring,status,risk_status,updated_by, date_updated)

                              VALUES

                              ('".$department_code."','".$risk_opportunity."','".$risk_reference."',
                                '".$risk_description."', '".$empty_quarter_and_year."', '".$empty_quarter_and_year."',
                                 '".$empty_scores."', '".$empty_scores."',
                              '".$empty_scores."', '".$empty_scores."', '".$empty_scores."',
                              '".$empty_scores."','".$comments_updates_monitoring."','".$status."','".$risk_status."','".$created_by."', '".$date_recorded."')

                              ";
    $query_insert = mysqli_query($dbc,$sql_insert);
    $query_update = mysqli_query($dbc,$sql_update);

      foreach ($_POST['risk_drivers'] as $row=>$selectedOption)
      {
        $driver = mysqli_real_escape_string($dbc,$_POST['risk_drivers'][$row]);
        $kri = mysqli_real_escape_string($dbc,$_POST['key_risk_indicator'][$row]);
        $threshold = mysqli_real_escape_string($dbc,$_POST['kri_threshold'][$row]);
        $treatment = mysqli_real_escape_string($dbc,$_POST['treatment_action'][$row]);

        $query = mysqli_query($dbc,"
                    INSERT INTO risk_drivers
                    (risk_reference,risk_drivers,key_risk_indicator,kri_threshold,treatment_action)
                    VALUES
                    ('".$risk_reference."','".$driver."','".$kri."','".$threshold."',
                    '".$treatment."')

                ");

      }


      //log the action
      $action_reference = "Added a ".$risk_opportunity. " with the description " . $risk_description;
      $action_name = $risk_opportunity." Adding";
      $action_icon = "fal fa-file-plus text-success";
      $page_id = "monitor-risks-link";
      $time_recorded = date('Y/m/d H:i:s');

      $sql_log = mysqli_query($dbc,"INSERT INTO activity_logs
                            (email,action_name,action_reference,action_icon,page_id,time_recorded)
                            VALUES
                          ('".$_SESSION['email']."','".$action_name."','".$action_reference."',
                          '".$action_icon."','".$page_id."','".$time_recorded."')"
                );



      if($query_insert && $query_update && $query && $sql_log)
      {
        mysqli_query($dbc,"COMMIT");
        echo "success";
      }
      else
      {
        echo mysqli_error($dbc);
        mysqli_query($dbc,"ROLLBACK");
      }


}

?>
