<?php
require_once('../setup/connect.php');
session_start();

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
    $risk_description =  mysqli_real_escape_string($dbc,strip_tags($_POST['risk_description']));
    $risk_or_opportunity_notification = mysqli_real_escape_string($dbc,strip_tags($_POST['risk_or_opportunity_notification']));
    $risk_reference = mysqli_real_escape_string($dbc,strip_tags($_POST['reference_no']));
    $person_responsible = mysqli_real_escape_string($dbc,strip_tags($_POST['quarterly_person_responsible']));
    $status = mysqli_real_escape_string($dbc,strip_tags($_POST['quarterly_approval_value']));
  //  $comments = mysqli_real_escape_string($dbc,strip_tags($_POST['comments']));

    if($status == "approved")
    {
      $mail_status = "approved";
    }
    if($status == "pending approval")
    {
      $mail_status = "pending approval";
    }
    if($status == "rejected")
    {
      $mail_status = "rejected";
    }

    $sql_insert = " UPDATE update_risk_status SET status='".$status."'  WHERE reference_no='".$risk_reference."' ";
    $query_insert = mysqli_query($dbc,$sql_insert);

    //log the action
    $action_reference = $status. " an updated risk /  opportunity with the reference_no ".$risk_reference;
    $action_name = "Risk Approvals";
    if($status == 'approved')
    {
      $action_icon = "far fa-check-circle text-success";
    }
    else
    {
      $action_icon = "fad fa-times-circle text-danger";
    }

    $page_id = "risk-approvals-link";
    $time_recorded = date('Y/m/d H:i:s');

    $sql_log = mysqli_query($dbc,"INSERT INTO activity_logs
                          (email,action_name,action_reference,action_icon,page_id,time_recorded)
                          VALUES
                        ('".$_SESSION['email']."','".$action_name."','".$action_reference."',
                        '".$action_icon."','".$page_id."','".$time_recorded."')"
              );

    if($query_insert && $sql_log)
    {
      //update risk driver table
      $sql_update_drivers = " UPDATE risk_drivers SET status='".$status."'  WHERE risk_reference='".$risk_reference."' ";
      if(mysqli_query($dbc,$sql_update_drivers))
      {
        exit("success");
      }
      else
      {
        exit("failed");
      }
      echo("success");
    }
    else
    {
      exit("failed");
    }


}


?>
