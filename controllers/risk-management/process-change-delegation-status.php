<?php

require_once('../setup/connect.php');
session_start();

if(isset($_REQUEST['sid']))
{
  $update = mysqli_query($dbc,"UPDATE delegations SET status='inactive' WHERE id='".$_POST['sid']."'");

  //log the action
  $action_reference = "Deactivated the Approval Delegation with the id ".$_POST['sid'];
  $action_name = "Delegation Deactivation";
  $action_icon = "far fa-handshake text-danger";
  $page_id = "delegate-approvals-link";
  $time_recorded = date('Y/m/d H:i:s');

  $sql_log = mysqli_query($dbc,"INSERT INTO activity_logs
                        (email,action_name,action_reference,action_icon,page_id,time_recorded)
                        VALUES
                      ('".$_SESSION['email']."','".$action_name."','".$action_reference."',
                      '".$action_icon."','".$page_id."','".$time_recorded."')"
            );

  if($update && $sql_log)
  {
    if(mysqli_affected_rows($dbc) > 0)
    {
      echo("success");
    }
    else
    {
       echo("failed");
    }
  }
}
?>
