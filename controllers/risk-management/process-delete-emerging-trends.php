<?php
require_once('../setup/connect.php');
session_start();
if(isset($_REQUEST["sid"]))
{
  $delete = "DELETE FROM emerging_trends WHERE id='".$_POST['sid']."'";

  //log the action
  $action_reference = "Deleted an Emerging Trend with the id ".$_POST['sid'];
  $action_name = "Emerging Trend deletion";
  $action_icon = "far fa-trash text-danger";
  $page_id = "emerging-trends-link";
  $time_recorded = date('Y/m/d H:i:s');

  $sql_log = mysqli_query($dbc,"INSERT INTO activity_logs
                        (email,action_name,action_reference,action_icon,page_id,time_recorded)
                        VALUES
                      ('".$_SESSION['email']."','".$action_name."','".$action_reference."',
                      '".$action_icon."','".$page_id."','".$time_recorded."')"
            );

	if($query = mysqli_query($dbc,$delete) && $sql_log)
    {
        if($affected = mysqli_affected_rows($dbc) > 0)
        {
           echo "success";
        }
        else
        {
            echo "failed";
        }

    }
    else
    {
        echo "Failed";
    }
}


?>
