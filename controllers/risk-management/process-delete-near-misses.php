<?php
require_once('../setup/connect.php');
session_start();
if($_SERVER['REQUEST_METHOD']== "POST")
{
  mysqli_query($dbc,"START TRANSACTION");

  $delete = mysqli_query($dbc,"DELETE FROM near_misses WHERE id='".$_POST['sid']."'");

  //log the action
  $action_reference = "Deleted a Near Miss with the id ".$_POST['sid'];
  $action_name = "Near Miss Deletion";
  $action_icon = "far fa-trash text-danger";
  $page_id = "lessons-learnt-link";
  $time_recorded = date('Y/m/d H:i:s');

  $sql_log = mysqli_query($dbc,"INSERT INTO activity_logs
                        (email,action_name,action_reference,action_icon,page_id,time_recorded)
                        VALUES
                      ('".$_SESSION['email']."','".$action_name."','".$action_reference."',
                      '".$action_icon."','".$page_id."','".$time_recorded."')"
            );

  if($delete && $sql_log)
  {
    echo "success";
    mysqli_query($dbc,"COMMIT");
  }
  else
  {
    echo mysqli_error($dbc);
    mysqli_query($dbc,"ROLLBACK");
  }

}
else
{
  echo "not posted";
}


?>
