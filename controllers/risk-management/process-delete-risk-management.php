<?php
session_start();
require_once('../setup/connect.php');
if($_SERVER['REQUEST_METHOD'] == "POST")
{
  $reference_no = $_POST['close_risk_reference'];
  $close_open = $_POST['close_open'];

  $period = $current_quarter_and_year['period'];
  $quarter = $current_quarter_and_year['quarter'];
  mysqli_query($dbc,"START TRANSACTION");

  $update_query = mysqli_query($dbc,"UPDATE risk_management set risk_status='".$close_open."' WHERE risk_reference='".$reference_no."'");

  //log the action
  if($close_open == "open")
  {
    $action_reference = "Opened  a risk/opportunity with the reference_no ".$reference_no;
    $action_name = "Risk/Opportunity re-activation";
    $action_icon = "far fa-trash-undo text-primary";
  }
  else
  {
    $action_reference = "Closed  a risk/opportunity with the reference_no ".$reference_no;
    $action_name = "Risk/Opportunity retirement";
    $action_icon = "far fa-trash text-danger";
  }
  $page_id = "monitor-risks-link";
  $time_recorded = date('Y/m/d H:i:s');

  $sql_log = mysqli_query($dbc,"INSERT INTO activity_logs
                        (email,action_name,action_reference,action_icon,page_id,time_recorded)
                        VALUES
                      ('".$_SESSION['email']."','".$action_name."','".$action_reference."',
                      '".$action_icon."','".$page_id."','".$time_recorded."')"
            );

  if($update_query)
  {
    if(mysqli_affected_rows($dbc) > 0)
    {
      mysqli_query($dbc,"COMMIT");
      echo "success";
    }
    else
    {
      echo "not updated";
    }
  }
  else
  {
    mysqli_query($dbc,"ROLLBACK");
    echo mysqli_error($dbc);
  }

/*
  $check_availability = mysqli_query($dbc,"SELECT reference_no FROM update_risk_status
                                                  WHERE reference_no='".$reference_no."'
                                                  && period_from='null' && quarter='null'");
  if(mysqli_num_rows($check_availability) > 0 )
  {
        $update_query = mysqli_query($dbc,"UPDATE update_risk_status set risk_status='".$close_open."'
                                                    WHERE reference_no='".$reference_no."'
                                                    && period_from='null' && quarter='null'");
        if($update_query)
        {
          if(mysqli_affected_rows($dbc) > 0)
          {
            mysqli_query($dbc,"COMMIT");
            echo "success";
          }
          else
          {
            echo "not updated";
          }
        }
        else
        {
          mysqli_query($dbc,"ROLLBACK");
          echo mysqli_error($dbc);
        }
  }
  else //no update, so close from register
  {
      $update_query = mysqli_query($dbc,"UPDATE update_risk_status set risk_status='".$close_open."'
                                                  WHERE reference_no='".$reference_no."'
                                                  && period_from='".$period."' && quarter='".$quarter."'");
      if($update_query)
      {
        if(mysqli_affected_rows($dbc) > 0)
        {
          mysqli_query($dbc,"COMMIT");
          echo "success";
        }
        else
        {
          echo "not updated";
        }
      }
      else
      {
        mysqli_query($dbc,"ROLLBACK");
        echo mysqli_error($dbc);
      }
  }
  */


}
else
{
    echo "not submitted";
}


?>
