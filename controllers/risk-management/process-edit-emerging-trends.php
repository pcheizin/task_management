<?php
require_once('../setup/connect.php');
session_start();
if($_SERVER['REQUEST_METHOD'] == 'POST')
{
   $dep_code = $_SESSION['department_code'];
   $factor= mysqli_real_escape_string($dbc,strip_tags($_POST['edit_factor']));
   $external_internal = mysqli_real_escape_string($dbc,strip_tags($_POST['edit_external_internal']));
   $related_risk_event = mysqli_real_escape_string($dbc,strip_tags($_POST['edit_related_risk_event']));
   $changes_in_risk_profile = mysqli_real_escape_string($dbc,strip_tags($_POST['edit_changes_in_risk_profile']));
   $period= mysqli_real_escape_string($dbc,strip_tags($_POST['edit_period']));
   $quarter = mysqli_real_escape_string($dbc,strip_tags($_POST['edit_quarter']));
   $created_by = $_SESSION['name'];
   $date_recorded = date("m/d/Y");
   $time_recorded = date("h:i:sa");
   $id = mysqli_real_escape_string($dbc,$_POST['id']);

   if($_SESSION['access_level'] == "director" || $_SESSION['designation'] == "Chief Executive" )
   {
     $is_corporate = "yes";
   }
   else
   {
     $is_corporate = "no";
   }


   mysqli_query($dbc,"START TRANSACTION");

   $update = mysqli_query($dbc,"UPDATE emerging_trends SET changed='yes' WHERE id='".$id."' && period='".$period."' && quarter='".$quarter."'");

   $insert = mysqli_query($dbc,"INSERT INTO emerging_trends
                                                (dep_code, period, quarter, factor, external_internal, related_risk_event,
                                                  changes_in_risk_profile,is_corporate, created_by, date_recorded, time_recorded)
                              VALUES

                              ('".$dep_code."','".$period."','".$quarter."','".$factor."','".$external_internal."','".$related_risk_event."',
                                '".$changes_in_risk_profile."','".$is_corporate."','".$created_by."','".$date_recorded."','".$time_recorded."') ");

  //log the action
$action_reference = "Modified an Emerging trend with the factor : ".$factor;
$action_name = "Emerging Trend Modification";
$action_icon = "far fa-file-edit text-warning";
$page_id = "emerging-trends-link";
$time_recorded = date('Y/m/d H:i:s');

$sql_log = mysqli_query($dbc,"INSERT INTO activity_logs
                        (email,action_name,action_reference,action_icon,page_id,time_recorded)
                        VALUES
            ('".$_SESSION['email']."','".$action_name."','".$action_reference."',
            '".$action_icon."','".$page_id."','".$time_recorded."')"
          );

    if($update && $insert && $sql_log)
    {
      echo "success";
      mysqli_query($dbc,"COMMIT");
    }
    else {
      echo mysqli_error($dbc);
      mysqli_query($dbc,"ROLLBACK");
    }


}
?>
