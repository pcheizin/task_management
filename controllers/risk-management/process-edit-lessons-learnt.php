<?php
require_once('../setup/connect.php');
session_start();
if($_SERVER['REQUEST_METHOD'] == 'POST')
{

  if($_SESSION['access_level'] == "director" || $_SESSION['designation'] == "Chief Executive" )
  {
    $is_corporate = "yes";
  }
  else
  {
    $is_corporate = "no";
  }

  // FOR STRATEGIES THAT WORKED WELL
  if(isset($_POST['edit_strategies_that_worked_well']))
  {
    $id = mysqli_real_escape_string($dbc,$_POST['id']);
    $period= mysqli_real_escape_string($dbc,strip_tags($_POST['edit_period']));
    $quarter = mysqli_real_escape_string($dbc,strip_tags($_POST['edit_quarter']));
    $strategies_that_worked_well = mysqli_real_escape_string($dbc,strip_tags($_POST['edit_strategies_that_worked_well']));
    $created_by = $_SESSION['name'];
    $dep_code = $_SESSION['department_code'];
    $date_recorded = date("m/d/Y");
    $time_recorded = date("h:i:sa");

    $update_query = mysqli_query($dbc,"UPDATE strategies_that_worked_well SET changed='yes' WHERE id='".$id."' && period='".$period."' && quarter='".$quarter."'");

    if($update_query)
    {
      //insert values to database
      $sql_statement = "INSERT INTO strategies_that_worked_well

                          (strategies_that_worked_well,dep_code,period,quarter,is_corporate,created_by,date_recorded,time_recorded)

                        VALUES
                            ('".$strategies_that_worked_well."', '".$dep_code."','".$period."', '".$quarter."','".$is_corporate."', '".$created_by."',
                               '".$date_recorded."', '".$time_recorded."')
                          ";

      //check if query runs

      //log the action
    $action_reference = "Modified the Strategy that worked well : ".$strategies_that_worked_well;
    $action_name = "Lessons Learnt Modification";
    $action_icon = "far fa-file-edit text-warning";
    $page_id = "lessons-learnt-link";
    $time_recorded = date('Y/m/d H:i:s');

    $sql_log = mysqli_query($dbc,"INSERT INTO activity_logs
                            (email,action_name,action_reference,action_icon,page_id,time_recorded)
                            VALUES
                ('".$_SESSION['email']."','".$action_name."','".$action_reference."',
                '".$action_icon."','".$page_id."','".$time_recorded."')"
              );

      if($insert_query = mysqli_query($dbc,$sql_statement) && $sql_log)
      {
          exit ("success");
      }
      else
      {
          exit ("failed");
      }
    }
    else {
      exit("failed");
    }
  }
    //FOR STRATEGIES THAT DID NOT WORK
    if(isset($_POST['edit_strategies_that_did_not_work']))
    {
      $id = mysqli_real_escape_string($dbc,$_POST['id']);
      $period= mysqli_real_escape_string($dbc,strip_tags($_POST['edit_period']));
      $quarter = mysqli_real_escape_string($dbc,strip_tags($_POST['edit_quarter']));
      $strategies_that_did_not_work = mysqli_real_escape_string($dbc,strip_tags($_POST['edit_strategies_that_did_not_work']));
      $created_by = $_SESSION['name'];
      $dep_code = $_SESSION['department_code'];
      $date_recorded = date("m/d/Y");
      $time_recorded = date("h:i:sa");

      $update_query = mysqli_query($dbc,"UPDATE strategies_that_did_not_work SET changed='yes' WHERE id='".$id."' && period='".$period."' && quarter='".$quarter."'");

      if($update_query)
      {
        //insert values to database
        $sql_statement = "INSERT INTO strategies_that_did_not_work

                            (strategies_that_did_not_work,dep_code,period,quarter,is_corporate,created_by,date_recorded,time_recorded)

                          VALUES
                              ('".$strategies_that_did_not_work."', '".$dep_code."','".$period."', '".$quarter."','".$is_corporate."', '".$created_by."',
                                 '".$date_recorded."', '".$time_recorded."')
                            ";

        //check if query runs

        //log the action
      $action_reference = "Modified the Strategy that did not work : ".$strategies_that_did_not_work;
      $action_name = "Lessons Learnt Modification";
      $action_icon = "far fa-file-edit text-warning";
      $page_id = "lessons-learnt-link";
      $time_recorded = date('Y/m/d H:i:s');

      $sql_log = mysqli_query($dbc,"INSERT INTO activity_logs
                              (email,action_name,action_reference,action_icon,page_id,time_recorded)
                              VALUES
                  ('".$_SESSION['email']."','".$action_name."','".$action_reference."',
                  '".$action_icon."','".$page_id."','".$time_recorded."')"
                );

        if($insert_query = mysqli_query($dbc,$sql_statement) && $sql_log)
        {
            exit ("success");
        }
        else
        {
            exit ("failed");
        }
      }
      else {
        exit("failed");
      }
    }

    //START FOR NEAR MISSES
    if(isset($_POST['edit_near_misses']))
    {
      $id = mysqli_real_escape_string($dbc,$_POST['id']);
      $period= mysqli_real_escape_string($dbc,strip_tags($_POST['edit_period']));
      $quarter = mysqli_real_escape_string($dbc,strip_tags($_POST['edit_quarter']));
      $near_misses = mysqli_real_escape_string($dbc,strip_tags($_POST['edit_near_misses']));
      $created_by = $_SESSION['name'];
      $dep_code = $_SESSION['department_code'];
      $date_recorded = date("m/d/Y");
      $time_recorded = date("h:i:sa");

      $update_query = mysqli_query($dbc,"UPDATE near_misses SET changed='yes' WHERE id='".$id."' && period='".$period."' && quarter='".$quarter."'");

      if($update_query)
      {
        //insert values to database
        $sql_statement = "INSERT INTO near_misses

                            (near_misses,dep_code,period,quarter,is_corporate,created_by,date_recorded,time_recorded)

                          VALUES
                              ('".$near_misses."', '".$dep_code."','".$period."', '".$quarter."','".$is_corporate."', '".$created_by."',
                                 '".$date_recorded."', '".$time_recorded."')
                            ";

        //log the action
        $action_reference = "Modified the Near Miss : ".$near_misses;
        $action_name = "Lessons Learnt Modification";
        $action_icon = "far fa-file-edit text-warning";
        $page_id = "lessons-learnt-link";
        $time_recorded = date('Y/m/d H:i:s');

        $sql_log = mysqli_query($dbc,"INSERT INTO activity_logs
                          (email,action_name,action_reference,action_icon,page_id,time_recorded)
                                                  VALUES
                          ('".$_SESSION['email']."','".$action_name."','".$action_reference."',
                                      '".$action_icon."','".$page_id."','".$time_recorded."')"
                          );

        //check if query runs

        if($insert_query = mysqli_query($dbc,$sql_statement) && $sql_log)
        {
            exit ("success");
        }
        else
        {
            exit ("failed");
        }
      }
      else {
        exit("failed");
      }
    }


}

?>
