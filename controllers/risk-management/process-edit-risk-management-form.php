<?php
session_start();
require_once('../setup/connect.php');

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
    $reference_no = mysqli_real_escape_string($dbc,strip_tags($_POST['reference_no']));
    $department_code = mysqli_real_escape_string($dbc,strip_tags($_POST['department_code']));
    $risk_opportunity = mysqli_real_escape_string($dbc,strip_tags($_POST['risk_opportunity']));
    $risk_or_opportunity = mysqli_real_escape_string($dbc,strip_tags($_POST['risk_or_opportunity']));
    $risk_reference = $department_code."/".$risk_or_opportunity."/".$reference_no;
    $strategic_objective = mysqli_real_escape_string($dbc,strip_tags($_POST['strategic_objective']));
    //$department = mysqli_real_escape_string($dbc,strip_tags($_POST['select_department']));
    $departmental_objective = mysqli_real_escape_string($dbc,strip_tags($_POST['departmental_objective']));
    //$departmental_sub_objective =implode(" , ",$_POST['departmental_sub_objectives']);
    $risk_description = mysqli_real_escape_string($dbc,strip_tags($_POST['risk_description']));
    $impact = mysqli_real_escape_string($dbc,strip_tags($_POST['impact']));
    $impact_score = mysqli_real_escape_string($dbc,strip_tags($_POST['impact_score']));
    $likelihood_score = mysqli_real_escape_string($dbc,strip_tags($_POST['likelihood_score']));
    $overall_score = mysqli_real_escape_string($dbc,strip_tags($_POST['overall_score']));
    //$person_responsible = mysqli_real_escape_string($dbc,strip_tags($_POST['person_responsible']));
    $person_responsible = implode(" , ",$_POST['person_responsible']);
    $created_by = $_SESSION['name'];
    $date_recorded = date("m/d/Y");
    $time_recorded = date("h:i:sa");

    //start transaction
    mysqli_query($dbc,"START TRANSACTION");

    $sql_update = "UPDATE risk_management SET changed='yes' WHERE risk_reference='".$risk_reference."'";

    $sql_update1 = mysqli_query($dbc,"UPDATE  approval_comments_new_edited SET viewed ='yes' WHERE reference_no ='".$risk_reference."'") or die ("failed");

    $run_update_query = mysqli_query($dbc,$sql_update) or die ("failed");
    $sql_update_driver = mysqli_query($dbc,"UPDATE risk_drivers SET changed='yes' WHERE risk_reference='".$risk_reference."'
                           && quarterly_update='no'");

                    $sql_insert = "INSERT INTO risk_management
                    (reference_no,department_code,risk_opportunity,risk_reference,strategic_objective_id,
                    departmental_objective_id,risk_description,
                    impact,impact_score,likelihood_score,overall_score, person_responsible,
                    created_by,date_recorded,time_recorded)

                    VALUES

                    (
                    '".$reference_no."', '".$department_code."', '".$risk_opportunity."', '".$risk_reference."', '".$strategic_objective."',
                    '".$departmental_objective."', '".$risk_description."',
                    '".$impact."', '".$impact_score."', '".$likelihood_score."', '".$overall_score."', '".$person_responsible."',
                    '".$created_by."', '".$date_recorded."', '".$time_recorded."'
                    )

                    "
                    ;
                    $query_insert = mysqli_query($dbc,$sql_insert);

                         foreach ($_POST['risk_drivers'] as $row=>$selectedOption)
                         {
                           $driver = mysqli_real_escape_string($dbc,$_POST['risk_drivers'][$row]);
                           $kri = mysqli_real_escape_string($dbc,$_POST['key_risk_indicator'][$row]);
                           $threshold = mysqli_real_escape_string($dbc,$_POST['kri_threshold'][$row]);
                           $treatment = mysqli_real_escape_string($dbc,$_POST['treatment_action'][$row]);
                           $strategy = mysqli_real_escape_string($dbc,$_POST['edit_risk_management_strategy_undertaken'][$row]);
                           $effects = mysqli_real_escape_string($dbc,$_POST['edit_effects_of_risk_to_authority'][$row]);
                           $action = mysqli_real_escape_string($dbc,$_POST['edit_action_to_be_undertaken'][$row]);
                           $kri_level = mysqli_real_escape_string($dbc,$_POST['current_kri_level'][$row]);

                           $query = mysqli_query($dbc,"
                                       INSERT INTO risk_drivers
                                       (risk_reference,risk_drivers,key_risk_indicator,current_kri_level,kri_threshold,treatment_action,
                                       risk_management_strategy_undertaken,effects_of_risk_to_authority,action_to_be_undertaken)
                                       VALUES
                                       ('".$risk_reference."','".$driver."','".$kri."','".$kri_level."','".$threshold."',
                                       '".$treatment."','".$strategy."','".$effects."','".$action."')

                                   ");

                         }

                         //log the action
                       $action_reference = "Modified the ". $risk_opportunity. " with the reference_no ". $reference_no;
                       $action_name = $risk_opportunity. " Modification";
                       $action_icon = "far fa-file-edit text-warning";
                       $page_id = "monitor-risks-link";
                       $time_recorded = date('Y/m/d H:i:s');

                       $sql_log = mysqli_query($dbc,"INSERT INTO activity_logs
                                               (email,action_name,action_reference,action_icon,page_id,time_recorded)
                                               VALUES
                                   ('".$_SESSION['email']."','".$action_name."','".$action_reference."',
                                   '".$action_icon."','".$page_id."','".$time_recorded."')"
                                 );


                    if($query_insert && $query && $sql_update_driver && $run_update_query && $sql_log && $sql_update1)
                    {
                      mysqli_query($dbc,"COMMIT");
                      echo "success";
                    }
                    else
                    {
                      mysqli_query($dbc,"ROLLBACK");

                      echo mysqli_error($dbc);
                    }

}


?>
