<?php
require_once('../setup/connect.php');
session_start();
if($_SERVER['REQUEST_METHOD'] == 'POST')
{
    //collect form fields
    $delegated_to = mysqli_real_escape_string($dbc,strip_tags($_POST['delegated_to']));
    $delegated_start_date = mysqli_real_escape_string($dbc,strip_tags($_POST['delegation_start_date']));
    $delegated_end_date = mysqli_real_escape_string($dbc,strip_tags($_POST['delegation_end_date']));
    $delegated_from_name = mysqli_real_escape_string($dbc,strip_tags($_POST['delegated_from_name']));
    $date_recorded = date("m/d/Y");
    $time_recorded = date("h:i:sa");


    // select linked email to the delegated from name
    $sql_delegated_email = mysqli_fetch_array(mysqli_query($dbc,"SELECT * FROM staff_users WHERE Name='".$delegated_from_name."'"));
    if($sql_delegated_email)
    {
      $delegated_from_email = $sql_delegated_email['Email'];
    }
    else
    {
      exit("failed");
    }
    //select linked name from staff users, and fetch all its records
    $sql_select = mysqli_fetch_array(mysqli_query($dbc,"SELECT * FROM staff_users WHERE Name='".$delegated_to."'"));
    if($sql_select)
    {
      $delegated_to_dep_code = $sql_select['DepartmentCode'];
      $delegated_to_email =  $sql_select['Email'];
    }
    else
    {
      exit("failed");
    }
    //insert values to database
    ini_set("SMTP","cmacas.cma.local");
    ini_set('smtp_port', 25);
    $headers = "MIME-Version: 1.0" . "\r\n";
    $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";

          // More headers
    $headers .= 'From: PPRMIS SYSTEM <PRisk@cma.or.ke>' . "\r\n";
    $subject = 'PPRMIS Approval Delegation Notification';
    $recipients = array(
                  $delegated_from_email,
                  $delegated_to_email
                  // more emails
                  );
    $email_to = implode(',', $recipients);
    $message =  "Dear <b>".$delegated_to."</b>, <br/><br/><br/>
                <b>".$delegated_from_name."</b> has delegated Risk Approvals to <b>".$delegated_to."</b> from <b>".$delegated_start_date."</b>,
                to <b>".$delegated_end_date."</b>.
               <br/><br/><br/><br/><br/>
              <b>This is an automated message, please do not reply</b>";


    $send_mail = mail($email_to,$subject,$message,$headers);
    if($send_mail)
    {
      $date_sent = date("d-m-Y h:i:sa");
      $message = mysqli_real_escape_string($dbc,$message);
      $store_sent_mail = mysqli_query($dbc,"INSERT INTO sent_mails (sent_from,sent_to,triggered_by,message_subject,message_body,date_sent)
                                                    VALUES
                                                    ('".$headers."','".$email_to."','".$_SESSION['name']."',
                                                      '".$subject."','".$message."','".$date_sent."')"
                                     ) or die (mysqli_error($dbc));

      exit ("success");
    }
    else
    {
      exit ("mail not sent");
    }
}

?>
