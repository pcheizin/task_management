<?php
session_start();
require_once('../setup/connect.php');

$reference_no = mysqli_real_escape_string($dbc,strip_tags($_POST['reference_no']));
$department_code = mysqli_real_escape_string($dbc,strip_tags($_POST['department_code']));
$risk_opportunity = mysqli_real_escape_string($dbc,strip_tags($_POST['risk_opportunity']));
$risk_or_opportunity = mysqli_real_escape_string($dbc,strip_tags($_POST['risk_or_opportunity']));
$risk_reference = $department_code."/".$risk_or_opportunity."/".$reference_no;
$risk_description = mysqli_real_escape_string($dbc,strip_tags($_POST['risk_description']));
$person_responsible = implode(" , ",$_POST['person_responsible']);
$created_by = $_SESSION['name'];

$searchString = ',';

if( strpos($person_responsible, $searchString) !== false )
{
  $one_person_to_approve = substr($person_responsible, 0, strpos($person_responsible, ','));
  $fetch_department_email = mysqli_fetch_array(mysqli_query($dbc,"SELECT staff_users.Email AS email,staff_users.designation AS designation FROM staff_users
                                                                  WHERE staff_users.designation='".$one_person_to_approve."' LIMIT 1"));
  $specific_email = $fetch_department_email['email'];
}
  else
{
  $one_person_to_approve = $person_responsible;
  $fetch_department_email = mysqli_fetch_array(mysqli_query($dbc,"SELECT staff_users.Email AS email,staff_users.designation AS designation FROM staff_users
                                                                  WHERE staff_users.designation='".$one_person_to_approve."' LIMIT 1"));
  $specific_email = $fetch_department_email['email'];
}
  $subject = 'PRMIS Risk Notification';
  $message = "Dear <b>".$person_responsible."</b>, <br/><br/><br/>
  Please login to <a href='https://pprmis.cma.or.ke/prmis/pages/risk-management/update-risk-status.php'>PPRMIS</a>  to view the <b>".$risk_opportunity."</b> with the
  reference no <b>".$risk_reference."</b>,  description
  <b>".$risk_description."</b> created by <b>$created_by</b> that is pending approval.
  <br/><br/><br/>
  <b>This is an automated message, please do not reply</b>";
  $send_mail = mail($specific_email,$subject,$message,$headers);
  if($send_mail)
  {
    $date_sent = date("d-m-Y h:i:sa");
    $message = mysqli_real_escape_string($dbc,$message);
    $store_sent_mail = mysqli_query($dbc,"INSERT INTO sent_mails (sent_from,sent_to,triggered_by,message_subject,message_body,date_sent)
                                                  VALUES
                                                  ('".$headers."','".$specific_email."','".$_SESSION['name']."','".$subject."','".$message."','".$date_sent."')"
                                   ) or die (mysqli_error($dbc));
    exit("success");
  }
  else
  {
    exit("mail not sent");
  }

 ?>
