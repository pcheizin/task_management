
<?php
require_once('../setup/connect.php');
session_start();

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
    $risk_description =  mysqli_real_escape_string($dbc,strip_tags($_POST['new_risk_description']));
    $risk_or_opportunity_notification = mysqli_real_escape_string($dbc,strip_tags($_POST['new_risk_or_opportunity_notification']));
    $risk_reference = mysqli_real_escape_string($dbc,strip_tags($_POST['new_reference_no']));
    $person_responsible = mysqli_real_escape_string($dbc,strip_tags($_POST['new_person_responsible']));
    $status = mysqli_real_escape_string($dbc,strip_tags($_POST['new_approval_value']));
  //  $comments = mysqli_real_escape_string($dbc,strip_tags($_POST['comments']));

    if($status == "approved")
    {
      $mail_status = "approved";
    }
    if($status == "pending approval")
    {
      $mail_status = "pending approval";
    }
    if($status == "rejected")
    {
      $mail_status = "rejected";
    }

                              $searchString = ',';

                              if( strpos($person_responsible, $searchString) !== false ) {
                                  //echo "Found";
                                  $one_person_to_approve = substr($person_responsible, 0, strpos($person_responsible, ','));
                                  $fetch_department_email = mysqli_fetch_array(mysqli_query($dbc,"SELECT staff_users.Email AS email,staff_users.designation AS designation FROM staff_users
                                                                                                  WHERE staff_users.designation='".$one_person_to_approve."' LIMIT 1"));
                                  $specific_email = $fetch_department_email['email'];
                                  $specific_title = $fetch_department_email['designation'];
                              }
                              else
                               {
                                 $one_person_to_approve = $person_responsible;
                                 $fetch_department_email = mysqli_fetch_array(mysqli_query($dbc,"SELECT staff_users.Email AS email,staff_users.designation AS designation FROM staff_users
                                                                                                 WHERE staff_users.designation='".$one_person_to_approve."' LIMIT 1"));
                                 $specific_email = $fetch_department_email['email'];
                                 $specific_title = $fetch_department_email['designation'];
                              }

                              $subject = 'PPRMIS Risk Approval Notification';
                              $message = 'Dear  ' .$specific_title. '</b>, <br/><br/><br/>
                                          The <b> '.$risk_or_opportunity_notification .' </b> with the reference no <b> ' .$risk_reference.' </b>, description
                                         <b> '.$risk_description. '</b> has  been <b> '.$mail_status. ' </b>.
                                         <br/><br/>
                                         <br/><br/><br/>
                                        <b>This is an automated message, please do not reply</b>';

                          //send a mail if risk is approved and edit is made
                          $find_status = mysqli_fetch_array(mysqli_query($dbc,"SELECT * FROM risk_management WHERE changed='no' && risk_reference='".$risk_reference."'"));
                          if($find_status['status']== 'approved' || $find_status['status'] == 'pending approval' || $find_status['status'] == 'rejected')
                          {
                            mail($specific_email,$subject,$message,$headers) or die ("mail not sent");
                            $date_sent = date("d-m-Y h:i:sa");
                            $message = mysqli_real_escape_string($dbc,$message);
                            $store_sent_mail = mysqli_query($dbc,"INSERT INTO sent_mails (sent_from,sent_to,triggered_by,message_subject,message_body,date_sent)
                                                                          VALUES
                                                                          ('".$headers."','".$specific_email."','".$_SESSION['name']."',
                                                                            '".$subject."','".$message."','".$date_sent."')"
                                                           ) or die (mysqli_error($dbc));
                          }

                          //send mail to risk creater
                          $find_last_modified_by = mysqli_fetch_array(mysqli_query($dbc,"SELECT * FROM risk_management WHERE changed='no' && risk_reference='".$risk_reference."' ORDER BY id DESC LIMIT 1"));
                          $find_risk_creator = mysqli_fetch_array(mysqli_query($dbc,"SELECT * FROM staff_users WHERE Name='".$find_last_modified_by['created_by']."'"));
                          $email_subject = "PRMIS Risk Approval Notification";
                          $risk_creator_email = $find_risk_creator['Email'];
                          $risk_creator_name = $find_risk_creator['Name'];
                          $message_to_risk_creator = "Dear <b>$risk_creator_name</b>, <br/><br/><br/>
                                      The <b>$risk_or_opportunity_notification</b> with the reference no <b>$risk_reference</b>, description
                                     <b>$risk_description</b> has  been <b>$mail_status</b>.
                                     <br/><br/><br/>
                                    <b>This is an automated message, please do not reply</b>";

                          //mail($risk_creator_email,$email_subject,$message_to_risk_creator,$headers) or die ("mail not sent");
                          $send_mail = mail($risk_creator_email,$email_subject,$message_to_risk_creator,$headers);
                          if($send_mail)
                          {
                            $date_sent = date("d-m-Y h:i:sa");
                            $message = mysqli_real_escape_string($dbc,$message);
                            $store_sent_mail = mysqli_query($dbc,"INSERT INTO sent_mails (sent_from,sent_to,triggered_by,message_subject,message_body,date_sent)
                                                                          VALUES
                                                                          ('".$headers."','".$risk_creator_email."',
                                                                            '".$_SESSION['name']."','".$email_subject."',
                                                                            '".$message_to_risk_creator."','".$date_sent."')"
                                                           ) or die (mysqli_error($dbc));
                            exit("success");
                          }
                          else {
                            exit("mail not sent");
                          }

                          //end send mail to risk creator


}


?>
