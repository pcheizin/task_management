<?php
session_start();
require_once('../setup/connect.php');
// find last inserted record in database, and send mail
/*
$last_row_inserted = mysqli_fetch_array(mysqli_query($dbc, "SELECT * FROM risk_management WHERE
                                                      department_code='".$_SESSION['department_code']."'
                                                      ORDER BY id DESC LIMIT 1"));

//set variables
$person_responsible = $last_row_inserted['person_responsible'];
$risk_reference = $last_row_inserted['risk_reference'];
$risk_description = $last_row_inserted['risk_description'];
$created_by = $last_row_inserted['created_by'];
$risk_opportunity = $last_row_inserted['risk_opportunity'];
*/
$risk_description = mysqli_real_escape_string($dbc,strip_tags($_POST['risk_description']));
$reference_no = mysqli_real_escape_string($dbc,strip_tags($_POST['reference_no']));
$department_code = mysqli_real_escape_string($dbc,strip_tags($_POST['department_code']));
$risk_opportunity = mysqli_real_escape_string($dbc,strip_tags($_POST['risk_opportunity']));
$risk_or_opportunity = mysqli_real_escape_string($dbc,strip_tags($_POST['risk_or_opportunity']));
$risk_reference = $department_code."/".$risk_or_opportunity."/".$reference_no;

$person_responsible = implode(" , ",$_POST['person_responsible']);
$created_by = $_SESSION['name'];
$date_recorded = date("m/d/Y");
$time_recorded = date("h:i:sa");
$searchString = ',';

if( strpos($person_responsible, $searchString) !== false )
{
  $one_person_to_approve = substr($person_responsible, 0, strpos($person_responsible, ','));
  $fetch_department_email = mysqli_fetch_array(mysqli_query($dbc,"SELECT staff_users.Email AS email,staff_users.designation AS designation FROM staff_users
                                                                  WHERE staff_users.designation='".$one_person_to_approve."' LIMIT 1"));
  $specific_email = $fetch_department_email['email'];
  $specific_title = $fetch_department_email['designation'];
}
else
{
  $one_person_to_approve = $person_responsible;
  $fetch_department_email = mysqli_fetch_array(mysqli_query($dbc,"SELECT staff_users.Email AS email,staff_users.designation AS designation FROM staff_users
                                                                  WHERE staff_users.designation='".$one_person_to_approve."' LIMIT 1"));
  $specific_email = $fetch_department_email['email'];
  $specific_title = $fetch_department_email['designation'];
}


                          $last_record = mysqli_fetch_array(mysqli_query($dbc,"SELECT * FROM risk_management WHERE risk_reference='".$risk_reference."' && changed='yes' ORDER BY id DESC LIMIT 1"));

                          $recent_record = mysqli_fetch_array(mysqli_query($dbc,"SELECT * FROM risk_management WHERE risk_reference='".$risk_reference."' && changed='no'"));

                          if($last_record['risk_description']!=$recent_record['risk_description'])
                          {
                            $risk_reference_changed = " - Risk Description changed from <b>". $last_record['risk_description'] ."</b>  to <b>". $recent_record['risk_description'] ."</b><br/><br/>";
                          }
                          else
                          {
                            $risk_reference_changed = "";
                          }

                          if($last_record['strategic_objective']!=$recent_record['strategic_objective'])
                          {
                            $strategic_objective_changed = " - Strategic Objective changed from <b>". $last_record['strategic_objective'] ."</b> to <b>". $recent_record['strategic_objective'] ."</b><br/><br/>";
                          }
                          else
                          {
                            $strategic_objective_changed = "";
                          }

                          if($last_record['departmental_objective']!=$recent_record['departmental_objective'])
                          {
                            $departmental_objective_changed = " - Departmental Objective changed from <b>". $last_record['departmental_objective'] ."</b>  to <b>". $recent_record['departmental_objective'] ."</b><br/><br/>";
                          }
                          else
                          {
                            $departmental_objective_changed = "";
                          }

                          if($last_record['departmental_sub_objective']!=$recent_record['departmental_sub_objective'])
                          {
                            $departmental_sub_objective_changed = " - Departmental Sub Objective changed from <b>". $last_record['departmental_sub_objective'] ."</b> to <b>". $recent_record['departmental_sub_objective'] ."</b><br/><br/>";
                          }
                          else
                          {
                            $departmental_sub_objective_changed = "";
                          }

                          if($last_record['impact']!=$recent_record['impact'])
                          {
                            $impact_changed = " - Impact changed from <b>". $last_record['impact'] ."</b> to <b>". $recent_record['impact'] ."</b><br/><br/>";
                          }
                          else
                          {
                            $impact_changed = "";
                          }

                          if($last_record['overall_score']!=$recent_record['overall_score'])
                          {
                            $overall_score_changed = " - Overall Score changed from <b>". $last_record['overall_score'] ."</b> to <b>". $recent_record['overall_score'] ."</b><br/><br/>";
                          }
                          else
                          {
                            $overall_score_changed = "";
                          }
                          if($last_record['person_responsible']!=$recent_record['person_responsible'])
                          {
                            $person_responsible_changed = " - Person responsible changed from <b>". $last_record['person_responsible'] ."</b> to <b>". $recent_record['person_responsible'] ."</b><br/><br/>";
                          }
                          else
                          {
                            $person_responsible_changed = "";
                          }


                        //mail(to,subject,message,headers,parameters);
                        $subject = 'PPRMIS Risk Modification Notification';
                        $message = "Dear <b>".$specific_title."</b>, <br/><br/><br/>
                                    The risk with the reference no <b>".$risk_reference."</b>, description
                                   <b>".$risk_description."</b> has  been modified
                                   <br/><br/>
                                    ".$risk_reference_changed."
                                    ".$strategic_objective_changed."
                                    ".$departmental_objective_changed."
                                    ".$departmental_sub_objective_changed."
                                    ".$impact_changed."
                                    ".$overall_score_changed."
                                    ".$person_responsible_changed."
                                   Please log in to <a href='https://pprmis.cma.or.ke/prmis/pages/risk-management/update-risk-status.php'>PPRMIS</a>  to view it.
                                   <br/><br/><br/>
                                  <b>This is an automated message, please do not reply</b>";

                    //send a mail if risk is approved and edit is made
                    $find_status = mysqli_fetch_array(mysqli_query($dbc,"SELECT * FROM risk_management WHERE changed='no' && risk_reference='".$risk_reference."'"));
                    if($find_status['status']== 'approved')
                    {
                      mail($specific_email,$subject,$message,$headers) or die ("mail not sent");
                      $date_sent = date("d-m-Y h:i:sa");
                      $message = mysqli_real_escape_string($dbc,$message);
                      $store_sent_mail = mysqli_query($dbc,"INSERT INTO sent_mails (sent_from,sent_to,triggered_by,message_subject,message_body,date_sent)
                                                                    VALUES
                                                                    ('".$headers."','".$specific_email."','".$_SESSION['name']."','".$subject."','".$message."','".$date_sent."')"
                                                     ) or die (mysqli_error($dbc));
                    }
                    else
                    {
                        echo(mysqli_error($dbc));
                    }

                    //send mail to risk creater
                    $find_last_modified_by = mysqli_fetch_array(mysqli_query($dbc,"SELECT * FROM risk_management WHERE changed='yes' && risk_reference='".$risk_reference."' ORDER BY id DESC LIMIT 1"));
                    $find_risk_creator = mysqli_fetch_array(mysqli_query($dbc,"SELECT * FROM staff_users WHERE Name='".$find_last_modified_by['created_by']."'"));
                    $email_subject = "PPRMIS Risk Modification Notification";
                    $risk_creator_email = $find_risk_creator['Email'];
                    $risk_creator_name = $find_risk_creator['Name'];
                    $message_to_risk_creator = "Dear <b>".$risk_creator_name."</b>, <br/><br/><br/>
                                The risk with the reference no <b>".$risk_reference."</b>, description
                               <b>".$risk_description."</b> has  been modified.
                               <br/><br/>
                               ".$risk_reference_changed."
                               ".$strategic_objective_changed."
                               ".$departmental_objective_changed."
                               ".$departmental_sub_objective_changed."
                               ".$impact_changed."
                               ".$overall_score_changed."
                               ".$person_responsible_changed."
                               Please log in to <a href='https://pprmis.cma.or.ke/prmis/pages/risk-management/update-risk-status.php'>PPRMIS</a> to view it.
                               <br/><br/><br/>
                              <b>This is an automated message, please do not reply</b>";

                    mail($risk_creator_email,$email_subject,$message_to_risk_creator,$headers) or die ("mail not sent");
                    $date_sent = date("d-m-Y h:i:sa");
                    $message_to_risk_creator = mysqli_real_escape_string($dbc,$message_to_risk_creator);
                    $store_sent_mail = mysqli_query($dbc,"INSERT INTO sent_mails (sent_from,sent_to,triggered_by,message_subject,message_body,date_sent)
                                                                  VALUES
                                                                  ('".$headers."','".$risk_creator_email."','".$_SESSION['name']."',
                                                                    '".$email_subject."','".$message_to_risk_creator."','".$date_sent."')"
                                                   ) or die (mysqli_error($dbc));

                    //end send mail to risk creator
 ?>
