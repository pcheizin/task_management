<?php
session_start();
require_once('../setup/connect.php');

$dep_code = mysqli_real_escape_string($dbc,strip_tags($_POST['dep_code']));
$risk_opportunity = mysqli_real_escape_string($dbc,strip_tags($_POST['risk_opportunity_quarter']));
$reference_no = mysqli_real_escape_string($dbc,strip_tags($_POST['reference_no']));
$risk_description = mysqli_real_escape_string($dbc,strip_tags($_POST['risk_description']));
$period_from = mysqli_real_escape_string($dbc,strip_tags($_POST['period_from']));
$quarter = mysqli_real_escape_string($dbc,strip_tags($_POST['quarter']));
//$person_responsible = implode(" , ",$_POST['person_responsible']);
$person_responsible = mysqli_real_escape_string($dbc,strip_tags($_POST['person_responsible_for_risk']));
$created_by = $_SESSION['name'];

//after risk is inserted to database, send an email
$searchString = ',';

if( strpos($person_responsible, $searchString) !== false ) {
    //echo "Found";
    $one_person_to_approve = substr($person_responsible, 0, strpos($person_responsible, ','));
    $fetch_department_email = mysqli_fetch_array(mysqli_query($dbc,"SELECT staff_users.Email AS email,staff_users.designation AS designation FROM staff_users
                                                                    WHERE staff_users.designation='".$one_person_to_approve."' LIMIT 1"));
    $specific_email = $fetch_department_email['email'];
}
else
 {
   $one_person_to_approve = $person_responsible;
   $fetch_department_email = mysqli_fetch_array(mysqli_query($dbc,"SELECT staff_users.Email AS email,staff_users.designation AS designation FROM staff_users
                                                                   WHERE staff_users.designation='".$one_person_to_approve."' LIMIT 1"));
   $specific_email = $fetch_department_email['email'];
}

//mail(to,subject,message,headers,parameters);
$subject = 'PPRMIS Risk Update Notification';
$message =  "Dear <b>".$person_responsible."</b>, <br/><br/><br/>
            The Risk Status with the reference no <b>".$reference_no."</b>,
            and description <b>".$risk_description."</b>, has been updated for the period <b>".$period_from."</b>
           ,quarter <b>".$quarter."</b>.
           <br/><br/><br/>
           Please log in to <a href='https://pprmis.cma.or.ke/prmis/pages/risk-management/update-risk-status.php'>PPRMIS</a> to view it.
           <br/><br/><br/><br/><br/>
          <b>This is an automated message, please do not reply</b>";



if(mail($specific_email,$subject,$message,$headers))
{
  echo("success");
  $date_sent = date("d-m-Y h:i:sa");
  $message = mysqli_real_escape_string($dbc,$message);
  $store_sent_mail = mysqli_query($dbc,"INSERT INTO sent_mails (sent_from,sent_to,triggered_by,message_subject,message_body,date_sent)
                                                VALUES
                                                ('".$headers."','".$specific_email."','".$_SESSION['name']."',
                                                  '".$subject."','".$message."','".$date_sent."')"
                                 ) or die (mysqli_error($dbc));
}
else
{
  echo("mail not sent");
}



                //send mail to risk creater

                $find_last_modified_by = mysqli_fetch_array(mysqli_query($dbc,"SELECT * FROM update_risk_status WHERE changed='no' && reference_no='".$reference_no."' ORDER BY id DESC LIMIT 1"));
                $find_risk_creator = mysqli_fetch_array(mysqli_query($dbc,"SELECT * FROM staff_users WHERE Name='".$find_last_modified_by['updated_by']."'"));
                $email_subject = "PPRMIS Risk Update Notification";
                $risk_creator_email = $find_risk_creator['Email'];
                $risk_creator_name = $find_risk_creator['Name'];
                $message_to_risk_creator = "Dear <b>$risk_creator_name</b>, <br/><br/><br/>
                                            The Risk Status with the reference no <b>".$reference_no."</b>,
                                            and description <b>".$risk_description."</b>, has been updated for the period <b>".$period_from."</b>
                                           ,quarter <b>".$quarter."</b>.
                                           <br/><br/><br/>
                                           Please log in to <a href='https://pprmis.cma.or.ke/prmis/pages/risk-management/update-risk-status.php'>PPRMIS</a> to view it.
                                           <br/><br/><br/><br/><br/>
                                          <b>This is an automated message, please do not reply</b>";

                //mail($risk_creator_email,$email_subject,$message_to_risk_creator,$headers) or die ("mail not sent");
                $send_mail = mail($risk_creator_email,$email_subject,$message_to_risk_creator,$headers);
                if($send_mail)
                {
                  $date_sent = date("d-m-Y h:i:sa");
                  $message_to_risk_creator = mysqli_real_escape_string($dbc,$message_to_risk_creator);
                  $store_sent_mail = mysqli_query($dbc,"INSERT INTO sent_mails (sent_from,sent_to,triggered_by,message_subject,message_body,date_sent)
                                                                VALUES
                                                                ('".$headers."','".$risk_creator_email."','".$_SESSION['name']."',
                                                                  '".$email_subject."','".$message_to_risk_creator."','".$date_sent."')"
                                                 ) or die (mysqli_error($dbc));
                  exit("success");
                }
                else {
                  exit("mail not sent");
                }
                //end send mail to risk creator
 ?>
