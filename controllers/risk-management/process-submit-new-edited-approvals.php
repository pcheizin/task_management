<?php
session_start();
require_once('../setup/connect.php');
if($_SERVER['REQUEST_METHOD'] == 'POST')
{
  $commentor = $_SESSION['name'];
  $email = $_SESSION['email'];
  $date_recorded = date("m/d/Y");
  $time_recorded = date("h:i:sa");
  $reference_no_for_risk= mysqli_real_escape_string($dbc,strip_tags($_POST['reference_no_for_risk']));
  $comments =  mysqli_real_escape_string($dbc,strip_tags($_POST['comments']));

  //select from risk management to find risk creator
  $sql_creator = mysqli_fetch_array(mysqli_query($dbc,"SELECT * FROM risk_management WHERE risk_reference='".$reference_no_for_risk."' && changed='no' ORDER BY id DESC LIMIT 1"));
  $sql_creator_name = $sql_creator['created_by'];

  //select from staff users to find related email to the name
  $sql_email_for_creator = mysqli_fetch_array(mysqli_query($dbc,"SELECT * FROM staff_users WHERE Name='".$sql_creator_name."'"));
  $sql_creator_email = $sql_email_for_creator['Email'];

  $sql_add_comment = "INSERT INTO approval_comments_new_edited (reference_no,comment,commented_by,commentor_email,commented_to,commented_to_email,date_commented,time_commented) VALUES
                      ('".$reference_no_for_risk."','".$comments."','".$commentor."','".$email."','".$sql_creator_name."','".$sql_creator_email."','".$date_recorded."','".$time_recorded."')";
  $query = mysqli_query($dbc,$sql_add_comment);

  //log the action
$action_reference = "Added comments for the risk/opportunity with the reference no ". $reference_no_for_risk;
$action_name = " Comments";
$action_icon = "fas fa-comment-lines text-success";
$page_id = "monitor-risks-link";
$time_recorded = date('Y/m/d H:i:s');

$sql_log = mysqli_query($dbc,"INSERT INTO activity_logs
                        (email,action_name,action_reference,action_icon,page_id,time_recorded)
                        VALUES
            ('".$_SESSION['email']."','".$action_name."','".$action_reference."',
            '".$action_icon."','".$page_id."','".$time_recorded."')"
          );

  if($query && $sql_log)
  {

    //send mail to risk creator for the comment
    ini_set("SMTP","cmacas.cma.local");
    ini_set('smtp_port', 25);
    $headers = "MIME-Version: 1.0" . "\r\n";
    $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";

          // More headers
    $headers .= 'From: PRMIS SYSTEM <PRisk@cma.or.ke>' . "\r\n";
    $subject = 'PPRMIS Comments Notification';
    $message = "Dear <b>".$sql_creator_name."</b>, <br/><br/><br/>
                <b>".$commentor."</b> has commented on the risk/opportunity with the reference <b>$reference_no_for_risk</b>. on the register<br/><br/>
                The comments are: <b>".$comments."</b><br/><br/>
                Please login to <a href='cmasrv13/'>PRMIS</a> and take the necessary action if required.

               <br/><br/><br/>

              <b>This is an automated message, please do not reply</b>";


    if(mail($sql_creator_email,$subject,$message,$headers))
    {
      exit("success");
    }
    else
    {
      exit("mail not sent");
    }

  }
  else {
    exit("failed");
  }
}


 ?>
