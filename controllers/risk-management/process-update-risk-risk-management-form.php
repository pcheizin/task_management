<?php
session_start();
require_once('../setup/connect.php');
if($_SERVER['REQUEST_METHOD'] == 'POST')
{
   //form fields
   $dep_code = mysqli_real_escape_string($dbc,strip_tags($_POST['dep_code']));
   $risk_opportunity = mysqli_real_escape_string($dbc,strip_tags($_POST['risk_opportunity_quarter']));
   $reference_no = mysqli_real_escape_string($dbc,strip_tags($_POST['reference_no']));
   $risk_description = mysqli_real_escape_string($dbc,strip_tags($_POST['risk_description']));
   $period_from = mysqli_real_escape_string($dbc,strip_tags($_POST['period_from']));
   $quarter = mysqli_real_escape_string($dbc,strip_tags($_POST['quarter']));
   $prior_impact_score = mysqli_real_escape_string($dbc,strip_tags($_POST['prior_impact_score']));
   $prior_likelihood_score = mysqli_real_escape_string($dbc,strip_tags($_POST['prior_likelihood_score']));
   $prior_overall_score = mysqli_real_escape_string($dbc,strip_tags($_POST['prior_overall_score']));
   $impact_score = mysqli_real_escape_string($dbc,strip_tags($_POST['impact_score']));
   $likelihood_score = mysqli_real_escape_string($dbc,strip_tags($_POST['likelihood_score']));
   $overall_score = mysqli_real_escape_string($dbc,strip_tags($_POST['overall_score']));
   $comments_updates_monitoring = mysqli_real_escape_string($dbc,strip_tags($_POST['comments_updates_monitoring']));
   //$person_responsible = implode(" , ",$_POST['person_responsible']);
   $person_responsible = mysqli_real_escape_string($dbc,strip_tags($_POST['person_responsible_for_risk']));
   $created_by = $_SESSION['name'];
   $date_recorded = date("m/d/Y");

       //start transaction
        mysqli_query($dbc,"START TRANSACTION");
       //select duplicate period and quater from database
        $sql_duplicate = mysqli_query($dbc,"SELECT * FROM update_risk_status WHERE reference_no='".$reference_no."' &&
                                       period_from='".$period_from."' && quarter='".$quarter."' && changed='no'");
        if(mysqli_num_rows($sql_duplicate))
        {
          exit("duplicate");
        }

        $sql_update1 = mysqli_query($dbc,"UPDATE  approval_comments_quarterly SET viewed ='yes' WHERE reference_no ='".$reference_no."'") or die ("failed");
        //insert into db
        $sql = "INSERT INTO update_risk_status
                 (dep_code,risk_opportunity,reference_no, risk_description, period_from, quarter,
                  prior_impact_score, prior_likelihood_score,
                  prior_overall_score, current_impact_score, current_likelihood_score,
                  current_overall_score,comments_updates_monitoring,updated_by, date_updated)

                  VALUES

                  ('".$dep_code."','".$risk_opportunity."','".$reference_no."', '".$risk_description."', '".$period_from."', '".$quarter."',
                     '".$prior_impact_score."', '".$prior_likelihood_score."',
                  '".$prior_overall_score."', '".$impact_score."', '".$likelihood_score."',
                  '".$overall_score."','".$comments_updates_monitoring."','".$created_by."', '".$date_recorded."')


                  ";

            $query = mysqli_query($dbc,$sql);

           $sql_update = mysqli_query($dbc,"UPDATE risk_drivers SET changed='yes' WHERE risk_reference='".$reference_no."'
                                                && period_from='".$period_from."' && quarter='".$quarter."'");


             $HODcomments = mysqli_query($dbc,"UPDATE approval_comments_quarterly SET viewed ='yes'");
           foreach ($_POST['update_risk_drivers'] as $row=>$selectedOption)
           {
             $driver = mysqli_real_escape_string($dbc,$_POST['update_risk_drivers'][$row]);
             $kri = mysqli_real_escape_string($dbc,$_POST['update_key_risk_indicator'][$row]);
             $threshold = mysqli_real_escape_string($dbc,$_POST['update_kri_threshold'][$row]);
             $treatment = mysqli_real_escape_string($dbc,$_POST['update_treatment_action'][$row]);
             $strategy = mysqli_real_escape_string($dbc,$_POST['update_risk_management_strategy_undertaken'][$row]);
             $effects = mysqli_real_escape_string($dbc,$_POST['update_effect_of_risk_to_authority'][$row]);
             $action = mysqli_real_escape_string($dbc,$_POST['update_action_to_be_undertaken'][$row]);
             $kri_level = mysqli_real_escape_string($dbc,$_POST['update_current_kri_level'][$row]);
             $quarterly_update = 'yes';

             $query_update = mysqli_query($dbc,"
                         INSERT INTO risk_drivers
                         (risk_reference,risk_drivers,key_risk_indicator,current_kri_level,kri_threshold,treatment_action,
                         risk_management_strategy_undertaken,effects_of_risk_to_authority,action_to_be_undertaken,period_from,quarter,quarterly_update)
                         VALUES
                         ('".$reference_no."','".$driver."','".$kri."','".$kri_level."','".$threshold."',
                         '".$treatment."','".$strategy."','".$effects."','".$action."','".$period_from."','".$quarter."','".$quarterly_update."')

                     ");

           }

           //log the action
         $action_reference = "Made a quarterly update the" .$risk_opportunity." with the reference no ". $reference_no;
         $action_name = " Quartely Update";
         $action_icon = "fas fa-file-edit text-success";
         $page_id = "monitor-risks-link";
         $time_recorded = date('Y/m/d H:i:s');

         $sql_log = mysqli_query($dbc,"INSERT INTO activity_logs
                                 (email,action_name,action_reference,action_icon,page_id,time_recorded)
                                 VALUES
                     ('".$_SESSION['email']."','".$action_name."','".$action_reference."',
                     '".$action_icon."','".$page_id."','".$time_recorded."')"
                   );

           if($HODcomments && $query && $sql_update && $query_update && $sql_log && $sql_update1)
           {
             mysqli_query($dbc,"COMMIT");
             echo "success";
           }
           else
           {
             mysqli_query($dbc,"ROLLBACK");
             echo mysqli_error($dbc);
           }



}


?>
