<?php
 if($reference_row['impact_score'] == "1")
                                        {
                                          $impact_score_name = "Insignificant";
                                        }
                                        if($reference_row['impact_score'] == "2")
                                        {
                                          $impact_score_name ="Minor" ;
                                        }
                                        if($reference_row['impact_score'] == "3")
                                        {
                                          $impact_score_name = "Moderate";
                                        }
                                        if($reference_row['impact_score'] == "4")
                                        {
                                          $impact_score_name = "Major";
                                        }
                                        if($reference_row['impact_score'] == "5")
                                        {
                                          $impact_score_name = "Catastrophic";
                                        }
                                        if($reference_row['impact_score'] != "1" && $reference_row['impact_score'] != "2" &&
                                            $reference_row['impact_score'] != "3" && $reference_row['impact_score'] != "4" && $reference_row['impact_score'] != "5")
                                        {
                                          $impact_score_name = "please select";
                                        }
                                        //likelihood score strings

                                        if($reference_row['likelihood_score'] == "1")
                                        {
                                          $likelihood_score_name = "Rare";
                                        }
                                        if($reference_row['likelihood_score'] == "2")
                                        {
                                          $likelihood_score_name = "Unlikely";
                                        }
                                        if($reference_row['likelihood_score'] == "3")
                                        {
                                          $likelihood_score_name = "Likely";
                                        }
                                        if($reference_row['likelihood_score'] == "4")
                                        {
                                          $likelihood_score_name = "Highly Certain";
                                        }
                                        if($reference_row['likelihood_score'] == "5")
                                        {
                                          $likelihood_score_name = "Almost Certain";
                                        }


?>
