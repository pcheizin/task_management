<?php
require_once('../setup/connect.php');
session_start();
if($_SERVER['REQUEST_METHOD'] == 'POST')
{
    //collect form fields
    $department_code = $_SESSION['department_code'];
    $department_name = $_SESSION['department'];
    $period = mysqli_real_escape_string($dbc,strip_tags($_POST['period_from']));
    $quarter= mysqli_real_escape_string($dbc,strip_tags($_POST['quarter']));
    $the_event = mysqli_real_escape_string($dbc,strip_tags($_POST['the_event']));
    $impact= mysqli_real_escape_string($dbc,strip_tags($_POST['impact']));
    $root_causes = mysqli_real_escape_string($dbc,strip_tags($_POST['root_causes']));
    $corrective_action_plans= mysqli_real_escape_string($dbc,strip_tags($_POST['corrective_action_plans']));
    $lessons_learnt = mysqli_real_escape_string($dbc,strip_tags($_POST['lessons_learnt']));
    $created_by = $_SESSION['name'];
    $date_recorded = date("m/d/Y");
    $time_recorded = date("h:i:sa");

    if($_SESSION['access_level'] == "director" || $_SESSION['designation'] == "Chief Executive" )
    {
      $is_corporate = "yes";
    }
    else
    {
      $is_corporate = "no";
    }

    $sql_update = mysqli_query($dbc,"UPDATE incident_report SET changed='yes' WHERE id='".$_POST['id']."'
                                            && period_from='".$period."' && quarter='".$quarter."'") or die ("failed");
    //insert values to database
    $sql_statement = "INSERT INTO incident_report

                        ( the_event, impact, root_causes, corrective_action_plans, lessons_learnt,
                           department_code, department_name, period_from,quarter, is_corporate,created_by, date_created, time_created)

                      VALUES
                          ('".$the_event."','".$impact."','".$root_causes."','".$corrective_action_plans."',
                          '".$lessons_learnt."','".$department_code."','".$department_name."','".$period."',
                        '".$quarter."','".$is_corporate."',  '".$created_by."', '".$date_recorded."', '".$time_recorded."')
                        ";

    //check if query runs

    //log the action
  $action_reference = "Updated an Incident with the event: " .$the_event;
  $action_name = "Incident Update";
  $action_icon = "fas fa-file-edit text-warning";
  $page_id = "incident-reporting-link";
  $time_recorded = date('Y/m/d H:i:s');

  $sql_log = mysqli_query($dbc,"INSERT INTO activity_logs
                          (email,action_name,action_reference,action_icon,page_id,time_recorded)
                          VALUES
              ('".$_SESSION['email']."','".$action_name."','".$action_reference."',
              '".$action_icon."','".$page_id."','".$time_recorded."')"
            );
    $insert_query = mysqli_query($dbc,$sql_statement);

    if($sql_log && $insert_query)
    {
        exit ("success");
    }
    else
    {
        exit ("failed");
    }
}

?>
