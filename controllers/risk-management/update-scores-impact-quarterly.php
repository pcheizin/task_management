<?php
require_once('../setup/connect.php');
session_start();
if($_SERVER['REQUEST_METHOD'] == 'POST')
{
    //collect form fields
    $score_id = mysqli_real_escape_string($dbc,strip_tags($_POST['id']));
    $impact_score = mysqli_real_escape_string($dbc,strip_tags($_POST['impact_score_quarterly']));

    //check for an existing record in the quarterly update table
    $sql = mysqli_query($dbc,"SELECT * FROM update_risk_status WHERE id='".$score_id."' && changed='no'");
      $row_likelihood_score = mysqli_fetch_array($sql);
      $likelihood_score = $row_likelihood_score['current_likelihood_score'];
      $overall_score = $impact_score * $likelihood_score ;
      $update  = mysqli_query($dbc,"UPDATE update_risk_status SET current_impact_score='".$impact_score."',current_overall_score='".$overall_score."' WHERE id='".$score_id."'");

      //log the action
    $action_reference = "Updated scores for the risk with id ". $score_id;
    $action_name = " Scores update";
    $action_icon = "fas fa-file-edit text-success";
    $page_id = "monitor-risks-link";
    $time_recorded = date('Y/m/d H:i:s');

    $sql_log = mysqli_query($dbc,"INSERT INTO activity_logs
                            (email,action_name,action_reference,action_icon,page_id,time_recorded)
                            VALUES
                ('".$_SESSION['email']."','".$action_name."','".$action_reference."',
                '".$action_icon."','".$page_id."','".$time_recorded."')"
              );
      if($update && $sql_log)
      {
        exit("success");
      }
      else
      {
        exit("failed");
      }
}

?>
