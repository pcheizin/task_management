<?php
require_once('../setup/connect.php');
session_start();
if($_SERVER['REQUEST_METHOD'] == 'POST')
{
    //collect form fields
    $score_id = mysqli_real_escape_string($dbc,strip_tags($_POST['id']));
    $likelihood_score = mysqli_real_escape_string($dbc,strip_tags($_POST['likelihood_score']));

    //check for an existing record in the quarterly update table
    $sql = mysqli_query($dbc,"SELECT * FROM update_risk_status WHERE current_likelihood_score!='' && id='".$score_id."' && changed='no'");
    if(mysqli_num_rows($sql) > 0)
    {
      $row_impact_score = mysqli_fetch_array($sql);
      $impact_score = $row_impact_score['current_impact_score'];
      $overall_score = $impact_score * $likelihood_score ;
      $update  = mysqli_query($dbc,"UPDATE update_risk_status SET current_likelihood_score='".$likelihood_score."',current_overall_score='".$overall_score."' WHERE id='".$score_id."'");
      if($update)
      {
        exit("success");
      }
      else
      {
        exit("failed");
      }
    }
    else
    {
      $sql = mysqli_query($dbc,"SELECT * FROM risk_management WHERE id='".$score_id."'");
      $row_impact_score = mysqli_fetch_array($sql);
      $impact_score = $row_impact_score['impact_score'];
      $overall_score = $impact_score * $likelihood_score;
      $update  = mysqli_query($dbc,"UPDATE risk_management SET likelihood_score='".$likelihood_score."', overall_score='".$overall_score."' WHERE id='".$score_id."'");

      //log the action
      $action_reference = "Updated scores for the risk with id ". $score_id;
      $action_name = " Scores update";
      $action_icon = "fas fa-file-edit text-success";
      $page_id = "monitor-risks-link";
      $time_recorded = date('Y/m/d H:i:s');

      $sql_log = mysqli_query($dbc,"INSERT INTO activity_logs
                              (email,action_name,action_reference,action_icon,page_id,time_recorded)
                              VALUES
                  ('".$_SESSION['email']."','".$action_name."','".$action_reference."',
                  '".$action_icon."','".$page_id."','".$time_recorded."')"
                );
      if($update && $sql_log)
      {


        //exit("success");
        exit("success");
      }
      else
      {
        exit("failed");
      }
    }
}

?>
