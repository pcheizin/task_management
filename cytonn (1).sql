-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Feb 22, 2022 at 02:21 PM
-- Server version: 10.4.19-MariaDB
-- PHP Version: 8.0.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `cytonn`
--

-- --------------------------------------------------------

--
-- Table structure for table `activity_logs`
--

CREATE TABLE `activity_logs` (
  `id` int(6) NOT NULL,
  `email` varchar(50) NOT NULL,
  `action_name` varchar(100) NOT NULL,
  `action_reference` varchar(3000) NOT NULL,
  `action_icon` varchar(100) DEFAULT NULL,
  `page_id` varchar(100) NOT NULL,
  `time_recorded` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `activity_logs`
--

INSERT INTO `activity_logs` (`id`, `email`, `action_name`, `action_reference`, `action_icon`, `page_id`, `time_recorded`) VALUES
(0, 'inventory@panoramaengineering.com', 'Logged in', 'Logged into the system', 'far fa-sign-in text-success', 'login-link', '2022/02/22 09:45:13');

-- --------------------------------------------------------

--
-- Table structure for table `backups`
--

CREATE TABLE `backups` (
  `id` int(6) NOT NULL,
  `backup_location` varchar(1000) COLLATE utf8mb4_unicode_ci NOT NULL,
  `backup_type` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `triggered_by` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `time_recorded` timestamp(6) NOT NULL DEFAULT current_timestamp(6)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `departments`
--

CREATE TABLE `departments` (
  `department_id` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `department_name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `departments`
--

INSERT INTO `departments` (`department_id`, `department_name`) VALUES
('ADMN', 'Administration'),
('FIN', 'Finance'),
('HCA', 'Human Resource'),
('ICT', 'Information Communication Technology'),
('PROC', 'Procurement');

-- --------------------------------------------------------

--
-- Table structure for table `feedback_receiver`
--

CREATE TABLE `feedback_receiver` (
  `id` int(6) NOT NULL,
  `email` varchar(250) NOT NULL,
  `added_by` varchar(250) NOT NULL,
  `date_added` varchar(250) NOT NULL,
  `time_added` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `feedback_receiver`
--

INSERT INTO `feedback_receiver` (`id`, `email`, `added_by`, `date_added`, `time_added`) VALUES
(12, 'inventory@panoramaengineering.com', 'PETER CHEGE KARIUKI', '02/18/2020', '03:55:14pm');

-- --------------------------------------------------------

--
-- Table structure for table `page_requests`
--

CREATE TABLE `page_requests` (
  `id` int(6) NOT NULL,
  `page_id` varchar(500) DEFAULT NULL,
  `page_name` varchar(500) NOT NULL,
  `requested_by` varchar(50) NOT NULL,
  `user_type` varchar(50) NOT NULL DEFAULT 'default',
  `time_requested` timestamp(6) NOT NULL DEFAULT current_timestamp(6)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `page_requests`
--

INSERT INTO `page_requests` (`id`, `page_id`, `page_name`, `requested_by`, `user_type`, `time_requested`) VALUES
(0, 'project-management-module', 'Project Management Module', 'inventory@panoramaengineering.com', '', '2022-02-22 13:21:00.648609');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `id` int(100) NOT NULL,
  `email` varchar(100) DEFAULT NULL,
  `token` varchar(500) DEFAULT NULL,
  `time_recorded` timestamp(6) NOT NULL DEFAULT current_timestamp(6)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `pm_activities`
--

CREATE TABLE `pm_activities` (
  `id` int(6) NOT NULL,
  `task_id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `milestone_id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `department` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `listing` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `activity_name` varchar(1000) COLLATE utf8mb4_unicode_ci NOT NULL,
  `start_date` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `end_date` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `duration` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `time_recorded` timestamp(6) NOT NULL DEFAULT current_timestamp(6) ON UPDATE current_timestamp(6),
  `recorded_by` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `pm_activities`
--

INSERT INTO `pm_activities` (`id`, `task_id`, `milestone_id`, `department`, `listing`, `activity_name`, `start_date`, `end_date`, `duration`, `time_recorded`, `recorded_by`) VALUES
(137, 'TASK1', '58', 'Information Communication Technology', 'Public', 'Senior devops challenge', '16-Feb-2022', '22-Feb-2022', '6', '2022-02-21 13:04:38.244953', 'PETER CHEGE KARIUKI'),
(138, 'TASK2', '59', 'Human Resource', 'Private', 'A bitbucket repository with your code.', '15-Feb-2022', '22-Feb-2022', '7', '2022-02-21 14:23:18.731799', 'PETER CHEGE KARIUKI'),
(0, 'TASK3', '0', 'Human Resource', 'Private', 'actv 34', '14-Feb-2022', '25-Feb-2022', '11', '2022-02-22 06:54:23.862286', 'PETER CHEGE KARIUKI');

-- --------------------------------------------------------

--
-- Table structure for table `pm_activity_updates`
--

CREATE TABLE `pm_activity_updates` (
  `id` int(6) NOT NULL,
  `task_id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `project_id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(300) COLLATE utf8mb4_unicode_ci NOT NULL,
  `color_code` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `comments` varchar(1000) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `changed` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'no',
  `date_recorded` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `time_recorded` timestamp(6) NOT NULL DEFAULT current_timestamp(6),
  `recorded_by` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `pm_activity_updates`
--

INSERT INTO `pm_activity_updates` (`id`, `task_id`, `project_id`, `status`, `color_code`, `comments`, `changed`, `date_recorded`, `time_recorded`, `recorded_by`) VALUES
(0, 'TASK3', '', 'Task Pending', 'three', 'Pending task', 'no', '22-Feb-22', '2022-02-22 12:11:11.650568', 'PETER CHEGE KARIUKI'),
(169, 'TASK2', '', 'Task Received', 'five', 'Task Received', 'no', '22-Feb-22', '2022-02-22 05:57:08.129289', 'PETER CHEGE KARIUKI'),
(170, 'TASK1', '', 'Task Completed', 'two', 'Task Completed', 'no', '22-Feb-22', '2022-02-22 05:57:22.282427', 'PETER CHEGE KARIUKI');

-- --------------------------------------------------------

--
-- Table structure for table `pm_listing`
--

CREATE TABLE `pm_listing` (
  `id` int(6) NOT NULL,
  `name` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `pm_listing`
--

INSERT INTO `pm_listing` (`id`, `name`) VALUES
(1, 'Private'),
(2, 'Public');

-- --------------------------------------------------------

--
-- Table structure for table `pm_milestones`
--

CREATE TABLE `pm_milestones` (
  `id` int(6) NOT NULL,
  `milestone_name` varchar(1000) COLLATE utf8mb4_unicode_ci NOT NULL,
  `start_date` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `end_date` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `duration` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `time_recorded` timestamp(6) NOT NULL DEFAULT current_timestamp(6),
  `recorded_by` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `total_tasks` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `completed_tasks` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `payment_status` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'not_initiated'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `pm_milestones`
--

INSERT INTO `pm_milestones` (`id`, `milestone_name`, `start_date`, `end_date`, `duration`, `time_recorded`, `recorded_by`, `total_tasks`, `completed_tasks`, `payment_status`) VALUES
(0, 'This is my category', '14-Feb-2022', '17-Feb-2022', '3', '2022-02-22 06:53:56.493535', 'PETER CHEGE KARIUKI', '1', '0', 'not_initiated'),
(58, 'Coding Challenge', '16-Feb-2022', '22-Feb-2022', '6', '2022-02-21 12:43:50.791777', 'PETER CHEGE KARIUKI', '1', '0', 'not_initiated'),
(59, 'Code Submission', '14-Feb-2022', '24-Feb-2022', '10', '2022-02-21 13:59:12.143766', 'PETER CHEGE KARIUKI', '1', '0', 'not_initiated');

-- --------------------------------------------------------

--
-- Table structure for table `pm_project_attached_workplans`
--

CREATE TABLE `pm_project_attached_workplans` (
  `id` int(6) NOT NULL,
  `project_id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `activity_id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `time_recorded` timestamp(6) NOT NULL DEFAULT current_timestamp(6) ON UPDATE current_timestamp(6),
  `recorded_by` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `changed` varchar(5) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'no'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `pm_project_attached_workplans`
--

INSERT INTO `pm_project_attached_workplans` (`id`, `project_id`, `activity_id`, `time_recorded`, `recorded_by`, `changed`) VALUES
(7, 'PROJ6', 'ACT1102', '2022-02-20 16:42:09.903240', 'PETER CHEGE KARIUKI', 'no'),
(8, 'PROJ7', 'ACT46', '2022-02-21 05:39:02.018570', 'PETER CHEGE KARIUKI', 'no');

-- --------------------------------------------------------

--
-- Table structure for table `pm_project_documents`
--

CREATE TABLE `pm_project_documents` (
  `id` int(6) NOT NULL,
  `project_id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `file` varchar(500) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_by` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `time_created` timestamp(6) NOT NULL DEFAULT current_timestamp(6) ON UPDATE current_timestamp(6)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `pm_project_users`
--

CREATE TABLE `pm_project_users` (
  `id` int(6) NOT NULL,
  `senior_user` varchar(100) NOT NULL,
  `project_id` varchar(100) NOT NULL,
  `date_recorded` varchar(50) NOT NULL,
  `time_recorded` timestamp NOT NULL DEFAULT current_timestamp(),
  `recorded_by` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `pm_resources`
--

CREATE TABLE `pm_resources` (
  `resource_id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `activity_id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `project_id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `resource_name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `time_recorded` timestamp(6) NOT NULL DEFAULT current_timestamp(6),
  `recorded_by` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `pm_resources`
--

INSERT INTO `pm_resources` (`resource_id`, `activity_id`, `project_id`, `resource_name`, `time_recorded`, `recorded_by`) VALUES
('RES1', 'TASK2', '', 'PETER CHEGE KARIUKI', '2022-02-21 18:40:39.777385', 'PETER CHEGE KARIUKI'),
('RES2', 'TASK1', '', 'PETER CHEGE KARIUKI', '2022-02-21 18:41:17.571166', 'PETER CHEGE KARIUKI'),
('RES3', 'TASK2', '', 'A WANJOHI', '2022-02-22 06:49:00.751755', 'PETER CHEGE KARIUKI'),
('RES4', 'TASK3', '', 'D MAINYE', '2022-02-22 12:10:53.414072', 'PETER CHEGE KARIUKI');

-- --------------------------------------------------------

--
-- Table structure for table `pm_task_files`
--

CREATE TABLE `pm_task_files` (
  `id` int(6) NOT NULL,
  `task_id` varchar(100) DEFAULT NULL,
  `document` varchar(100) DEFAULT NULL,
  `recorded_by` varchar(100) DEFAULT NULL,
  `date_recorded` timestamp(6) NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `pm_task_status`
--

CREATE TABLE `pm_task_status` (
  `id` int(6) NOT NULL,
  `name` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `pm_task_status`
--

INSERT INTO `pm_task_status` (`id`, `name`) VALUES
(1, 'Task Received'),
(2, 'Tasks Pending'),
(3, 'Task Completed');

-- --------------------------------------------------------

--
-- Table structure for table `pm_task_status_codes`
--

CREATE TABLE `pm_task_status_codes` (
  `id` int(3) NOT NULL,
  `color_class` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `code_name` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status_name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `time_recorded` timestamp(6) NOT NULL DEFAULT current_timestamp(6)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `sent_mails`
--

CREATE TABLE `sent_mails` (
  `id` int(6) NOT NULL,
  `sent_from` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sent_to` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `triggered_by` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `message_subject` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `message_body` varchar(5000) COLLATE utf8mb4_unicode_ci NOT NULL,
  `date_sent` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `sign_in_logs`
--

CREATE TABLE `sign_in_logs` (
  `id` int(6) NOT NULL,
  `email` varchar(100) DEFAULT NULL,
  `name` varchar(100) DEFAULT NULL,
  `ip_address` varchar(50) DEFAULT NULL,
  `user_type` varchar(50) NOT NULL DEFAULT 'default',
  `time_signed_in` varchar(50) DEFAULT NULL,
  `time_signed_out` varchar(50) DEFAULT NULL,
  `date_recorded` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `staff_users`
--

CREATE TABLE `staff_users` (
  `EmpNo` varchar(100) NOT NULL,
  `Name` varchar(100) DEFAULT NULL,
  `Email` varchar(255) DEFAULT NULL,
  `DepartmentCode` varchar(50) NOT NULL,
  `status` varchar(50) NOT NULL DEFAULT 'active',
  `access_level` varchar(50) NOT NULL DEFAULT 'standard',
  `designation` varchar(250) DEFAULT NULL,
  `emp_photo` varchar(100) NOT NULL DEFAULT 'user.jpg',
  `password` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `staff_users`
--

INSERT INTO `staff_users` (`EmpNo`, `Name`, `Email`, `DepartmentCode`, `status`, `access_level`, `designation`, `emp_photo`, `password`) VALUES
('CYT001', 'PETER CHEGE KARIUKI', 'inventory@panoramaengineering.com', 'ICT', 'active', 'admin', 'Senior Developer', 'J7SN23R.jpg', '81dc9bdb52d04dc20036dbd8313ed055'),
('CYT003', 'D MBURU', 'dmburu@cytonn.com', 'PROC', 'active', 'admin', 'Procurement', 'user.jpg', '81dc9bdb52d04dc20036dbd8313ed055'),
('CYT004', 'D MAINYE', 'dmainye@cytonn.com', 'DO', 'active', 'admin', 'Human Resource', 'avatar.png', '81dc9bdb52d04dc20036dbd8313ed055'),
('CYT005', 'EVA CHAKA', 'echaka@cytonn.com', 'FA', 'active', 'admin', 'finance', 'user.jpg', '81dc9bdb52d04dc20036dbd8313ed055'),
('CYT006', 'A WANJOHI', 'awanjohi@cytonn.com', 'LO', 'active', 'admin', 'Logistics', 'user.jpg', '81dc9bdb52d04dc20036dbd8313ed055'),
('CYTOO2', 'P KAGWI', 'pkagwi@cytonn.com', 'FA', 'active', 'admin', 'Finance', 'user.jpg', '81dc9bdb52d04dc20036dbd8313ed055');

-- --------------------------------------------------------

--
-- Table structure for table `user_feedback`
--

CREATE TABLE `user_feedback` (
  `id` int(6) NOT NULL,
  `feedback_message` varchar(1000) NOT NULL,
  `feedback_person` varchar(250) NOT NULL,
  `date_submitted` varchar(250) NOT NULL,
  `time_submitted` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `activity_logs`
--
ALTER TABLE `activity_logs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`email`),
  ADD KEY `page_id` (`page_id`);

--
-- Indexes for table `backups`
--
ALTER TABLE `backups`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `departments`
--
ALTER TABLE `departments`
  ADD PRIMARY KEY (`department_id`);

--
-- Indexes for table `feedback_receiver`
--
ALTER TABLE `feedback_receiver`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email` (`email`);

--
-- Indexes for table `page_requests`
--
ALTER TABLE `page_requests`
  ADD PRIMARY KEY (`id`),
  ADD KEY `requested_by` (`requested_by`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pm_activities`
--
ALTER TABLE `pm_activities`
  ADD PRIMARY KEY (`task_id`),
  ADD UNIQUE KEY `id` (`id`),
  ADD KEY `milestone_id` (`milestone_id`);

--
-- Indexes for table `pm_activity_updates`
--
ALTER TABLE `pm_activity_updates`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pm_listing`
--
ALTER TABLE `pm_listing`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pm_milestones`
--
ALTER TABLE `pm_milestones`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pm_task_files`
--
ALTER TABLE `pm_task_files`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pm_task_status`
--
ALTER TABLE `pm_task_status`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `staff_users`
--
ALTER TABLE `staff_users`
  ADD PRIMARY KEY (`EmpNo`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `password_resets`
--
ALTER TABLE `password_resets`
  MODIFY `id` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `pm_task_files`
--
ALTER TABLE `pm_task_files`
  MODIFY `id` int(6) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
