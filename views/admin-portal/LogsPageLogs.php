<?php
if(!$_SERVER['REQUEST_METHOD'] == "POST")
{
  exit();
}
session_start();
include("../../controllers/setup/connect.php");
if($_SESSION['access_level']!='admin')
{
    exit("unauthorized");
}
?>

<div class="row">
    <div class="col-12">
      <div class="card card-primary card-outline">
        <div class="card-header bg-light">Page Logs
          <div class="card-tools">
            <!-- Collapse Button -->
            <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
          </div>
        </div>
        <div class="card-body table-responsive p-0">
          <table id="admin-logs-navigations-table" class="table table-hover table-bordered table-striped" width="100%">
            <thead>
            <tr>
              <th>No</th>
              <th>Page Name</th>
              <th>Requested By</th>
              <th>Time Navigated</th>
            </tr>
            </thead>
            <tbody>
              <?php
                  $no = 1;
                  $sql = mysqli_query($dbc,"SELECT * FROM page_requests ORDER BY id DESC");
                  while($row = mysqli_fetch_array($sql))
                  {
                    $name = mysqli_fetch_array(mysqli_query($dbc,"SELECT Name FROM staff_users WHERE Email='".$row['requested_by']."'"));
                    ?>
                    <tr>
                      <td><?php echo $no++ ;?></td>
                      <td class="<?php echo $row['page_id'];?>" style="cursor:pointer;"><?php echo $row['page_name'];?></td>
                      <td><?php echo $name['Name'];?></td>
                      <td><?php echo $row['time_requested'];?></td>
                    </tr>
                    <?php
                  }
               ?>
            </tbody>
            <tfoot>
            <tr>
              <th>No</th>
              <th>Name</th>
              <th>Time Navigated</th>
            </tr>
            </tfoot>
          </table>
        </div>
        <!-- /.card-body -->
      </div>
      <!-- /.card -->
    </div>
  </div>
  <!-- /.row -->


  <div class="row">
    <div class="col-md-12">
      <div class="card">
        <div class="card-header bg-light">
          <h3 class="card-title">Page Logs Chart Distribution</h3>
          <div class="card-tools">
            <!-- Collapse Button -->
            <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
          </div>
          <!-- /.card-tools -->
        </div>
        <!-- /.card-header -->
        <div class="card-body">
          <?php
          //pie chart for task status
          $sql = mysqli_query($dbc,"SELECT page_name, count(*) AS page_count, page_name FROM page_requests
                                                   GROUP BY page_name");

          while ($row = mysqli_fetch_array($sql))
          {
            $count[] = $row['page_count'];
            $name[] = $row['page_name'];
          }
          $counted_names = json_encode($count);
          $names = json_encode($name);
          ?>
          <div class="chart">
            <!-- Resource Distribution Chart Canvas -->
            <canvas id="page-logs-chart" height="300" style="height: 300px; display: block; width: 577px;" class="chartjs-render-monitor" width="577"></canvas>
            <!--<div id="chart" height="250" style="height: 250px;"></div>-->
          </div>
          <!-- /.chart-responsive -->
        </div>
        <!-- /.card-body -->
      </div>
      <!-- /.card -->
    </div>
  </div>


  <script>
  //START TASK STATUS CHART
  var pieChartCanvas = $('#page-logs-chart').get(0).getContext('2d');
  var pieData        = {
  labels: <?php echo $names;?>,
  datasets: [
  {
  fill: false,
  data: <?php echo $counted_names;?>,
  //backgroundColor : ['#f56954', '#00a65a', '#f39c12', '#00c0ef', '#3c8dbc', '#d2d6de'],
  }
  ]
  }
  var pieOptions     = {
  legend: {
  display: true,
  position: 'right',
  labels: {
  fontColor: '#333',
  usePointStyle:true
  }
  },
  plugins: {

  colorschemes: {

  scheme: 'tableau.ClassicAreaRedGreen21'

  }

  }
  }
  //Create pie or douhnut chart
  // You can switch between pie and douhnut using the method below.
  var pieChart = new Chart(pieChartCanvas, {
  type: 'doughnut',
  data: pieData,
  options: pieOptions
  })


  //END TASK STATUS CHART
  </script>
