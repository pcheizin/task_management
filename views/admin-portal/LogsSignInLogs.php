<?php
if(!$_SERVER['REQUEST_METHOD'] == "POST")
{
  exit();
}
session_start();
include("../../controllers/setup/connect.php");
if($_SESSION['access_level']!='admin')
{
    exit("unauthorized");
}
?>

<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header bg-light">Sign In Logs
              <div class="card-tools">
                <!-- Collapse Button -->
                <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
              </div>
            </div>
            <div class="card-body table-responsive">
                <?php
               $sql_query = mysqli_query($dbc,"SELECT * FROM sign_in_logs ORDER BY id DESC");
               $number = 1;
               if($total_rows = mysqli_num_rows($sql_query) > 0)
               {?>
               <table class="table table-hover" id="sign-in-logs-table">
                 <thead>
                   <tr>
                     <td>NO</td>
                     <td>Email</td>
                     <td>Name</td>
                     <td>Ip Address</td>
                     <td>Time Signed In</td>
                     <td>Time Signed Out</td>
                   </tr>
                 </thead>
                 <?php
                 while($row = mysqli_fetch_array($sql_query))
                 {?>
                 <tr style="cursor: pointer;">
                   <td><?php echo $number++;?></td>
                   <td><?php echo $row['email'];?></td>
                   <td><?php echo $row['name'];?></td>
                   <td><?php echo $row['ip_address'];?></td>
                   <td><?php echo $row['time_signed_in'];?></td>
                   <td><?php echo $row['time_signed_out'];?></td>
                 </tr>
                 <?php
                 }
                 ?>
                 <tfoot>
                     <tr>
                         <th>NO</th>
                         <th>Email</th>
                         <th>Name</th>
                         <th>Ip Address</th>
                         <th>Time Signed In</th>
                         <th>Time Signed Out</th>
                     </tr>
                 </tfoot>
               </table>
               <?php
               }
               ?>
            </div>
        </div>
    </div>
</div>

<div class="row">
  <div class="col-md-12">
    <div class="card">
      <div class="card-header bg-light">
        <h3 class="card-title">Login History Chart Distribution</h3>
        <div class="card-tools">
          <!-- Collapse Button -->
          <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
        </div>
        <!-- /.card-tools -->
      </div>
      <!-- /.card-header -->
      <div class="card-body">
        <?php
        //pie chart for task status
        $sql = mysqli_query($dbc,"SELECT name, count(*) AS name_count,name FROM sign_in_logs WHERE user_type!='testuser'
                                                 GROUP BY email");

        while ($row = mysqli_fetch_array($sql))
        {
          $count[] = $row['name_count'];
          $name[] = $row['name'];
        }
        $counted_names = json_encode($count);
        $names = json_encode($name);
        ?>
        <div class="chart">
          <!-- Resource Distribution Chart Canvas -->
          <canvas id="login-history-chart" height="300" style="height: 300px; display: block; width: 577px;" class="chartjs-render-monitor" width="577"></canvas>
          <!--<div id="chart" height="250" style="height: 250px;"></div>-->
        </div>
        <!-- /.chart-responsive -->
      </div>
      <!-- /.card-body -->
    </div>
    <!-- /.card -->
  </div>
</div>


<script>
//START TASK STATUS CHART
var pieChartCanvas = $('#login-history-chart').get(0).getContext('2d');
var pieData        = {
labels: <?php echo $names;?>,
datasets: [
{
fill: false,
data: <?php echo $counted_names;?>,
//backgroundColor : ['#f56954', '#00a65a', '#f39c12', '#00c0ef', '#3c8dbc', '#d2d6de'],
}
]
}
var pieOptions     = {
legend: {
display: false,
position: 'bottom',
labels: {
fontColor: '#333',
usePointStyle:true
}
},
plugins: {

colorschemes: {

scheme: 'brewer.DarkTwo8'

}

}
}
//Create pie or douhnut chart
// You can switch between pie and douhnut using the method below.
var pieChart = new Chart(pieChartCanvas, {
type: 'line',
data: pieData,
options: pieOptions
})


//END TASK STATUS CHART
</script>
