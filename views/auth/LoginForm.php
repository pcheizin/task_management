
<?php
if($_SERVER['REQUEST_METHOD'] == "POST")
{
  require_once('../../controllers/setup/connect.php');
  if(isset($_SERVER['REMOTE_USER']))
  {
    $user = $_SERVER['REMOTE_USER'];

  }
  else
  {
    exit("unauthenticated");
  }

if(isset($_SESSION['user_email']))
{
  session_destroy();
}
 session_start();

 //$email_sql = mysqli_query($dbc,"SELECT username,email FROM ldap WHERE username='".$user."'");
 $email_sql = mysqli_query($dbc,"SELECT username,email FROM ldap WHERE LOWER(username) = LOWER('".$user."')");
 $email_sql_row = mysqli_fetch_array($email_sql);
 $user_email = $email_sql_row['email'];

 $_SESSION['user_email'] = $user_email;
 $_SESSION['user_name'] = $user;


  ?>
<div class="row">
  <div class="lockscreen-wrapper animated slideInLeft card card-body card-primary card-outline shadow-lg " style="max-width: 25rem;background-color: rgb(233, 236, 239); opacity:0.7">
  <!-- User name -->
  <div class="lockscreen-name mb-4 text-muted" style="font-size:25px;">
    <strong>CMAKE\<?php echo $user;?></strong>
  </div>


  <!-- START LOCK SCREEN ITEM -->
  <div class="lockscreen-item">
    <!-- lockscreen image -->
    <div class="lockscreen-image">
      <img src="assets/img/user-lock.png" alt="User Image" height="100" width="100" title="Account Locked">
    </div>
    <!-- /.lockscreen-image -->

    <?php
    if($user == "MAbdalla")
    {
      ?>
      <!-- lockscreen credentials (contains the form) -->
      <form class="lockscreen-credentials" id="login-form">
        <div class="input-group ">
          <input type="hidden" id="email" name="email" class="form-control"
                         maxlength="20" required  value="<?php echo $user;?>"
                         placeholder="Your Windows Username" readonly
                  />
          <!--<input type="password" name="password" id="password"
                  maxlength="40" class="form-control pwd"  required placeholder="Your Windows Password">

          <span class="input-group-append password-reveal-icon d-none">
              <button class="btn btn-default reveal" type="button"><i class="fa fa-eye"></i></button>
          </span>-->
          <!--<div class="input-group-append">
            <button type="submit" class="btn btn-block"><i class="fas fa-arrow-right text-muted"></i></button>
          </div>-->
        </div>
        <button type="submit" class="btn btn-block hvr-back-pulse"><i class="fas fa-sign-in text-muted"></i> LOG IN </button>
      </form>
      <!-- /.lockscreen credentials -->
      <?php
    }
    else
    {
      ?>
      <!-- lockscreen credentials (contains the form) -->
      <form class="lockscreen-credentials" id="login-form">
        <div class="input-group ">
          <input type="hidden" id="email" name="email" class="form-control"
                         maxlength="20" required  value="<?php echo $user;?>"
                         placeholder="Your Windows Username" readonly
                  />
          <!--<input type="password" name="password" id="password"
                  maxlength="40" class="form-control pwd"  required placeholder="Your Windows Password">

          <span class="input-group-append password-reveal-icon d-none">
              <button class="btn btn-default reveal" type="button"><i class="fa fa-eye"></i></button>
          </span>-->
          <!--<div class="input-group-append">
            <button type="submit" class="btn btn-block"><i class="fas fa-arrow-right text-muted"></i></button>
          </div>-->
        </div>
        <button type="submit" class="btn btn-block hvr-back-pulse"><i class="fas fa-sign-in text-muted"></i> LOG IN </button>
      </form>
      <!-- /.lockscreen credentials -->
      <?php
    }

     ?>


  </div>
  <!-- /.lockscreen-item -->
  <div class="help-block text-center text-muted">
    <span class="text-primary invisible" id="caps-lock"><i class="fad fa-lock-alt"></i> CAPS LOCK IS ON!</span>
  </div>
  <div class="text-center mt-3">
    <a href="#" onclick="CannotLogin('<?php echo $user;?>');">I need help signing in</a>
  </div>
</div>

</div>


  <?php
}
else
{
  exit("form not submitted");
}
 ?>
