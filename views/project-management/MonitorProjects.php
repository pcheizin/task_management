<?php
if(!$_SERVER['REQUEST_METHOD'] == "POST")
{
  exit();
}
session_start();
include("../../controllers/setup/connect.php");

if (!isset($_SESSION['email']))
{
     exit("<a href='#' class='login-link'>Please Log in to continue</a>");
}

?>
<nav aria-label="breadcrumb">
     <ol class="breadcrumb">
       <li class="breadcrumb-item active" aria-current="page">Project Management : Monitor Projects</li>
     </ol>
</nav>

<div class="row">
  <div class="col-lg-12 col-xs-12">
    <div class="card card-primary card-outline">
      <div class="card-header">
        Project List
        <button class="btn btn-link" style="float:right;"
                data-toggle="modal" data-target="#add-project-modal">
                <i class="fa fa-plus-circle"></i> Create Project
        </button>
      </div>
      <div class="card-body table-responsive">
       <table class="table table-striped table-bordered table-hover" id="projects-list-table" style="width:100%">
         <thead>
           <tr>
             <td>#</td>
             <td>Project Name</td>
             <td>Project Description</td>
             <td>Project Owner</td>
             <td>Project Phase</td>
             <td>Project Status</td>
             <td>Start Date</td>
             <td>End Date</td>
             <td>Days Due</td>
             <!--<td>Files</td> -->
             <td>Edit</td>
             <td>Delete</td>
           </tr>
         </thead>
         <?php
         $no = 1;
          $sql = mysqli_query($dbc,"SELECT * FROM pm_projects ORDER BY id DESC");
          while($row = mysqli_fetch_array($sql)){
          ?>
         <tr style="cursor: pointer;">
           <td> <?php echo $no++;?> </td>
           <!--<td onclick="ViewProject('<?php echo $row['id'];?>');">
                <span class="text-primary" style="cursor:pointer;"><?php echo $row['project_name'] ;?></span>
           </td> -->
           <td>
                <?php echo $row['project_name'] ;?>
           </td>
           <td><?php echo $row['project_description'] ;?></td>
           <td><?php echo $row['project_owner'] ;?></td>
           <td>
             <!-- start project phase modal -->
            <div class="modal fade" id="project-phase-modal-<?php echo $row['project_id'];?>" role="dialog">
              <div class="modal-dialog" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <h5 class="modal-title">Updating Project Status and Phase : <?php echo $row['project_name'];?> </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </div>
                  <div class="modal-body">
                    <div id="accordion-<?php echo $row['project_id'];?>">
                      <div class="card">
                        <div class="card-header bg-light" data-toggle="collapse" data-target="#collapseOne-<?php echo $row['project_id'];?>">
                          <h5 class="mb-0">
                            <button class="btn btn-link" >
                              <i class="fal fa-users-medical fa-lg"></i> Update Project Phase
                            </button>
                          </h5>
                        </div>

                        <div id="collapseOne-<?php echo $row['project_id'];?>" class="collapse" data-parent="#accordion-<?php echo $row['project_id'];?>">
                          <div class="card-body">
                          <form id="add-project-phase-form-<?php echo $row['project_id'];?>" onsubmit="SubmitProjectPhase('<?php echo $row['project_id'];?>');">

                            <div class="col-md-12 col-xs-12 form-group">
                              <label><span class="required">*</span> Project Phase</label><br/><br/>
                              <select name="project_phase" id="project_phase-<?php echo $row['project_id'];?>" class="form-control">

                                <option selected disabled> --Select Project Phase -- </option>
                                <option value="pre_initiating">Pre initiating</option>
                                <option value="initiating">Initiating</option>
                                <option value="planning">Planning</option>
                                <option value="executing">Executing</option>
                                <option value="closure">Closure</option>
                                <option value="benefits_Tracking">Benefits Tracking</option>
                              </select>
                            </div>
                            <div class="col-md-12 mt-5">
                                <button type="submit" class="btn btn-primary btn-block submitting">SUBMIT</button>
                            </div>

                          </form>
                          </div><!--- end of card body -->
                        </div>
                      </div>
                      <div class="card">
                        <div class="card-header bg-light" data-toggle="collapse" data-target="#collapseTwo-<?php echo $row['project_id'];?>">
                          <h5 class="mb-0">
                            <button class="btn btn-link collapsed">
                              <i class="fal fa-tasks fa-lg"></i> Update Project Status
                            </button>
                          </h5>
                        </div>

                        <div id="collapseTwo-<?php echo $row['project_id'];?>" class="collapse" data-parent="#accordion-<?php echo $row['project_id'];?>">
                          <div class="card-body">
                            <form id="add-project-status-form-<?php echo $row['project_id'];?>" onsubmit="SubmitProjectStatus('<?php echo $row['project_id'];?>');">
                              <div class="col-md-12 col-xs-12 form-group">
                                <label><span class="required">*</span> Project Status</label><br/><br/>
                                <select name="project_status" id="project_status-<?php echo $row['project_id'];?>" class="form-control" required>
                                      <option selected disabled> --Select Project Status--</option>
                                      <option value="Active">Active</option>
                                      <option value="Onhold">Onhold</option>
                                      <option value="Rejected">Rejected</option>
                                      <option value="Completed">Completed</option>

                                </select>
                              </div>


                              <div class="col-md-12 mt-6">
                                  <button type="submit" class="btn btn-primary btn-block submitting">SUBMIT</button>
                              </div>

                            </form>
                          </div>
                        </div>
                      </div>

                    </div>
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                  </div>
                </div>
              </div>
            </div>
            <!-- end project phase modal  -->
            <?php

            $Proj_phase = mysqli_query($dbc,"SELECT * FROM pm_projects_update WHERE project_id ='".$row['project_id']."' ORDER BY id");
            if(mysqli_num_rows($Proj_phase) > 0)
            {

                           $result = mysqli_query($dbc, "SELECT * FROM pm_projects_update WHERE project_id ='".$row['project_id']."' ORDER BY id DESC LIMIT 1"  );
                           if(mysqli_num_rows($result))
                           {
                             while($project_phase1 = mysqli_fetch_array($result))
                             {
                               ?>
                               <a class="" href="#" data-toggle="modal" data-target="#project-phase-modal-<?php echo $row['project_id'];?>"
                                 title="Click on <?php echo $project_phase1['project_phase'];?> to update project phase">
                               <span class="text-primary" style="cursor:pointer;"><?php echo $project_phase1['project_phase'];?></span>
                               </a>

                               <?php
                             }
                           }
                           ?>
                 <?php
               }
               else {
                 ?>
                 <a class="" href="#" data-toggle="modal" data-target="#project-phase-modal-<?php echo $row['project_id'];?>"
                   title="Click on <?php echo $row['project_phase'];?> to update project phase">
                 <span class="text-primary" style="cursor:pointer;"><?php echo $row['project_phase'];?></span>
                 </a>

                 <?php

               }
               ?>
           </td>
             <td>
              <?php

              $Proj_phase = mysqli_query($dbc,"SELECT * FROM pm_projects_update_status WHERE project_id ='".$row['project_id']."' ORDER BY id");
              if(mysqli_num_rows($Proj_phase) > 0)
              {

                   $result = mysqli_query($dbc, "SELECT * FROM pm_projects_update_status WHERE project_id ='".$row['project_id']."' ORDER BY id DESC LIMIT 1"  );
                   if(mysqli_num_rows($result))
                   {
                     while($project_phase1 = mysqli_fetch_array($result))
                     {
                       ?>
                       <a class="" href="#" data-toggle="modal" data-target="#project-phase-modal-<?php echo $row['project_id'];?>"
                         title="Click on <?php echo $project_phase1['project_status'];?> to update project status">
                       <span class="text-primary" style="cursor:pointer;"><?php echo $project_phase1['project_status'];?></span>
                       </a>

                       <?php
                     }
                   }
                   ?>
            <?php
            }
            else {

            ?>
            <a class="" href="#" data-toggle="modal" data-target="#project-phase-modal-<?php echo $row['project_id'];?>"
            title="Click to update project status">
            <span class="text-primary" style="cursor:pointer;">click to update</span>
            </a>

            <?php

            }
            ?>
          </td>
           <td><?php echo $row['start_date'] ;?></td>
           <td><?php echo $row['end_date'] ;?></td>
           <td>
             <?php
             $todays_date = date('d-M-y');

             $date1 = new DateTime($row['end_date']); //inclusive
            $date2 = new DateTime($todays_date); //exclusive
            $diff = $date2->diff($date1);
            echo $diff->format("%a");


              ?>


           </td>
           <td>
             <button type="button" class="btn btn-link" data-toggle="modal" data-target="#edit-project-modal-<?php echo $row['project_id'];?>">
              <i class="fad fa-edit text-primary"></i>
            </button>


            <!-- edit project modal -->
            <div class="modal fade" id="edit-project-modal-<?php echo $row['project_id'];?>">
            <div class="modal-dialog modal-lg modal-dialog-scrollable" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <h5 class="modal-title">Modifying Project <strong> Reference No: <?php echo $row['project_id'];?></strong></h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div class="modal-body">
                  <form id="edit-project-form-<?php echo $row['project_id'];?>" class="mt-4" onsubmit="ModifyProject('<?php echo $row['project_id'];?>');">
                    <input type="hidden" value="edit_project" name="edit_project">
                    <input type="hidden" value="<?php echo $row['project_id'];?>" name="id">
                    <div class="row"><h6>Project Summary</h6></div>
                    <div class="row bg-light">
                        <div class="col-lg-6 col-xs-12 form-group">
                          <label for="strategic_objective"><span class="required">*</span>Strategic Objectives</label>
                          <?php
                          $result = mysqli_query($dbc, "SELECT * FROM strategic_objectives ");
                          //mapping objecting with id
                          $mapped_result = mysqli_fetch_array(mysqli_query($dbc, "SELECT * FROM strategic_objectives WHERE
                                                          strategic_objective_id='".$row['strategic_objective_id']."'"));
                          ?>
                          <select name="strategic_objective" id="strategic_objective-<?php echo $row['project_id'];?>" class="select2 form-control">
                          <option value="<?php echo $row['strategic_objective_id'];?>" selected><?php echo $mapped_result['strategic_objective_description'];?></option>
                          <?php
                          while($row_result = mysqli_fetch_array($result)) {
                              ?><option value="<?php echo $row_result['strategic_objective_id'];?>"><?php echo $row_result['strategic_objective_description'];?></option><?php
                          }
                          ?>
                          </select>
                        </div>
                        <div class="col-lg-6 col-xs-12 form-group">
                            <label for="project_name"><span class="required">*</span>Project Name</label>
                            <textarea name="project_name" id="project_name-<?php echo $row['project_id'];?>" class="form-control" required><?php echo $row['project_name'];?></textarea>
                        </div>
                        <div class="col-lg-6 col-xs-12 form-group">
                            <label for="project_name"><span class="required">*</span>Project Description</label>
                            <textarea name="project_description" id="project_description-<?php echo $row['project_id'];?>" class="form-control" required><?php echo $row['project_description'];?></textarea>
                        </div>
                        <div class="col-lg-6 col-xs-12 form-group">
                            <label>Related Workplan Activity</label>
                            <?php
                            $result = mysqli_query($dbc,"SELECT activity_id FROM pm_project_attached_workplans WHERE project_id='".$row['project_id']."' && changed='no'");

                            //mapping objecting with id
                            $mapped_result = mysqli_fetch_array(mysqli_query($dbc, "SELECT * FROM perfomance_management WHERE
                                                            activity_id='".$row['activity_id']."'"));
                            ?>
                            <select name="related_workplan_activity[]" multiple="multiple" id="related_workplan_activity-<?php echo $row['project_id'];?>" class="select2 form-control">
                            <?php
                            while($row_result = mysqli_fetch_array($result)) {
                               $activity_description = mysqli_fetch_array(mysqli_query($dbc,"SELECT activity_description,department_id FROM perfomance_management WHERE
                                       activity_id='".$row_result['activity_id']."'"));
                                ?><option value="<?php echo $row_result['activity_id'];?>" selected><?php echo $activity_description['activity_description'];?>
                                  (<?php echo $activity_description['department_id'];?>)
                                </option><?php
                            }
                            ?>
                            <?php
                                $activities = mysqli_query($dbc,"SELECT activity_id,department_id,activity_description FROM perfomance_management
                                                                        WHERE activity_status='open' ORDER BY department_id ASC");
                                while($row_activities = mysqli_fetch_array($activities))
                                {
                                  ?>
                                  <option value="<?php echo $row_activities['activity_id'];?>"><?php echo $row_activities['activity_description'];?>
                                    (<?php echo $row_activities['department_id'];?>)
                                  </option>
                                  <?php
                                }

                             ?>
                            </select>

                        </div>
                    </div>
                    <!-- start row project timelines -->
                    <div class="row mt-3"><h6>Timelines</h6></div>
                    <div class="row bg-light">
                      <div class="col-lg-4 col-xs-12 form-group">
                        <label><span class="required">*</span>Project Start Date</label>
                        <div class="input-group mb-3">
                          <div class="input-group-prepend">
                            <span class="input-group-text"><i class="fal fa-calendar-day"></i></span>
                          </div>
                          <input type="text" class="form-control pull-right" id="project_start_date-<?php echo $row['project_id'];?>"
                                  value="<?php echo $row['start_date'];?>" name="project_start_date" required
                                  onchange="ChangeStartDate('<?php echo $row['project_id'] ;?>');"
                                  onmousedown="ChangeStartDate('<?php echo $row['project_id'] ;?>');">
                        </div>
                      </div>
                      <div class="col-lg-4 col-xs-12 form-group">
                        <label><span class="required">*</span>Project End Date</label>
                        <div class="input-group mb-3">
                          <div class="input-group-prepend">
                            <span class="input-group-text"><i class="fad fa-calendar-day"></i></span>
                          </div>
                          <input type="text" class="form-control pull-right" id="project_end_date-<?php echo $row['project_id'];?>"
                                  value="<?php echo $row['end_date'];?>" name="project_end_date" required
                                  onchange="ChangeEndDate('<?php echo $row['project_id'] ;?>');"
                                  onmousedown="ChangeEndDate('<?php echo $row['project_id'] ;?>');">
                        </div>
                      </div>
                      <div class="col-lg-4 col-xs-12 form-group">
                          <label><span class="required">*</span>Duration</label>
                          <input type="hidden" class="form-control " name="duration" id="duration-project-duration-in-days-<?php echo $row['project_id'];?>"
                                value="<?php echo $row['duration'];?>" readonly required>
                          <input type="text" class="form-control pull-right bg-grey" id="project-duration-<?php echo $row['project_id'];?>" value="<?php echo $row['duration'];?>" readonly required>
                      </div>
                    </div>
                    <!-- end row project timelines -->

                    <!-- start project budget -->
                    <?php
                    //select from the m_budget table
                    $sql_pm_budget = mysqli_query($dbc,"SELECT * FROM pm_budget WHERE project_id='".$row['project_id']."'");
                  //  while($budget = mysqli_fetch_array($sql_pm_budget))
                  //  {
                   $budget_1 = mysqli_fetch_array($sql_pm_budget);
                   $budget_id_1 = $budget_1['id'];
                   $fa = strtoupper($budget_1['funding_agency']);

                      ?>
                      <div class="row">
                        <small class="status text-success"></small><br/>
                      </div>
                      <div class="row  mt-3"><h6>Budget</h6></div>
                      <div class="row bg-light">
                        <div class="col-lg-12 col-xs-12 form-group">
                            <label><span class="required">*</span>Funding Agency</label>
                            <select class="form-control select2" data-tags="true" id="funding-agency-<?php echo $row['project_id'];?>" data-placeholder="Select Funding Agency" name="funding_agency" required>
                              <option value="<?php echo $budget_1['funding_agency'];?>" selected><?php echo $budget_1['funding_agency'];?></option>
                              <option value="CMA">CMA</option>
                              <option value="FSSP">FSSP</option>
                              <option value="CMA & FSSP">CMA & FSSP</option>
                            </select>
                      </div>
                    </div>
                        <!-- start project budget -->
                        <?php
                        $sql_pm_budget = mysqli_query($dbc,"SELECT * FROM pm_budget WHERE project_id='".$row['project_id']."'");
                        while($budget = mysqli_fetch_array($sql_pm_budget))
                        {
                          ?>
                       <div class="row budget-row bg-light">
                         <div class="form-group col-lg-3">
                           <label><span class="required">*</span>Budget Line</label>
                           <select class=" form-control budget-line-<?php echo $row['project_id'];?>" name="budget_line[]" title="Select Budget Line" required>
                             <option value="<?php echo $budget['budget_line'];?>" selected><?php echo $budget['budget_line'];?></option>
                             <option value="internal">Internal</option>
                             <option value="external">External</option>
                           </select>
                         </div>
                         <div class="form-group col-lg-3">
                           <label><span class="required">*</span>Currency</label>
                           <select class="fa fa-sm form-control currency-<?php echo $row['project_id'];?>" name="currency[]" title="Select Currency" required>
                             <option value="<?php echo $budget['currency_type'];?>" selected>-<?php echo $budget['currency_type'];?>-</option>
                             <option value="KES">(KES) Kshs</option>
                             <option value="USD">(&#xf155;)Usd</option>
                             <option value="EURO">(&#xf153;)Euro</option>
                             <option value="POUND">(&#xf154;)Pound</option>
                           </select>
                         </div>
                         <div class="form-group col-lg-3">
                           <label><span class="required">*</span>Amount</label>
                           <input type="number" min="0" class="form-control internal-budget-value amount-<?php echo $row['project_id'];?>"
                                value="<?php echo $budget['amount'];?>" name="amount[]" required>
                         </div>
                         <div class="form-group col-lg-3">
                             <br/>
                               <button class="btn btn-link btn-add ml-2 add-budget-info-edit" type="button" title="Add New">
                                   <span class="fad fa-plus-circle"></span>
                               </button>
                         </div>
                         <button class="btn btn-link btn-add ml-2 remove-budget-info-edit" type="button" title="Add New">
                             <span class="fad fa-minus-circle text-danger"></span>
                         </button>
                        </div>
                        <?php
                        }
                         ?>

                        <div class="dynamic-form-budget-edit"></div>

                        <!-- end project budget -->



                    <!--start row project ownerships -->
                    <div class="row  mt-3"><h6>Stakeholders</h6></div>
                    <div class="row bg-light">
                      <div class="col-lg-4 col-xs-12 form-group">
                          <label><span class="required">*</span>Project Owner</label>
                          <?php
                          $result = mysqli_query($dbc, "SELECT * FROM staff_users  WHERE designation!='TEST USER' && status = 'active' ORDER BY Name ASC");
                          //mapping objecting with id
                          $mapped_result = mysqli_fetch_array(mysqli_query($dbc, "SELECT * FROM pm_projects WHERE
                                                          project_owner='".$row['project_owner']."'"));
                          ?>
                          <select name="project_owner" id="project_owner-<?php echo $row['project_id'];?>" class="select2 form-control">
                          <option value="<?php echo $row['project_owner'];?>"selected><?php echo $mapped_result['project_owner'];?></option>
                          <?php
                          while($row_result = mysqli_fetch_array($result)) {
                              ?><option value="<?php echo $row_result['Name'];?>"><?php echo $row_result['Name'];?></option><?php
                          }
                          ?>
                          </select>
                      </div>
                      <div class="col-lg-4 col-xs-12 form-group">
                          <label><span class="required">*</span>Senior User</label>
                          <?php
                          $result = mysqli_query($dbc, "SELECT * FROM staff_users  WHERE designation!='TEST USER' && status = 'active' ORDER BY Name ASC");
                          //mapping objecting with id
                          $mapped_result = mysqli_fetch_array(mysqli_query($dbc, "SELECT * FROM pm_projects WHERE
                                                          senior_user='".$row['senior_user']."'"));
                          ?>
                          <select data-column-name-projects-user="senior_user:<?php echo $mapped_result['id'];?>" name="senior_user[]" id="senior_user-<?php echo $row['project_id'];?>"
                                  class="select2 form-control editable-project-user" data-tags="true" multiple="multiple">
                          <option value="<?php echo $row['senior_user'];?>" selected><?php echo $row['senior_user'];?></option>
                          <?php
                          while($row_result = mysqli_fetch_array($result)) {
                              ?><option value="<?php echo $row_result['Name'];?>"><?php echo $row_result['Name'];?></option><?php
                          }
                          ?>
                          </select>
                      </div>
                      <div class="col-lg-4 col-xs-12 form-group">
                          <label><span class="required">*</span>Senior Contractor</label>
                          <?php
                          $result = mysqli_query($dbc, "SELECT * FROM pm_contractors  WHERE status='active' ORDER BY contractor_name ASC");
                          //mapping objecting with id
                          $mapped_result = mysqli_fetch_array(mysqli_query($dbc, "SELECT * FROM pm_projects WHERE
                                                          senior_contractor='".$row['senior_contractor']."'"));
                          ?>
                          <select name="senior_contractor" id="senior_contractor-<?php echo $row['project_id'];?>" class="select2 form-control">
                          <option value="<?php echo $row['senior_contractor'];?>" selected><?php echo $mapped_result['senior_contractor'];?></option>
                          <?php
                          while($row_result = mysqli_fetch_array($result)) {
                              ?><option value="<?php echo $row_result['contractor_name'];?>"><?php echo $row_result['contractor_name'];?></option><?php
                          }
                          ?>
                          </select>

                      </div>
                      <div class="col-lg-4 col-xs-12 form-group">
                          <label><span class="required">*</span>Project Advisor</label>
                          <?php
                          $result = mysqli_query($dbc, "SELECT * FROM staff_users  WHERE designation!='TEST USER' && status = 'active' ORDER BY Name ASC");
                          //mapping objecting with id
                          $mapped_result = mysqli_fetch_array(mysqli_query($dbc, "SELECT * FROM pm_projects WHERE
                                                          project_advisor='".$row['project_advisor']."'"));
                          ?>
                          <select data-column-name-projects_advisor="project_advisor:<?php echo $mapped_result['id'];?>" name="project_advisor[]" id="project_advisor-<?php echo $row['project_id'];?>"
                                  class="select2 form-control editable-project-advisor" data-tags="true" multiple="multiple">
                          <option value="<?php echo $mapped_result['project_advisor'];?>" selected><?php echo $mapped_result['project_advisor'];?></option>
                          <?php
                          while($row_result = mysqli_fetch_array($result)) {
                              ?><option value="<?php echo $row_result['Name'];?>"><?php echo $row_result['Name'];?></option><?php
                          }
                          ?>
                          </select>
                      </div>
                      <div class="col-lg-4 col-xs-12 form-group">
                          <label><span class="required">*</span>Project Manager</label>
                          <?php
                          $result = mysqli_query($dbc, "SELECT * FROM staff_users  WHERE designation!='TEST USER' && status = 'active' ORDER BY Name ASC");
                          //mapping objecting with id
                          $mapped_result = mysqli_fetch_array(mysqli_query($dbc, "SELECT * FROM pm_projects WHERE
                                                          project_manager='".$row['project_manager']."'"));
                          ?>
                          <select name="project_manager" id="project_manager-<?php echo $row['project_id'];?>" class="select2 form-control">
                          <option value="<?php echo $row['project_manager'];?>" selected><?php echo $mapped_result['project_manager'];?></option>
                          <?php
                          while($row_result = mysqli_fetch_array($result)) {
                              ?><option value="<?php echo $row_result['Name'];?>"><?php echo $row_result['Name'];?></option><?php
                          }
                          ?>
                          </select>
                      </div>
                    </div>
                    <!-- end row project owenerships -->
                    <div class="row">
                      <small class="status-project-user text-success"></small><br/>
                    </div>

                    <!-- start row related activity -->
                    <div class="row  mb-4">


                    </div>
                    <!-- end row related activity -->

                    <div class="pull-left mt-4">
                      <small class="text-muted">Modified by:- <?php echo $_SESSION['name'];?></small>
                    </div>

                          <!-- start row button -->
                    <div class="row">
                      <div class="col-md-12 text-center">
                          <button type="submit" class="btn btn-primary btn-block font-weight-bold submitting">SUBMIT
                          </button>
                      </div>
                    </div>

                          <!-- end row button -->
                  </form>
                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
              </div>
            </div>
            </div>
            <!-- end of edit project modal -->

           </td>
           <td>
             <a href="#" class="btn btn-link" onclick="CloseProject('<?php echo $row['id'];?>');">
                <i class="fad fa-trash-alt text-danger"></i>
             </a>
           </td>
         </tr>
         <?php
            }
          ?>
       </table>
      </div>
    </div>
  </div>
</div>



<!-- add project modal -->
<div class="modal fade" id="add-project-modal" onmouseenter="CallSmartWizard()">
<div class="modal-dialog modal-lg modal-dialog-scrollable" role="document">
  <div class="modal-content">
    <div class="modal-header bg-light">
      <h5 class="modal-title">Creating Project</h5>
      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
    <div class="modal-body">
      <form id="add-project-form" class="needs-validation" enctype="multipart/form-data">
        <input type="hidden" value="add-project" name="add-project">
        <div id="smartwizard-add-project" style="height:400px;">
          <ul class="nav">
             <li>
                 <a class="nav-link" href="#step-1">
                    Project Summary
                 </a>
             </li>
             <li>
                 <a class="nav-link" href="#step-2">
                    Timelines
                 </a>
             </li>
             <li>
                 <a class="nav-link" href="#step-3">
                    Budget
                 </a>
             </li>
             <li>
                 <a class="nav-link" href="#step-4">
                    Stakeholders
                 </a>
             </li>
          </ul>

          <div class="tab-content" style="min-height:300px;overflow-y: scroll">
             <div id="step-1" class="tab-pane" role="tabpanel">
               <div class="row">

                 <div class="col-lg-6 col-xs-12 form-group">
                     <label><span class="required">*</span>Project Name</label>
                     <textarea name="project_name" class="form-control project-name" maxlength="250" required style="overflow-y: scroll;  max-height: 100px;resize: none;"></textarea>
                 </div>

                 <div class="col-lg-6 col-xs-12 form-group">
                     <label><span class="required">*</span>Project Description</label>
                     <textarea name="project_description" class="form-control project-description" maxlength="500" required style="overflow-y: scroll;  max-height: 100px;resize: none;"></textarea>

                 </div>


                 </div>
                 <div class="row mb-3">
                   <div class="col-lg-6 col-xs-12 form-group">
                     <label><span class="required">*</span>Strategic Objectives</label>
                     <?php
                     $result = mysqli_query($dbc, "SELECT * FROM strategic_objectives");
                     echo '
                     <select name="strategic_objective" class="select2 form-control strategic-objective" data-placeholder="Select Strategic Objective" required>
                     <option></option>';
                     while($row = mysqli_fetch_array($result)) {
                       // we're sending the strategic objective id to the db
                         echo '<option value="'.$row['strategic_objective_id'].'">'.$row['strategic_objective_description']."</option>";
                     }
                     echo '</select>';
                     ?>
                   </div>


                     <div class="col-lg-6 col-xs-12 form-group">
                         <label>Related Workplan Activity</label>
                         <?php
                         $result = mysqli_query($dbc, "SELECT * FROM perfomance_management WHERE activity_status='open' ORDER BY department_id ASC");
                         ?>
                           <select name="related_workplan_activity[]" data-tags="false" multiple="multiple" class="select2 form-control" data-placeholder="Select Related Workplan Activity">
                             <option></option>
                         <?php
                         while($row = mysqli_fetch_array($result)) {
                           ?>

                             <option value="<?php echo $row['activity_id'];?>"> - <?php echo $row['activity_description'];?> (<?php echo $row['department_id'];?>)</option>
                           <?php
                         }
                         ?>
                         </select>
                     </div>
               </div>
             </div>
             <div id="step-2" class="tab-pane" role="tabpanel">
               <!-- start row project timelines -->
               <div class="row ">
                 <div class="col-lg-4 col-xs-12 form-group">
                   <label><span class="required">*</span>Project Start Date</label>
                   <div class="input-group mb-3">
                     <div class="input-group-prepend">
                       <span class="input-group-text"><i class="fal fa-calendar-day"></i></span>
                     </div>
                     <input type="text" class="form-control pull-right project_start_date" name="project_start_date" required>

                   </div>
                 </div>
                 <div class="col-lg-4 col-xs-12 form-group">
                   <label><span class="required">*</span>Project End Date</label>
                   <div class="input-group mb-3">
                     <div class="input-group-prepend">
                       <span class="input-group-text"><i class="fad fa-calendar-day"></i></span>
                     </div>
                     <input type="text" class="form-control pull-right project_end_date" name="project_end_date" required>

                   </div>
                 </div>
                 <div class="col-lg-4 col-xs-12 form-group">
                     <label><span class="required">*</span>Duration</label>
                     <input type="hidden" class="form-control project-duration-in-days" name="duration" readonly required>
                     <input type="text" class="form-control pull-right project-duration bg-grey" readonly required>
                 </div>
                 <div class="col-md-4 col-xs-12 form-group">
                   <label><span class="required">*</span> Project Phase</label>
                   <select name="project_phase" class="form-control" required>

                     <option selected disabled> --Select Project Phase -- </option>
                     <option value="pre_initiating">Pre initiating</option>
                     <option value="initiating">Initiating</option>
                     <option value="planning">Planning</option>
                     <option value="executing">Executing</option>
                     <option value="closure">Closure</option>
                     <option value="benefits_Tracking">Benefits Tracking</option>
                   </select>

                 </div>
               </div>
               <!-- end row project timelines -->
             </div>
             <div id="step-3" class="tab-pane" role="tabpanel">
               <!-- start project budget -->
               <div class="row ">
                 <div class="col-lg-4 col-xs-12 form-group">
                     <label><span class="required">*</span>Funding Agency</label>
                     <select class="form-control select2 funding-agency" data-tags="true" data-placeholder="Select Funding Agency"
                             name="funding_agency" required>
                       <option></option>
                       <option value="CMA">CMA</option>
                       <option value="FSSP">FSSP</option>
                       <option value="CMA & FSSP">CMA & FSSP</option>
                     </select>

                 </div>
              </div>
              <div class="row budget-row">
                <div class="form-group col-lg-3">
                  <label><span class="required">*</span>Budget Line</label>
                  <select class=" form-control" name="line[]" title="Select Budget Line">
                    <option value="">-Choose-</option>
                    <option value="internal">Internal</option>
                    <option value="external">External</option>
                  </select>
                </div>
                <div class="form-group col-lg-3">
                  <label><span class="required">*</span>Currency</label>
                  <select class="fa fa-sm form-control" name="currency[]" title="Select Currency">
                    <option value="">-&#xf3d1;-</option>
                    <option value="KES">(KES) Kshs</option>
                    <option value="USD">(&#xf155;)Usd</option>
                    <option value="EURO">(&#xf153;)Euro</option>
                    <option value="POUND">(&#xf154;)Pound</option>
                  </select>
                </div>
                <div class="form-group col-lg-3">
                  <label><span class="required">*</span>Amount</label>
                  <input type="number" min="0" class="form-control internal-budget-value" value="0" name="budget[]">
                </div>
                <div class="form-group col-lg-3">
                    <br/>
                      <button class="btn btn-link btn-add ml-2 add-budget-info" type="button" title="Add New">
                          <span class="fad fa-plus-circle"></span>
                      </button>
                </div>
               </div>

               <div class="dynamic-form-budget"></div>

               <!-- end project budget -->
             </div>
             <div id="step-4" class="tab-pane" role="tabpanel">
               <!--start row project ownerships -->
               <div class="row ">
                 <div class="col-lg-4 col-xs-12 form-group">
                     <label><span class="required">*</span>Project Owner</label>
                     <?php
                     $result = mysqli_query($dbc, "SELECT * FROM staff_users  WHERE designation!='TEST USER' && status = 'active' ORDER BY Name ASC");
                     echo '
                     <select name="project_owner" data-tags="false" class="select2 form-control" data-placeholder="Select Project Owner" required>
                     <option></option>';
                     while($row = mysqli_fetch_array($result)) {
                         echo '<option value="'.$row['Name'].'">'.$row['Name']."</option>";
                     }
                     echo '</select>';
                     ?>

                 </div>
                 <div class="col-lg-4 col-xs-12 form-group">
                     <label><span class="required">*</span>Senior User</label>
                     <?php
                     $result = mysqli_query($dbc, "SELECT * FROM staff_users  WHERE designation!='TEST USER' && status = 'active' ORDER BY Name ASC");
                     echo '
                     <select name="senior_user[]" data-tags="true" class="select2 form-control" required multiple="multiple"
                     data-placeholder="Select user" required>

                     <option></option>';
                     while($row = mysqli_fetch_array($result)) {
                         echo '<option value="'.$row['Name'].'">'.$row['Name']."</option>";
                     }
                     echo '</select>';
                     ?>

                 </div>
                 <div class="col-lg-4 col-xs-12 form-group">
                     <label><span class="required">*</span>Senior Contractor</label>
                     <?php
                     $result = mysqli_query($dbc, "SELECT * FROM pm_contractors  WHERE status='active' ORDER BY contractor_name ASC");
                     echo '
                     <select name="senior_contractor" data-tags="true" class="select2 form-control" data-placeholder="Select Senior Contractor" required>
                     <option></option>';
                     while($row = mysqli_fetch_array($result)) {
                         echo '<option value="'.$row['contractor_name'].'">'.$row['contractor_name']."</option>";
                     }
                     echo '</select>';
                     ?>
                 </div>
                 <div class="col-lg-4 col-xs-12 form-group">
                     <label><span class="required">*</span>Project Advisor</label>
                     <?php
                     $result = mysqli_query($dbc, "SELECT * FROM staff_users  WHERE designation!='TEST USER' && status = 'active' ORDER BY Name ASC");
                     echo '
                     <select name="project_advisor[]" data-tags="true" class="select2 form-control" required multiple="multiple"
                     data-placeholder="Select user" required>

                     <option></option>';
                     while($row = mysqli_fetch_array($result)) {
                         echo '<option value="'.$row['Name'].'">'.$row['Name']."</option>";
                     }
                     echo '</select>';
                     ?>

                 </div>
                 <div class="col-lg-4 col-xs-12 form-group">
                     <label><span class="required">*</span>Project Manager</label>
                     <?php
                     $result = mysqli_query($dbc, "SELECT * FROM staff_users  WHERE designation!='TEST USER' && status = 'active' ORDER BY Name ASC");
                     echo '
                     <select name="project_manager" class="select2 form-control" required
                     data-placeholder="Select user" required>

                     <option></option>';
                     while($row = mysqli_fetch_array($result)) {
                         echo '<option value="'.$row['Name'].'">'.$row['Name']."</option>";
                     }
                     echo '</select>';
                     ?>

                 </div>
               </div>
               <!-- end row project owenerships -->
             </div>
          </div>
      </div>


        <div class="pull-left mt-4 d-none">
          <small class="text-muted">Recorded by:- <?php echo $_SESSION['name'];?></small>
        </div>

              <!-- start row button -->
        <div class="row submit-project-btn d-none">
          <div class="col-md-12 text-center">
              <button type="submit" class="btn btn-primary btn-block font-weight-bold submitting">SUBMIT</button>
          </div>
        </div>

              <!-- end row button -->
      </form>
    </div>
    <div class="modal-footer">
      <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
    </div>
  </div>
</div>
</div>
<!-- end of add project modal -->

<script>
$("#smartwizard-add-project").on("leaveStep", function(e, anchorObject, stepNumber, stepDirection) {
    if(stepDirection == "3")
    {
      $('.sw-btn-group-extra').removeClass('d-none');
    }
    else
    {
      $('.sw-btn-group-extra').addClass('d-none');
    }
});

var dynamic_budget = `<div class="row">
                <div class="form-group col-lg-3">
                  <label><span class="required">*</span>Budget Line</label>
                  <select class=" form-control" name="line[]" title="Select Budget Line">
                    <option value="">-Choose-</option>
                    <option value="internal">Internal</option>
                    <option value="external">External</option>
                  </select>
                </div>
                <div class="form-group col-lg-3">
                  <label><span class="required">*</span>Currency</label>
                  <select class="fa fa-sm form-control" name="currency[]" title="Select Currency">
                    <option value="">-&#xf3d1;-</option>
                    <option value="KES">(KES) Kshs</option>
                    <option value="USD">(&#xf155;)Usd</option>
                    <option value="EURO">(&#xf153;)Euro</option>
                    <option value="POUND">(&#xf154;)Pound</option>
                  </select>
                </div>
                <div class="form-group col-lg-3">
                  <label><span class="required">*</span>Currency</label>
                  <input type="number" min="0" class="form-control internal-budget-value" value="0" name="budget[]">
                </div>
                    <br/>
                      <button class="btn btn-link btn-add ml-2 remove-budget-info" type="button" title="Add New">
                          <span class="fad fa-minus-circle text-danger"></span>
                      </button>
                </div>
                `;


var dynamic_budget_edit = `<div class="row">
                                <div class="form-group col-lg-3">
                                  <label><span class="required">*</span>Budget Line</label>
                                  <select class=" form-control" name="budget_line[]" title="Select Budget Line">
                                    <option value="">-Choose-</option>
                                    <option value="internal">Internal</option>
                                    <option value="external">External</option>
                                  </select>
                                </div>
                                <div class="form-group col-lg-3">
                                  <label><span class="required">*</span>Currency</label>
                                  <select class="fa fa-sm form-control" name="currency[]" title="Select Currency">
                                    <option value="">-&#xf3d1;-</option>
                                    <option value="KES">(KES) Kshs</option>
                                    <option value="USD">(&#xf155;)Usd</option>
                                    <option value="EURO">(&#xf153;)Euro</option>
                                    <option value="POUND">(&#xf154;)Pound</option>
                                  </select>
                                </div>
                                <div class="form-group col-lg-3">
                                  <label><span class="required">*</span>Currency</label>
                                  <input type="number" min="0" class="form-control internal-budget-value" value="0" name="amount[]">
                                </div>
                                    <br/>
                                      <button class="btn btn-link btn-add ml-2 remove-budget-info-edit" type="button" title="Add New">
                                          <span class="fad fa-minus-circle text-danger"></span>
                                      </button>
                                </div>
                                `;

$(document).on("click",'.add-budget-info', function(e){
  e.preventDefault();
  $(".dynamic-form-budget").append(dynamic_budget);
});
$(document).on("click",'.remove-budget-info', function(e){
e.preventDefault();
  $(this).parent('div').remove();
});


$(document).on("click",'.add-budget-info-edit', function(e){
  e.preventDefault();
  $(".dynamic-form-budget-edit").append(dynamic_budget_edit);
});
$(document).on("click",'.remove-budget-info-edit', function(e){
e.preventDefault();
  $(this).parent('div').remove();
});
</script>
