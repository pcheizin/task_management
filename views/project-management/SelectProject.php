<?php
session_start();
include("../../controllers/setup/connect.php");

if(!$_SERVER['REQUEST_METHOD'] == "POST")
{
  exit();
}

if (!isset($_SESSION['email']))
{
   exit("<a href='#' class='login-link'>Please Log in to continue</a>");
}
//$project_module = $_POST['project_module'];
?>
<nav aria-label="breadcrumb">
      <ol class="breadcrumb">
        <li class="breadcrumb-item active" aria-current="page">Query Task in respective Task categories and department</li>
      </ol>
</nav>

<div class="card card-body">
 <form id="task_category_form">
    <div class="row">
      <div class="col-lg-3 col-xs-12 form-group">
         <label for="departments"><span class="required">*</span>Departments</label>
         <?php
            $result = mysqli_query($dbc, "SELECT * FROM departments");
            echo '
            <select name="department_name" id="department_name" class="select2 form-control" required>
            <option value="">search and select...</option>';
            while($row = mysqli_fetch_array($result)) {
                echo '<option value="'.$row['department_name'].'">'.$row['department_name']."</option>";
            }
            echo '</select>';
            ?>
      </div>
      <div class="col-lg-3 col-xs-12 form-group">
         <label for="departments"><span class="required">*</span>Task Category</label>
         <?php
            $result = mysqli_query($dbc, "SELECT * FROM pm_milestones");
            echo '
            <select name="milestone_id" id="milestone_id" class="select2 form-control" required>
            <option value="">search and select...</option>';
            while($row = mysqli_fetch_array($result)) {
                echo '<option value="'.$row['id'].'">'.$row['milestone_name']."</option>";
            }
            echo '</select>';
            ?>
      </div>
        <div class="col-lg-3 col-xs-12 form-group">
           <label></label><br/>
           <button class="btn btn-primary">Query</button>
        </div>
        </div>
     </form>
</div>
    <!-- /.card -->
 <div id="generated-report-data"></div>
