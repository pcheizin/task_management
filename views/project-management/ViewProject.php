
<?php
session_start();
include("../../controllers/setup/connect.php");
if (!isset($_SESSION['email']))
{
     exit("<a href='#' class='login-link'>Please Log in to continue</a>");
}
if(!isset($_POST['id']))
{
  exit("Please select project");
}

$project = mysqli_fetch_array(mysqli_query($dbc,"SELECT * FROM pm_projects WHERE id='".$_POST['id']."'"));
?>
<nav aria-label="breadcrumb">
     <ol class="breadcrumb">

      <li class="breadcrumb-item active" aria-current="page">
          Project: <strong> <?php echo $project['project_name'];?> </strong>
      </li>

     </ol>
</nav>
<br/>

<input type="hidden" name="project-id" class="project-id" value="<?php echo $project['id'] ;?>">


<div class="row">
  <div class="col-lg-12">
    <ul class="nav nav-tabs nav-fill" role="tablist">
      <li class="nav-item">
        <a class="nav-link project-files-tab" data-toggle="tab" href="#project-files-tab"
          role="tab" aria-selected="false">
           <i class="fad fa-file-alt fa-lg"></i> Project Files
        </a>
      </li>
      <li class="nav-item">
        <a class="nav-link milestones-tab" data-toggle="tab" href="#milestones-tab"
          role="tab" aria-selected="false">
           <i class="fad fa-map-signs fa-lg"></i> Milestones
        </a>
      </li>
      <li class="nav-item">
        <a class="nav-link project-resource-plan-tab" data-toggle="tab" href="#project-resource-plan-tab" role="tab"
           aria-selected="false">
           <i class="fad fa-people-carry fa-lg"></i> Resource Plan
        </a>
      </li>
      <li class="nav-item">
        <a class="nav-link project-risks-tab" data-toggle="tab" href="#project-risks-tab" role="tab"
           aria-selected="false">
           <i class="fad fa-exclamation-triangle fa-lg"></i> Risks
        </a>
      </li>
      <li class="nav-item">
        <a class="nav-link project-lessons-learnt-tab" data-toggle="tab" href="#project-lessons-learnt-tab" role="tab"
            aria-controls="contact" aria-selected="false">
            <i class="nav-icon fal fa-chalkboard-teacher fa-lg"></i> Lessons learnt
        </a>
      </li>
      <li class="nav-item">
        <a class="nav-link project-issue-logs-tab" data-toggle="tab" href="#project-issue-logs-tab" role="tab"
            aria-controls="contact" aria-selected="false">
            <i class="fad fa-pennant fa-lg"></i> Issue Logs
        </a>
      </li>
      <li class="nav-item">
        <a class="nav-link project-payments-tab" data-toggle="tab" href="#project-payments-tab" role="tab"
            aria-controls="contact" aria-selected="false">
           <i class="fad fa-money-check-alt fa-lg"></i>  Payments
        </a>
      </li>
    </ul>
    <div class="tab-content">
      <div class="tab-pane fade" id="project-files-tab" role="tabpanel"></div>

      <div class="tab-pane fade" id="milestones-tab" role="tabpanel"></div>

      <div class="tab-pane fade" id="project-resource-plan-tab" role="tabpanel"></div>

      <div class="tab-pane fade" id="project-risks-tab" role="tabpanel"></div>

      <div class="tab-pane fade" id="project-lessons-learnt-tab" role="tabpanel"></div>

      <div class="tab-pane fade" id="project-issue-logs-tab" role="tabpanel"></div>

      <div class="tab-pane fade" id="project-payments-tab" role="tabpanel"></div>

    </div>

  </div>
</div>




<!--PROJECT MODALS -->
