<?php
session_start();
include("../../controllers/setup/connect.php");
if (!isset($_SESSION['email']))
{
     exit("<a href='#' class='login-link'>Please Log in to continue</a>");
}
$project = mysqli_fetch_array(mysqli_query($dbc,"SELECT * FROM pm_projects WHERE id='".$_POST['project_id']."'"));
?>

<div class="col-lg-12 col-xs-12">
  <div class="card card-primary card-outline">
    <div class="card-header">
      Task Files
      <button class="btn btn-link" style="float:right;"
              data-toggle="modal" data-target="#add-task-file-modal">
              <i class="fa fa-plus-circle"></i> Add Task File
      </button>
    </div>
    <div class="card-body table-responsive">
     <table class="table table-striped table-bordered table-hover" id="project-files-table" style="width:100%">
       <thead>
         <tr>
           <td>#</td>
           <td>File Name</td>
           <td>recorded_by By</td>
           <td>Date Uploaded</td>
           <td>Delete</td>
         </tr>
       </thead>
       <?php
       $no = 1;
        $sql_project_files = mysqli_query($dbc,"SELECT * FROM pm_task_files ORDER BY id DESC");
        while ($row_files = mysqli_fetch_array($sql_project_files))
        {
          ?>
          <tr style="cursor: pointer;">
            <td width="50px"><?php echo $no++;?>.</td>
            <td>
                <a class="border-bottom" href="views/project-management/documents/<?php echo $row_files['document'] ;?>"
                   target="_blank" title="Click to download">
                   <?php echo $row_files['document'] ;?>
               </a>
            </td>
            <td><?php echo $row_files['recorded_by'];?></td>

                  <td><?php echo $row_files['date_recorded'];?></td>

            <td>
                <a class="btn" href="#" onclick="RemoveProjectFile('<?php echo $row_files['id'] ;?>','<?php echo $row_files['document'] ;?>');"
                   title="Click to remove <?php echo $row_files['document'] ;?> ">
                   <i class="fas fa-file-times fa-lg text-danger"></i>
                </a>
            </td>
          </tr>
          <?php
        }
        ?>
     </table>
    </div>
  </div>
</div>

<!-- start add project file form -->
<div class="modal fade" id="add-task-file-modal" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header bg-light">
        <h5 class="modal-title">Task Files</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
          <form id="add-task-document-form" class="mt-4" enctype="multipart/form-data">

              <input type="hidden" name="add-task-document" value="add-task-document">
          <!-- start row project files -->
        <div class="row mx-3">
          <small class="text-muted">(Attach Task Files i.e Contract Documents and Other Relevant Documents)</small><br/><br/>


          <div class="col-lg-12 col-xs-12 form-group">
             <label for="name"><span class="required">*</span>Task List</label>
             <?php
                $result = mysqli_query($dbc, "SELECT * FROM pm_activities");
                echo '
                <select name="activity_name" id="activity_name" class="select2 form-control" required>
                <option value="">search and select...</option>';
                while($row = mysqli_fetch_array($result)) {
                    echo '<option value="'.$row['activity_name'].'">'.$row['activity_name']."</option>";
                }
                echo '</select>';
                ?>
          </div>

                    <div class="col-lg-12 col-xs-12">
                        <label>Task Documents</label>
                      <div class="input-group mb-3">
                        <div class="input-group-prepend">
                          <span class="input-group-btn">
                              <span class="btn btn-primary btn-file project-file">
                                  <i class="fal fa-file-alt"></i>  Browse &hellip; <input type="file" name="additional_file" class="form-control delivery-note-document" single>
                              </span>
                          </span>
                        </div>
                        <input type="text" class="form-control bg-white delivery-note-document-label" readonly>
                      </div>
                      <div class="row delivery-note-document-error"></div>

                    </div>
        </div>
        <!-- end row project files -->
        <!-- start row button -->
        <div class="row">
        <div class="col-md-12 text-center"><br/><br/>
        <button type="submit" class="btn btn-primary btn-block font-weight-bold file-upload-button">SUBMIT</button>
        </div>
        </div>

        <!-- end row button -->

        </form>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
<!-- end add project file form -->


<script>
$('.file-upload').on( 'change', function() {
   myfile= $( this ).val();
   var ext = myfile.split('.').pop().toLowerCase();
   if(ext=="pdf" || ext=="docx" || ext=="doc" || ext=="xlsx" || ext=="xls")
   {
     $('.file-upload-button').prop("disabled",false);
     $('.file-upload-feedback').addClass('d-none');
     $('.file-upload').removeClass('is-invalid').addClass('is-valid');

   } else
   {
       $('.file-upload-button').prop("disabled",true);
       $('.file-upload-feedback').removeClass('d-none');
       $('.file-upload').removeClass('is-valid').addClass('is-invalid');
   }
});

</script>
