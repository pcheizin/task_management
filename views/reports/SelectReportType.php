<?php
session_start();
include("../../controllers/setup/connect.php");
if($_SERVER['REQUEST_METHOD'] == "POST")
{
  if(!isset($_SESSION['email']))
  {
    exit("<a href='#' class='login-link'>Please Log in to continue</a>");
  }
  if(isset($_SESSION['email']))
  {
    ?>

    <nav aria-label="breadcrumb">
          <ol class="breadcrumb">
            <li class="breadcrumb-item active" aria-current="page">Tasks Follow Up Report</li>
          </ol>
    </nav>
    <div class="row">
        <div class="col-md-12">
        <ul class="ml-4 report-menu nav nav-pills nav-sidebar flex-column nav-compact nav-child-indent" data-widget="treeview" role="menu" data-accordion="false">
           <li class="nav-item has-treeview border-top">
            <a href="#" class="nav-link text-info">

              <p>
                 1. All Task List
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="#" class="nav-link text-primary" onclick="ReportType('departmental');">
                  <p>(a) Per Department</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="#" class="nav-link text-primary" onclick="ReportType('listing');">
                  <p>(b) Per listing(Private or Public)</p>
                </a>
              </li>

              <li class="nav-item">
                <a href="#" class="nav-link text-primary" onclick="ReportType('task_status');">
                  <p>(c) Per Task Status</p>
                </a>
              </li>

              <li class="nav-item">
                <a href="#" class="nav-link text-primary" onclick="ReportType('task_category');">
                  <p>(d) Per Task Category</p>
                </a>
              </li>

              <li class="nav-item">
                <a href="#" class="nav-link text-primary" onclick="ReportType('task_timeline');">
                  <p>(e) Given Timeline</p>
                </a>
              </li>

            </ul>
          </li>



        </ul>
        </div>
    </div>

    <?php
  }
  else
  {
    echo "unauthorised";
  }
}
else
{
  echo "form not submitted";
}


 ?>
