<?php

session_start();
include("../../controllers/setup/connect.php");

if($_SERVER['REQUEST_METHOD'] == 'POST')
{

 if (!isset($_SESSION['email']))
 {
    exit("unauthenticated");
 }

 $product_start_date = mysqli_real_escape_string($dbc,strip_tags($_POST['product_start_date']));
$product_end_date = mysqli_real_escape_string($dbc,strip_tags($_POST['product_end_date']));

  $sql = mysqli_query($dbc,
                          "SELECT * FROM pm_activities WHERE start_date BETWEEN '".$product_start_date."' AND '".$product_end_date."' &&
                          end_date BETWEEN '".$product_start_date."' AND '".$product_end_date."'ORDER BY id DESC"
                          );



/*  }*/
  if($sql)
  {
    $total_rows = mysqli_num_rows($sql);
    if($total_rows > 0)
    {

    ?>
    <div class="card">

     <!-- /.card-header -->
     <div class="card-body">
        <div class="table-responsive">
          <table class="table table-striped table-bordered table-hover" id="tasks-department-table" style="width:100%">
            <thead>
              <tr>
                <td>#</td>
                 <td>Task Category</td>
                <td>Task Name</td>
                   <td>Department</td>

                <td>Start</td>
                <td>End Date</td>
                <td>Days Due</td>
                <td>Assigned Users</td>
                <td>Task Status</td>
                <td>Comments</td>

              </tr>
            </thead>
            <?php
           // $sql_tasks = mysqli_query($dbc,"SELECT * FROM pm_activities");
             $no = 1;
             while($row_tasks = mysqli_fetch_array($sql))
             {
               $milestone = mysqli_fetch_array(mysqli_query($dbc,"SELECT milestone_name FROM pm_milestones WHERE id='".$row_tasks['milestone_id']."'"));
               ?>
               <tr style="cursor: pointer;">
                 <td> <?php echo $no++;?>.</td>

                   <td><?php echo $milestone['milestone_name'];?></td>
                 <td>
                   <?php echo $row_tasks['activity_name'];?>
                 </td>

                 <td>
                   <?php echo $row_tasks['department'];?>
                 </td>

                 <td><?php echo $row_tasks['start_date'];?></td>
                 <td><?php echo $row_tasks['end_date'];?></td>
                 <td>
                   <?php
                   $todays_date = date('d-M-y');
                   $date1 = new DateTime($row_tasks['end_date']); //inclusive
                   $date2 = new DateTime($todays_date); //exclusive
                   $diff = $date2->diff($date1);
                   echo $diff->format("%a");

                    ?>
                 </td>
                 <td>

                     <?php
                     $sql_resources = mysqli_query($dbc,"SELECT * FROM pm_resources WHERE activity_id='".$row_tasks['task_id']."'");
                     while($resources = mysqli_fetch_array($sql_resources))
                     {
                       ?>
                         <small class="border-bottom">
                           <?php echo $resources['resource_name'];?>
                           <a href="#" class="btn btn-link float-right"
                               onclick="DeleteResource('<?php echo $resources['resource_id'];?>','<?php echo $resources['resource_name'];?>','<?php echo $project['project_id'];?>','<?php echo $resources['activity_id'];?>');"
                               title="Remove <?php echo $resources['resource_name'];?> from <?php echo $resources['activity_id'];?>">
                              <i class="far fa-user-times text-danger"></i>
                           </a><br/>
                         </small><br/>
                       <?php
                     }
                      ?>


                 </td>


                 <?php
                 //most recent updated task
                     $sql_status = mysqli_fetch_array(mysqli_query($dbc,"SELECT * FROM pm_activity_updates WHERE task_id='".$row_tasks['task_id']."'
                                                                             ORDER BY id DESC LIMIT 1 "));
                     if($sql_status['color_code'] == "one")
                     {
                       $text_color = "text-white";
                     }
                     else
                     {
                       $text_color = "text-dark";
                     }
                  ?>
                 <td class="<?php echo $sql_status['color_code'];?>" width="40px;">
                   <small class="<?php echo $text_color ;?>"><?php echo $sql_status['status'];?></small>

                 </td>
                 <td><?php echo $sql_status['comments'];?></td>



               </tr>
               <?php
             }
             ?>
          </table>
        </div>
     </div>
     <!-- /.card-body -->
     <div class="card-footer">

     </div>
     <!-- card-footer -->
   </div>
   <!-- /.card -->
     <?php
   } // end num row
   else  //no rows
   {
     ?>
     <div class="alert alert-danger alert-dismissible">
       <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
       <strong>No Records!<br/></strong> Sorry, no Tasks in the Selected Department.
     </div>
     <?php
   }
  }
  else
  {
    exit(mysqli_error($dbc));
  }

}
else
{
  exit("NO data");
  ?>

 <?php
}
