<?php
session_start();
require_once('../../../controllers/setup/connect.php');
ob_start();
 ?>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title></title>
  <link href="https://fonts.googleapis.com/css?family=Crimson+Text&display=swap" rel="stylesheet">
  <link rel="stylesheet" href="../../../assets/css/pdf.css?v=2" media="all">

  <link rel="stylesheet" href="../../../assets/fonts/fontawesome-pro-5.12.0/css/all.min.css">
  <!-- Ionicons 2.0.1-->
  <link rel="stylesheet" href="../../../assets/css/ionicons.min.css">
  <!-- Tempusdominus Bbootstrap 4 -->
  <link rel="stylesheet" href="../../../assets/plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css">
  <!-- iCheck -->
  <link rel="stylesheet" href="../../../assets/plugins/icheck-bootstrap/icheck-bootstrap.min.css">
  <!-- JQVMap -->
  <link rel="stylesheet" href="../../../assets/plugins/jqvmap/jqvmap.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="../../../assets/css/adminlte.min.css">
  <!--Jquery UI -->
  <link href="../../../assets/css/jquery-ui.css" rel="Stylesheet"type="text/css"/>


  <style>
  /* START RATING COLORS  */
  .one{
      background-color: #006400;
  }
  .two{
      background-color: #00FF00;
  }
  .three{
      background-color: #FFFF00;
  }
  .four{
      background-color: #FFC200;
  }
  .five{
      background-color: #FF0000;
  }
  .project-milestones-table th {
    border: 1px solid black;
    border-collapse: collapse;
    table-layout:fixed;
    word-wrap:break-word;
    font-size: 20px;
    padding: 0px;
  }

  .project-milestones-table td {
    border-collapse: collapse;
    table-layout:fixed;
    word-wrap:break-word;
    font-size: 18px;
    padding: 0px;
  }

  </style>

</head>

<body>
  <?php
  if($_SERVER['REQUEST_METHOD'] == "POST")
  {
    $project = mysqli_fetch_array(mysqli_query($dbc,"SELECT * FROM pm_projects WHERE project_id='".$_POST['project_id']."'"));
    $project_name = $project['project_name'];
    ?>
    <!-- start insert a page break -->
        <center>
            <img src="../../../assets/img/cmapicture.jpg">
        </center>
         <h2 style="text-align:center;">
           Project Milestone Report <br/><br/>
           <span class="mt-2"><small class="text-muted">Project:</small> <?php echo $project_name;?></span> <br/>
           as at <br/>
          <?php echo date('d-M-Y') ;?>
        </h2>

     <!-- /.box-header -->
    <!-- start insert a page break -->
     <div style="page-break-after:always;"></div>
    <!-- end insert a page break -->

    <div class="row">
      <div class="col-lg-12">

      </div>
    </div>
    <div class="row">
    <div class="col-lg-12 col-xs-12">
         <table class="table-bordered project-milestones-table" style="width:100%">
           <thead>
             <tr class="font-weight-bold">
               <td>#</td>
               <td>Milestone</td>
               <td>Task Name</td>
               <td>Update</td>
               <td>Status</td>
             </tr>
           </thead>
           <?php
           $sql_tasks = mysqli_query($dbc,"SELECT * FROM pm_activities
                                                WHERE milestone_id
                                                IN
                                                (SELECT id FROM pm_milestones WHERE project_id='".$project['project_id']."')
                  ");
            $no = 1;
            while($row_tasks = mysqli_fetch_array($sql_tasks))
            {
              $milestone = mysqli_fetch_array(mysqli_query($dbc,"SELECT milestone_name FROM pm_milestones WHERE id='".$row_tasks['milestone_id']."'"));
              ?>
              <tr>
                <td width="4%;" > <?php echo $no++;?>.</td>
                <td width="20%;" class="p-3"><?php echo $milestone['milestone_name'];?></td>
                <td width="10%;" class="p-3"><?php echo $row_tasks['activity_name'];?></td>

                <?php
                //most recent updated task
                    $sql_status = mysqli_fetch_array(mysqli_query($dbc,"SELECT * FROM pm_activity_updates WHERE task_id='".$row_tasks['task_id']."'
                                                                            ORDER BY id DESC LIMIT 1 "));
                    if($sql_status['color_code'] == "one")
                    {
                      $text_color = "text-white";
                    }
                    else
                    {
                      $text_color = "text-dark";
                    }
                 ?>
                 <td width="20%"><?php echo $sql_status['comments'];?></td>
                <td class="<?php echo $sql_status['color_code'];?>" width="20%;">
                  <span class="<?php echo $text_color ;?>"><?php echo $sql_status['status'];?></span>
                </td>

              </tr>
              <?php
            }
            ?>
         </table>
    </div>
    </div>



    <div style="page-break-after:always;"></div>
    <!-- end risk table -->

    <!-- jQuery -->
    <script src="../../../assets/plugins/jquery/jquery.min.js"></script>
    <!-- jQuery UI 1.11.4 -->
    <script src="../../../assets/plugins/jquery-ui/jquery-ui.min.js"></script>
    <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
    <script>
      $.widget.bridge('uibutton', $.ui.button)
    </script>
    <!-- Bootstrap 4 -->
    <script src="../../../assets/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
    <!-- ChartJS -->
    <script src="../../../assets/plugins/chart.js/Chart.min.js"></script>
    <!-- Sparkline -->
    <script src="../../../assets/plugins/sparklines/sparkline.js"></script>
    <!-- JQVMap -->
    <script src="../../../assets/plugins/jqvmap/jquery.vmap.min.js"></script>
    <script src="../../../assets/plugins/jqvmap/maps/jquery.vmap.usa.js"></script>
    <!-- jQuery Knob Chart -->
    <script src="../../../assets/plugins/jquery-knob/jquery.knob.min.js"></script>
    <!-- daterangepicker -->
    <script src="../../../assets/plugins/moment/moment.min.js"></script>
    <!-- Tempusdominus Bootstrap 4 -->
    <script src="../../../assets/plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js"></script>

    <!--highcharts -->
    <script src="../../../assets/js/highcharts.js"></script>
    <script src="../../../assets/js/exporting.js"></script>
    <script src="../../../assets/js/offline-exporting.js"></script>


</body>
</html>
<?php
$date = date('Y-m-d-H-i-s') ;
$filename = "Project-Milestone-Report".$date;
$html = ob_get_contents();
ob_end_clean();
file_put_contents("{$filename}.html", $html);


//convert HTML to PDF
shell_exec("wkhtmltopdf -O landscape  --footer-html pdffooter.html -q {$filename}.html {$filename}.pdf");
if(file_exists("{$filename}.pdf")){
  header("Content-type:application/pdf");
  header("Content-Disposition:attachment;filename={$filename}.pdf");
  echo file_get_contents("{$filename}.pdf");
  //echo "{$filename}.pdf";
}else{
  exit;
}
}
?>
