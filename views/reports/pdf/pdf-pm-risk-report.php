<?php
session_start();
require_once('../../../controllers/setup/connect.php');
ob_start();
 ?>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title></title>
  <link href="https://fonts.googleapis.com/css?family=Crimson+Text&display=swap" rel="stylesheet">
  <link rel="stylesheet" href="../../../assets/css/pdf.css?v=2" media="all">

  <link rel="stylesheet" href="../../../assets/fonts/fontawesome-pro-5.12.0/css/all.min.css">
  <style>
  thead { display: table-header-group }
  tfoot { display: table-row-group }
  tr { page-break-inside: avoid }
  </style>


</head>

<body>
  <?php

  $report_type = mysqli_real_escape_string($dbc,strip_tags($_POST['project-risks-issues-lessons-report']));
  if($_SERVER['REQUEST_METHOD'] == "POST")
  {
    ?>
    <!-- start insert a page break -->
        <center>
            <img src="../../../assets/img/cmapicture.jpg">
        </center>
         <h1 style="text-align:center;">
           Project Risk Report <br/>
           For active projects <br/>
           as at <br/>
          <?php echo date('d-M-Y') ;?>
         </h1>

     <!-- /.box-header -->
      <div style="page-break-after:always;"></div>

    <!-- start static content -->
    <?php
      include("static-content-report.php");
     ?>
    <!-- end static content -->

    <!-- start risk heatmap -->
    <!-- start insert a page break -->
     <div style="page-break-after:always;"></div>
    <!-- end insert a page break -->

  <?php
  $sql_heatmap_row = mysqli_num_rows(mysqli_query($dbc,"SELECT * FROM pm_risks_updates WHERE changed='no' && risk_id IN
                                              (SELECT risk_id FROM pm_risks WHERE status='open')"));
  if($sql_heatmap_row > 0)
  {
   ?>
    <!-- start risk analysis -->
    <div class="box">
    <h4 class="box-title">RISK ANALYSIS<br/>Overview</h4>
    <div class="box-body table-responsive no-padding">
      <div class="col-md-8 heatmap-opportunity-chart" style="width:500px; float:left;">
        <table class="heatmap-table" style="font-size:20px;margin:5px;">
           <tr>
             <td rowspan="5" class="impact_rotate">Impact</td>
             <td>Catastrophic <br/><small class="text-primary">5</small></td>
             <td class="medium" style="background-color: #FFFF00;"  title="OVERALL SCORE: 5">
               <?php
                  $sql = mysqli_query($dbc,"SELECT * FROM pm_risks_updates WHERE impact_score='5' && likelihood_score='1' && changed='no' && risk_id IN
                                                              (SELECT risk_id FROM pm_risks WHERE status='open')");
                   if(mysqli_num_rows($sql) > 0)
                   {
                     while ($risk_position = mysqli_fetch_array($sql)) {
                      echo $risk_position['risk_id']."<br>";
                     }
                   }

                ?>
                <br/>
             </td>
             <td class="high" style="background-color: #FFC200;" title="OVERALL SCORE: 10">
               <?php
                  $sql = mysqli_query($dbc,"SELECT * FROM pm_risks_updates WHERE impact_score='5' && likelihood_score='2' && changed='no' && risk_id IN
                                                              (SELECT risk_id FROM pm_risks WHERE status='open')");
                   if(mysqli_num_rows($sql) > 0)
                   {
                     while ($risk_position = mysqli_fetch_array($sql)) {
                      echo $risk_position['risk_id']."<br>";
                     }
                   }

                ?>
             </td>
             <td class="high" style="background-color: #FFC200;" title="OVERALL SCORE: 15">
               <?php
                  $sql = mysqli_query($dbc,"SELECT * FROM pm_risks_updates WHERE impact_score='5' && likelihood_score='3' && changed='no' && risk_id IN
                                                              (SELECT risk_id FROM pm_risks WHERE status='open')");
                   if(mysqli_num_rows($sql) > 0)
                   {
                     while ($risk_position = mysqli_fetch_array($sql)) {
                      echo $risk_position['risk_id']."<br>";
                     }
                   }

                ?>
             </td>
             <td class="very_high" style="background-color: #FF0000;" title="OVERALL SCORE: 20">
               <?php
                  $sql = mysqli_query($dbc,"SELECT * FROM pm_risks_updates WHERE impact_score='5' && likelihood_score='4' && changed='no' && risk_id IN
                                                              (SELECT risk_id FROM pm_risks WHERE status='open')");
                   if(mysqli_num_rows($sql) > 0)
                   {
                     while ($risk_position = mysqli_fetch_array($sql)) {
                      echo $risk_position['risk_id']."<br>";
                     }
                   }

                ?>
             </td>
             <td class="very_high" style="background-color: #FF0000;" title="OVERALL SCORE: 25">
               <?php
                  $sql = mysqli_query($dbc,"SELECT * FROM pm_risks_updates WHERE impact_score='5' && likelihood_score='5' && changed='no' && risk_id IN
                                                              (SELECT risk_id FROM pm_risks WHERE status='open')");
                   if(mysqli_num_rows($sql) > 0)
                   {
                     while ($risk_position = mysqli_fetch_array($sql)) {
                      echo $risk_position['risk_id']."<br>";
                     }
                   }

                ?>
             </td>
           </tr>
           <tr>
             <td>Major <br/><small class="text-primary">4</small></td>
                 <td class="low" style="background-color: #00FF00;" title="OVERALL SCORE: 4">
                   <?php
                      $sql = mysqli_query($dbc,"SELECT * FROM pm_risks_updates WHERE impact_score='4' && likelihood_score='1' && changed='no' && risk_id IN
                                                                  (SELECT risk_id FROM pm_risks WHERE status='open')");
                       if(mysqli_num_rows($sql) > 0)
                       {
                         while ($risk_position = mysqli_fetch_array($sql)) {
                          echo $risk_position['risk_id']."<br>";
                         }
                       }

                    ?>
                 </td>
                 <td class="medium" style="background-color: #FFFF00;" title="OVERALL SCORE: 8">
                   <?php
                      $sql = mysqli_query($dbc,"SELECT * FROM pm_risks_updates WHERE impact_score='4' && likelihood_score='2' && changed='no' && risk_id IN
                                                                  (SELECT risk_id FROM pm_risks WHERE status='open')");
                       if(mysqli_num_rows($sql) > 0)
                       {
                         while ($risk_position = mysqli_fetch_array($sql)) {
                          echo $risk_position['risk_id']."<br>";
                         }
                       }

                    ?>
                 </td>
                 <td class="high" style="background-color: #FFC200;" title="OVERALL SCORE: 12">
                   <?php
                      $sql = mysqli_query($dbc,"SELECT * FROM pm_risks_updates WHERE impact_score='4' && likelihood_score='3' && changed='no' && risk_id IN
                                                                  (SELECT risk_id FROM pm_risks WHERE status='open')");
                       if(mysqli_num_rows($sql) > 0)
                       {
                         while ($risk_position = mysqli_fetch_array($sql)) {
                          echo $risk_position['risk_id']."<br>";
                         }
                       }

                    ?>
                 </td>
                 <td class="high" style="background-color: #FFC200;" title="OVERALL SCORE: 16">
                   <?php
                      $sql = mysqli_query($dbc,"SELECT * FROM pm_risks_updates WHERE impact_score='4' && likelihood_score='4' && changed='no' && risk_id IN
                                                                  (SELECT risk_id FROM pm_risks WHERE status='open')");
                       if(mysqli_num_rows($sql) > 0)
                       {
                         while ($risk_position = mysqli_fetch_array($sql)) {
                          echo $risk_position['risk_id']."<br>";
                         }
                       }

                    ?>
                 </td>
                 <td class="very_high" style="background-color: #FF0000;" title="OVERALL SCORE: 20">
                   <?php
                      $sql = mysqli_query($dbc,"SELECT * FROM pm_risks_updates WHERE impact_score='4' && likelihood_score='5' && changed='no' && risk_id IN
                                                                  (SELECT risk_id FROM pm_risks WHERE status='open')");
                       if(mysqli_num_rows($sql) > 0)
                       {
                         while ($risk_position = mysqli_fetch_array($sql)) {
                          echo $risk_position['risk_id']."<br>";
                         }
                       }

                    ?>
                 </td>

           </tr>
           <tr>
             <td>Moderate <br/><small class="text-primary">3</small></td>
             <td class="low" style="background-color: #00FF00;" title="OVERALL SCORE: 3">
               <?php
                  $sql = mysqli_query($dbc,"SELECT * FROM pm_risks_updates WHERE impact_score='3' && likelihood_score='1' && changed='no' && risk_id IN
                                                              (SELECT risk_id FROM pm_risks WHERE status='open')");
                   if(mysqli_num_rows($sql) > 0)
                   {
                     while ($risk_position = mysqli_fetch_array($sql)) {
                      echo $risk_position['risk_id']."<br>";
                     }
                   }

                ?>
             </td>
             <td class="medium" style="background-color: #FFFF00;" title="OVERALL SCORE: 6">
               <?php
                  $sql = mysqli_query($dbc,"SELECT * FROM pm_risks_updates WHERE impact_score='3' && likelihood_score='2' && changed='no' && risk_id IN
                                                              (SELECT risk_id FROM pm_risks WHERE status='open')");
                   if(mysqli_num_rows($sql) > 0)
                   {
                     while ($risk_position = mysqli_fetch_array($sql)) {
                      echo $risk_position['risk_id']."<br>";
                     }
                   }

                ?>
             </td>
             <td class="medium" style="background-color: #FFFF00;" title="OVERALL SCORE: 9">
               <?php
                  $sql = mysqli_query($dbc,"SELECT * FROM pm_risks_updates WHERE impact_score='3' && likelihood_score='3' && changed='no' && risk_id IN
                                                              (SELECT risk_id FROM pm_risks WHERE status='open')");
                   if(mysqli_num_rows($sql) > 0)
                   {
                     while ($risk_position = mysqli_fetch_array($sql)) {
                      echo $risk_position['risk_id']."<br>";
                     }
                   }

                ?>
             </td>
             <td class="high" style="background-color: #FFC200;" title="OVERALL SCORE: 12">
               <?php
                  $sql = mysqli_query($dbc,"SELECT * FROM pm_risks_updates WHERE impact_score='3' && likelihood_score='4' && changed='no' && risk_id IN
                                                              (SELECT risk_id FROM pm_risks WHERE status='open')");
                   if(mysqli_num_rows($sql) > 0)
                   {
                     while ($risk_position = mysqli_fetch_array($sql)) {
                      echo $risk_position['risk_id']."<br>";
                     }
                   }

                ?>
             </td>
             <td class="high" style="background-color: #FFC200;" title="OVERALL SCORE: 15">
               <?php
                  $sql = mysqli_query($dbc,"SELECT * FROM pm_risks_updates WHERE impact_score='3' && likelihood_score='5' && changed='no' && risk_id IN
                                                              (SELECT risk_id FROM pm_risks WHERE status='open')");
                   if(mysqli_num_rows($sql) > 0)
                   {
                     while ($risk_position = mysqli_fetch_array($sql)) {
                      echo $risk_position['risk_id']."<br>";
                     }
                   }

                ?>
             </td>
           </tr>
           <tr>
             <td>Minor <br/><small class="text-primary">2</small></td>
             <td class="very_low" style="background-color: #006400;" title="OVERALL SCORE: 2">
               <?php
                  $sql = mysqli_query($dbc,"SELECT * FROM pm_risks_updates WHERE impact_score='2' && likelihood_score='1' && changed='no' && risk_id IN
                                                              (SELECT risk_id FROM pm_risks WHERE status='open')");
                   if(mysqli_num_rows($sql) > 0)
                   {
                     while ($risk_position = mysqli_fetch_array($sql)) {
                      echo $risk_position['risk_id']."<br>";
                     }
                   }

                ?>
             </td>
             <td class="low" style="background-color: #00FF00;" title="OVERALL SCORE: 4">
               <?php
                  $sql = mysqli_query($dbc,"SELECT * FROM pm_risks_updates WHERE impact_score='2' && likelihood_score='2' && changed='no' && risk_id IN
                                                              (SELECT risk_id FROM pm_risks WHERE status='open')");
                   if(mysqli_num_rows($sql) > 0)
                   {
                     while ($risk_position = mysqli_fetch_array($sql)) {
                      echo $risk_position['risk_id']."<br>";
                     }
                   }

                ?>
             </td>
             <td class="medium" style="background-color: #FFFF00;" title="OVERALL SCORE: 6">
               <?php
                  $sql = mysqli_query($dbc,"SELECT * FROM pm_risks_updates WHERE impact_score='2' && likelihood_score='3' && changed='no' && risk_id IN
                                                              (SELECT risk_id FROM pm_risks WHERE status='open')");
                   if(mysqli_num_rows($sql) > 0)
                   {
                     while ($risk_position = mysqli_fetch_array($sql)) {
                      echo $risk_position['risk_id']."<br>";
                     }
                   }

                ?>
             </td>
             <td class="medium" style="background-color: #FFFF00;" title="OVERALL SCORE: 8">
               <?php
                  $sql = mysqli_query($dbc,"SELECT * FROM pm_risks_updates WHERE impact_score='2' && likelihood_score='4' && changed='no' && risk_id IN
                                                              (SELECT risk_id FROM pm_risks WHERE status='open')");
                   if(mysqli_num_rows($sql) > 0)
                   {
                     while ($risk_position = mysqli_fetch_array($sql)) {
                      echo $risk_position['risk_id']."<br>";
                     }
                   }

                ?>
             </td>
             <td  class="high" style="background-color: #FFC200;" title="OVERALL SCORE: 10">
               <?php
                  $sql = mysqli_query($dbc,"SELECT * FROM pm_risks_updates WHERE impact_score='2' && likelihood_score='5' && changed='no' && risk_id IN
                                                              (SELECT risk_id FROM pm_risks WHERE status='open')");
                   if(mysqli_num_rows($sql) > 0)
                   {
                     while ($risk_position = mysqli_fetch_array($sql)) {
                      echo $risk_position['risk_id']."<br>";
                     }
                   }

                ?>
             </td>
           </tr>
           <tr>
             <td>Insignificant <br/><small class="text-primary">1</small></td>
             <td class="very_low" style="background-color: #006400;" title="OVERALL SCORE: 1">
               <?php
                  $sql = mysqli_query($dbc,"SELECT * FROM pm_risks_updates WHERE impact_score='1' && likelihood_score='1' && changed='no' && risk_id IN
                                                              (SELECT risk_id FROM pm_risks WHERE status='open')");
                   if(mysqli_num_rows($sql) > 0)
                   {
                     while ($risk_position = mysqli_fetch_array($sql)) {
                      echo $risk_position['risk_id']."<br>";
                     }
                   }

                ?>
             </td>
             <td class="very_low" style="background-color: #006400;" title="OVERALL SCORE: 2">
               <?php
                  $sql = mysqli_query($dbc,"SELECT * FROM pm_risks_updates WHERE impact_score='1' && likelihood_score='2' && changed='no' && risk_id IN
                                                              (SELECT risk_id FROM pm_risks WHERE status='open')");
                   if(mysqli_num_rows($sql) > 0)
                   {
                     while ($risk_position = mysqli_fetch_array($sql)) {
                      echo $risk_position['risk_id']."<br>";
                     }
                   }

                ?>
             </td>
             <td class="low" style="background-color: #00FF00;" title="OVERALL SCORE: 3">
               <?php
                  $sql = mysqli_query($dbc,"SELECT * FROM pm_risks_updates WHERE impact_score='1' && likelihood_score='3' && changed='no' && risk_id IN
                                                              (SELECT risk_id FROM pm_risks WHERE status='open')");
                   if(mysqli_num_rows($sql) > 0)
                   {
                     while ($risk_position = mysqli_fetch_array($sql)) {
                      echo $risk_position['risk_id']."<br>";
                     }
                   }

                ?>
             </td>
             <td class="low" style="background-color: #00FF00;" title="OVERALL SCORE: 4">
               <?php
                  $sql = mysqli_query($dbc,"SELECT * FROM pm_risks_updates WHERE impact_score='1' && likelihood_score='4' && changed='no' && risk_id IN
                                                              (SELECT risk_id FROM pm_risks WHERE status='open')");
                   if(mysqli_num_rows($sql) > 0)
                   {
                     while ($risk_position = mysqli_fetch_array($sql)) {
                      echo $risk_position['risk_id']."<br>";
                     }
                   }

                ?>
             </td>
             <td class="medium" style="background-color: #FFFF00;" title="OVERALL SCORE: 5">
               <?php
                  $sql = mysqli_query($dbc,"SELECT * FROM pm_risks_updates WHERE impact_score='1' && likelihood_score='5' && changed='no' && risk_id IN
                                                              (SELECT risk_id FROM pm_risks WHERE status='open')");
                   if(mysqli_num_rows($sql) > 0)
                   {
                     while ($risk_position = mysqli_fetch_array($sql)) {
                      echo $risk_position['risk_id']."<br>";
                     }
                   }

                ?>
             </td>
           </tr>
           <tr>
             <td colspan="2" rowspan="2"><i class="fa fa-times fa-lg"></i></td>
             <td>Rare <br/><small class="text-primary">1</small></td>
             <td>Unlikely <br/><small class="text-primary">2</small></td>
             <td>Likely <br/><small class="text-primary">3</small></td>
             <td>Highly Likely <br/><small class="text-primary">4</small></td>
             <td>Almost Certain <br/><small class="text-primary">5</small></td>
           </td>
         </tr>
         <tr>
           <td colspan="5">Likelihood</td>
         </tr>
       </table>
       </div>

    <!-- end of first div-col-8 -->
    <div class="col-md-4 heatmap-ratings-table"  style="overflow:wrap;width:730px; float:right;">
    <?php
     $sql_query = mysqli_query($dbc,"SELECT * FROM pm_risks_updates WHERE changed='no' && risk_id IN
                                                 (SELECT risk_id FROM pm_risks WHERE status='open') ORDER BY overall_score DESC");

     $number = 1;
     if($total_rows = mysqli_num_rows($sql_query) > 0)
     {?>
       <table>
           <thead>
             <tr>
               <td style="padding-left: 5px;padding-top:5px;padding-bottom:5px;">#</td>
               <td style="padding-left: 5px;padding-top:5px;padding-bottom:5px;">Risk</td>
               <td style="padding-left: 5px;padding-top:5px;padding-bottom:5px;">Score</td>
               <td style="padding-left: 5px;padding-top:5px;padding-bottom:5px;">Ref No</td>
             </tr>
           </thead>
           <?php
           while($row = mysqli_fetch_array($sql_query))
           {
            ?>
             <tr style="">
               <td width="30px;" style="padding-left: 5px;"><?php echo $number++ ;?></td>
               <td style="padding-left: 5px;padding-top:5px;padding-bottom:5px;"><?php echo $row['risk_description'];?></td>
               <td style="padding-left: 5px;padding-top:5px;padding-bottom:5px;"><?php echo $row['overall_score'];?></td>
               <td style="padding-left: 5px;padding-top:5px;padding-bottom:5px;"><?php echo $row['risk_id'];?></td>
             </tr>
           <?php
           }
            ?>
         </table>

         <?php
    }
    else {
     ?>
     <table class="table table-bordered">
       <thead>
         <tr>
           <td class="text-danger"><i class="fa fa-info-circle"></i> No Records Found</td>

         </tr>

       </thead>
       <tr>
         <td class="text-danger">Sorry, no records available. </td>

       </tr>

     </table>

     <?php
    }
          ?>
    </div>
    </div>
    <?php
  }
     ?>
    <!-- end risk analysis -->
    <!-- start insert a page break -->
    <div style="page-break-after:always;"></div>
    <!-- end risk heatmap -->


    <!--start risk table -->
    <div class="row">
      <div class="col-12">
        <table class="table detailed-analysis-table project-risks export-table" width="100%" style="page-break-after: always;">
          <thead>
            <tr>
              <th scope="col">#</th>
              <th scope="col">Risk</th>
              <th scope="col">Impact</th>
              <th scope="col">Project</th>
              <th scope="col">Project Phase Affected</th>
              <th scope="col">Proximity</th>
              <th scope="col">Rating</th>
              <th scope="col">Prior Rating</th>
              <th scope="col">Risk Owner</th>
              <th scope="col">Mitigations Strategy</th>
              <th scope="col">Actions Applied</th>
            </tr>
          </thead>
          <tbody>
            <?php
                 $no = 1;
                   $project_risks = mysqli_query($dbc,"SELECT * FROM pm_risks_updates WHERE changed='no' && risk_id IN
                                                               (SELECT risk_id FROM pm_risks WHERE status='open') ORDER BY overall_score DESC");

                 while($project_risks_row = mysqli_fetch_array($project_risks))
                 {
                   ?>
                   <tr>
                     <th scope="row"><?php echo $no++ ;?></th>
                     <td><?php echo $project_risks_row['risk_description'];?></td>
                     <td><?php echo $project_risks_row['impact'];?></td>
                     <td>
                       <?php
                            $project_name_title = mysqli_fetch_array(mysqli_query($dbc,"SELECT project_name FROM pm_projects
                                                                WHERE project_id='".$project_risks_row['project_id']."'"));
                        echo $project_name_title['project_name'];?>

                     </td>
                     <td><?php echo $project_risks_row['phase'];?></td>
                     <td><?php echo $project_risks_row['proximity'];?></td>
                     <td>
                       <?php
                        $overall_score = $project_risks_row['overall_score'];
                        $likelihood_score = $project_risks_row['likelihood_score'];
                        $impact_score = $project_risks_row['impact_score'];
                        if($overall_score < 26 && $overall_score > 19 )
                        {
                          ?>
                          <span class="badge five"><?php echo $overall_score;?> <br/>(L<?php echo $likelihood_score;?>) * (I <?php echo $impact_score;?>)</span>
                          <?php
                        }
                        if($overall_score < 17 && $overall_score > 9 )
                        {
                          ?>
                          <span class="badge four"><?php echo $overall_score;?> <br/>(L<?php echo $likelihood_score;?>) * (I <?php echo $impact_score;?>)</span>
                          <?php
                        }
                        if($overall_score < 10 && $overall_score > 5 )
                        {
                          ?>
                          <span class="badge three"><?php echo $overall_score;?> <br/>(L<?php echo $likelihood_score;?>) * (I <?php echo $impact_score;?>)</span>
                          <?php
                        }
                        if($overall_score < 5 && $overall_score > 2 )
                        {
                          ?>
                          <span class="badge two"><?php echo $overall_score;?> <br/> (L<?php echo $likelihood_score;?>) * (I <?php echo $impact_score;?>)</span>
                          <?php
                        }
                        if($overall_score < 3 && $overall_score > 0 )
                        {
                          ?>
                          <span class="badge one"><?php echo $overall_score;?> <br/>(L<?php echo $likelihood_score;?>) * (I <?php echo $impact_score;?>)</span>
                          <?php
                        }
                        ?>
                      </td>
                      <td>
                          <?php
                              $prior_rating = mysqli_fetch_array(mysqli_query($dbc,"SELECT likelihood_score,impact_score,overall_score FROM
                                                                          pm_risks_updates WHERE risk_id='".$project_risks_row['risk_id']."'
                                                                          ORDER BY id DESC LIMIT 1,1"));

                              if($overall_score > $prior_rating['overall_score'])
                              {
                                if($prior_rating['overall_score'] == NULL)
                                {
                                  ?>
                                  <span class="text-danger">
                                          <i class="fa fa-caret-up"></i>
                                        0 <br/>
                                        (L<?php echo $prior_rating['likelihood_score'];?>) * (I <?php echo $prior_rating['impact_score'];?>)

                                  </span>
                                  <?php
                                }
                                else
                                {
                                  ?>
                                  <span class="text-danger">
                                          <i class="fa fa-caret-up"></i>
                                        <?php echo $prior_rating['overall_score'];?> <br/>
                                        (L<?php echo $prior_rating['likelihood_score'];?>) * (I <?php echo $prior_rating['impact_score'];?>)

                                  </span>
                                  <?php
                                }
                                ?>
                                <?php
                              }
                              else if ($overall_score == $prior_rating['overall_score'])
                              {
                                ?>
                                <span class="text-warning">
                                        <i class="fa fa-arrows-h"></i>
                                      <?php echo $prior_rating['overall_score'];?> <br/>
                                      (L<?php echo $prior_rating['likelihood_score'];?>) * (I <?php echo $prior_rating['impact_score'];?>)

                                </span>
                                <?php
                              }
                              else if ($overall_score < $prior_rating['overall_score'])
                              {
                                ?>
                                <span class="text-success">
                                        <i class="fa fa-caret-down"></i>
                                      <?php echo $prior_rating['overall_score'];?> <br/>
                                      (L<?php echo $prior_rating['likelihood_score'];?>) * (I <?php echo $prior_rating['impact_score'];?>)

                                </span>
                                <?php
                              }

                           ?>
                      </td>
                     <td><?php echo $project_risks_row['risk_owner'];?></td>
                     <td><?php echo $project_risks_row['mitigation_strategy'];?></td>
                     <td> <div style="page-break-inside: avoid;"><?php echo $project_risks_row['actions_applied'];?></div></td>
                   </tr>
                   <?php
                 }
             ?>
          </tbody>

        </table>
      </div>
    </div>

    <!-- end risk table -->


</body>
</html>
<?php
$date = date('Y-m-d-H-i-s') ;
$filename = "Projects-Risk-Report".$date;
$html = ob_get_contents();
ob_end_clean();
file_put_contents("{$filename}.html", $html);


//convert HTML to PDF
shell_exec("wkhtmltopdf -O landscape  --footer-html pdffooter.html -q {$filename}.html {$filename}.pdf");
if(file_exists("{$filename}.pdf")){
  header("Content-type:application/pdf");
  header("Content-Disposition:attachment;filename={$filename}.pdf");
  echo file_get_contents("{$filename}.pdf");
  //echo "{$filename}.pdf";
}else{
  exit;
}
}
?>
