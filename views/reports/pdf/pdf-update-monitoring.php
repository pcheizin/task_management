<?php
session_start();
require_once('../../../controllers/setup/connect.php');
ob_start();
 ?>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title></title>
  <link href="https://fonts.googleapis.com/css?family=Crimson+Text&display=swap" rel="stylesheet">
  <link rel="stylesheet" href="../../../assets/css/pdf.css?v=2" media="all">

  <link rel="stylesheet" href="../../../assets/fonts/fontawesome-pro-5.12.0/css/all.min.css">
  <!-- Ionicons 2.0.1-->
  <link rel="stylesheet" href="../../../assets/css/ionicons.min.css">
  <!-- Tempusdominus Bbootstrap 4 -->
  <link rel="stylesheet" href="../../../assets/plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css">
  <!-- iCheck -->
  <link rel="stylesheet" href="../../../assets/plugins/icheck-bootstrap/icheck-bootstrap.min.css">
  <!-- JQVMap -->
  <link rel="stylesheet" href="../../../assets/plugins/jqvmap/jqvmap.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="../../../assets/css/adminlte.min.css">
  <!--Jquery UI -->
  <link href="../../../assets/css/jquery-ui.css" rel="Stylesheet"type="text/css"/>


  <style>

table{
    border-collapse:collapse;
}
tr td{
    page-break-inside: avoid;
    white-space: nowrap;
}
  </style>

</head>

<body>
  <?php
  $year = $current_quarter_and_year['period'];
  $quarter = $current_quarter_and_year['quarter'];

 $sql_query = mysqli_query($dbc,"SELECT * FROM departments
                                 WHERE department_status='current' ORDER BY department_id ASC"
                             );

$total_departments = mysqli_num_rows($sql_query);
$number = 1;
$completed = "";
 if($total_rows = mysqli_num_rows($sql_query) > 0)
 {?>
 <table class="table table-bordered summarized-monitoring-table">
   <thead>
     <tr>
       <td colspan="8">Performance & Risk Update Summary for <?php echo $quarter?>, <?php echo $year;?></td>
     </tr>
     <tr>

       <td>NO</td>
       <td>Department</td>
       <td>Total Risks</td>
       <td>Updated Risks</td>
       <td>Approved Risks</td>
       <td>Total Activities</td>
       <td>Updated Activities</td>
       <td>Overall Status</td>
     </tr>
   </thead>
   <?php
   while($row = mysqli_fetch_array($sql_query))
   {

     //total risks
     $total_no = mysqli_num_rows(mysqli_query($dbc,"SELECT risk_reference FROM risk_management
                                                      WHERE changed='no' && department_code='".$row['department_id']."'
                                                      && risk_status='open'
                                                      "));
                                                      //updated risks for current quarter
                                                      $number_of_updated_risks =mysqli_num_rows(mysqli_query($dbc,"SELECT reference_no FROM update_risk_status
                                                      WHERE period_from='".$current_quarter_and_year['period']."'
                                                      && quarter = '".$current_quarter_and_year['quarter']."'
                                                      && dep_code='".$row['department_id']."'
                                                      && reference_no IN (SELECT risk_reference FROM risk_management
                                                      WHERE department_code='".$row['department_id']."'
                                                      && changed='no' && risk_status='open'
                                                      )
                                                      && changed='no'"));
                                                      //updated and approved risks for current quarter
                                                      $number_of_updated_pending_approved_risks =mysqli_num_rows(mysqli_query($dbc,"SELECT reference_no FROM update_risk_status
                                                      WHERE period_from='".$current_quarter_and_year['period']."'
                                                      && quarter = '".$current_quarter_and_year['quarter']."'
                                                      && dep_code='".$row['department_id']."'
                                                      && status='pending approval'
                                                      && reference_no IN (SELECT risk_reference FROM risk_management
                                                      WHERE department_code='".$row['department_id']."'
                                                      && changed='no' && risk_status='open'
                                                      )
                                                      && changed='no'"));
                                                      $number_of_updated_approved_risks =mysqli_num_rows(mysqli_query($dbc,"SELECT reference_no FROM update_risk_status
                                                      WHERE period_from='".$current_quarter_and_year['period']."'
                                                      && quarter = '".$current_quarter_and_year['quarter']."'
                                                      && dep_code='".$row['department_id']."'
                                                      && status!='pending approval'
                                                      && reference_no IN (SELECT risk_reference FROM risk_management
                                                      WHERE department_code='".$row['department_id']."'
                                                      && changed='no' && risk_status='open'
                                                      )
                                                      && changed='no'"));
                                                      $pending_update = $total_no - $number_of_updated_risks;
                                                      if($total_no == $number_of_updated_risks)
                                                      {
                                                      $symbol = "check text-success";
                                                      }
                                                      else
                                                      {
                                                      $symbol = "times text-danger";
                                                      }

                                                      if($total_no == $number_of_updated_approved_risks)
                                                      {
                                                        $symbol_1 = "check text-success";
                                                      }
                                                      else
                                                      {
                                                        $symbol_1 = "times text-danger";
                                                      }


                                                      //ACTIVITIES
                                                      //START counting
                                                      $total_activities = mysqli_num_rows(mysqli_query($dbc,"SELECT activity_id FROM perfomance_management
                                                                                                       WHERE department_id='".$row['department_id']."'
                                                                                                       && activity_status='open'
                                                                                                       &&
                                                                                                       activity_id IN
                                                                                                       (SELECT activity_id FROM activity_strategic_outcomes
                                                                                                       WHERE changed='no' &&
                                                                                                       year_id='".$current_quarter_and_year['period']."')
                                                                                                       "));

                                                  //updated risks for current quarter
                                                   $number_of_updated_activites =mysqli_num_rows(mysqli_query($dbc,"SELECT activity_id FROM performance_update
                                                                                      WHERE year_id='".$current_quarter_and_year['period']."'
                                                                                      && quarter_id = '".$current_quarter_and_year['quarter']."'
                                                                                      && activity_id IN (SELECT activity_id FROM perfomance_management
                                                                                                          WHERE changed='no' && department_id='".$row['department_id']."'
                                                                                                          && activity_status='open')
                                                                                      && changed='no'"));

                                                    $activities_pending_update = $total_activities - $number_of_updated_activites;

                                                    if($total_activities == $number_of_updated_activites)
                                                    {
                                                      $symbol_2 = "check text-success";
                                                    }
                                                    else
                                                    {
                                                      $symbol_2 = "times text-danger";
                                                    }

     ?>
   <tr style="cursor: pointer;">
     <td><?php echo $number++;?></td>
     <td><?php echo $row['department_name'];?></td>
     <td><?php echo $total_no; ?> </td>
     <td>(<?php echo $number_of_updated_risks;?> / <?php echo $total_no;?>) <i class="fas fa-<?php echo $symbol;?> float-right"></i> </td>
     <td> (<?php echo $number_of_updated_approved_risks;?> / <?php echo $total_no;?>) <i class="fas fa-<?php echo $symbol_1;?> float-right"></i> </td>
     <td><?php echo $total_activities;?></td>
     <td>(<?php echo $number_of_updated_activites;?> / <?php echo $total_activities;?>) <i class="fas fa-<?php echo $symbol_2;?> float-right"></i> </td>
     <td>
        <?php
            if(($total_no == $number_of_updated_risks)  && ($total_no == $number_of_updated_approved_risks) && ($total_activities == $number_of_updated_activites))
            {
              $completed++;
              ?><i class="fas fa-check text-success float-right"></i> <?php
            }
            else
            {
              ?><i class="fas fa-times text-danger float-right"></i> <?php
            }
         ?>
     </td>
   </tr>
   <?php
   }
   ?>
   <tfoot>
     <tr>
     <td colspan="2">SUMMARY</td>
      <td>Total Risks:
        <?php                //total_authority_risks =
              $total_authority_risks = mysqli_num_rows(mysqli_query($dbc,"SELECT risk_reference FROM risk_management
                                                                        WHERE changed='no' &&
                                                                        (department_code!='SPU' && department_code!='SP' && department_code!='RF' && department_code !='CES OFFICE' && department_code !='IG')
                                                                        && risk_status='open'
                                                                        "));


                                                                        ?>
      <strong><?php echo $total_authority_risks;?></strong>

    </td>
    <td>
      <?php
      $total_authority_updated_risks  =mysqli_num_rows(mysqli_query($dbc,"SELECT reference_no FROM update_risk_status
      WHERE period_from='".$current_quarter_and_year['period']."'
      && quarter = '".$current_quarter_and_year['quarter']."'
      && reference_no IN (SELECT risk_reference FROM risk_management
      WHERE changed='no' && risk_status='open' &&
      (department_code!='SPU' && department_code!='SP' && department_code!='RF' && department_code !='CES OFFICE' && department_code !='IG')
      )
      && changed='no'"));

      $total_authority_pending_update_risks = $total_authority_risks - $total_authority_updated_risks;

      ?>
      (<strong><?php echo $total_authority_updated_risks ;?>/<?php echo $total_authority_risks ;?></strong>) risks have been updated.<br/>
      <strong><?php echo $total_authority_pending_update_risks ;?></strong> risks are pending update
    </td>

    <td>
      <?php
      $authority_number_of_updated_approved_risks =mysqli_num_rows(mysqli_query($dbc,"SELECT reference_no FROM update_risk_status
      WHERE period_from='".$current_quarter_and_year['period']."'
      && quarter = '".$current_quarter_and_year['quarter']."'
      && status!='pending approval'
      && reference_no IN (SELECT risk_reference FROM risk_management
      WHERE changed='no' && risk_status='open' &&
      (department_code!='SPU' && department_code!='SP' && department_code!='RF' && department_code !='CES OFFICE' && department_code !='IG')
      )
      && changed='no'"));

      $authority_number_of_updated_pending_approval_risks =mysqli_num_rows(mysqli_query($dbc,"SELECT reference_no FROM update_risk_status
      WHERE period_from='".$current_quarter_and_year['period']."'
      && quarter = '".$current_quarter_and_year['quarter']."'
      && status='pending approval'
      && reference_no IN (SELECT risk_reference FROM risk_management
      WHERE changed='no' && risk_status='open' &&
      (department_code!='SPU' && department_code!='SP' && department_code!='RF' && department_code !='CES OFFICE' && department_code !='IG')
      )
      && changed='no'"));

      ?>

       (<strong><?php echo $authority_number_of_updated_approved_risks ;?>/<?php echo $total_authority_updated_risks ;?></strong>) updated risks
      have been approved. <br/>
      <strong><?php echo $authority_number_of_updated_pending_approval_risks ;?></strong> are pending approval
    </td>

    <td>
      <?php
      $total_authority_activities = mysqli_num_rows(mysqli_query($dbc,"SELECT activity_id FROM perfomance_management
                                                       WHERE activity_status='open'
                                                       && department_id IN
                                                       (SELECT department_id FROM departments
                                                                                       WHERE department_status='current')
                                                       &&
                                                       activity_id IN
                                                       (SELECT activity_id FROM activity_strategic_outcomes
                                                       WHERE changed='no' &&
                                                       year_id='".$current_quarter_and_year['period']."')
                                                       "));

  //updated risks for current quarter
   $number_of_updated_authority_activities =mysqli_num_rows(mysqli_query($dbc,"SELECT activity_id FROM performance_update
                                      WHERE year_id='".$current_quarter_and_year['period']."'
                                      && quarter_id = '".$current_quarter_and_year['quarter']."'
                                      && activity_id IN (SELECT activity_id FROM perfomance_management
                                                          WHERE changed='no'
                                                          && activity_status='open'
                                                        && department_id!='IG')
                                      && changed='no'"));

    $authority_activities_pending_update = $total_authority_activities - $number_of_updated_authority_activities;
    ?>
    Total Activities: <strong><?php echo $total_authority_activities;?>
    </td>

    <td>
        (<strong><?php echo $number_of_updated_authority_activities ;?>/<?php echo $total_authority_activities ;?></strong>) activities
        have been updated. <br/>
        <strong><?php echo $authority_activities_pending_update ;?></strong> are yet to be updated.
    </td>

    <td>
          (<strong><?php echo $completed;?>/<?php echo $total_departments;?></strong>) departments finished updating
    </td>



  </tr>
   </tfoot>
 </table>
    <!-- tfoot -->
 <?php
 }
 ?>
    <!-- end risk table -->

    <!-- jQuery -->
    <script src="../../../assets/plugins/jquery/jquery.min.js"></script>
    <!-- jQuery UI 1.11.4 -->
    <script src="../../../assets/plugins/jquery-ui/jquery-ui.min.js"></script>
    <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
    <script>
      $.widget.bridge('uibutton', $.ui.button)
    </script>
    <!-- Bootstrap 4 -->
    <script src="../../../assets/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
    <!-- ChartJS -->
    <script src="../../../assets/plugins/chart.js/Chart.min.js"></script>
    <!-- Sparkline -->
    <script src="../../../assets/plugins/sparklines/sparkline.js"></script>
    <!-- JQVMap -->
    <script src="../../../assets/plugins/jqvmap/jquery.vmap.min.js"></script>
    <script src="../../../assets/plugins/jqvmap/maps/jquery.vmap.usa.js"></script>
    <!-- jQuery Knob Chart -->
    <script src="../../../assets/plugins/jquery-knob/jquery.knob.min.js"></script>
    <!-- daterangepicker -->
    <script src="../../../assets/plugins/moment/moment.min.js"></script>
    <!-- Tempusdominus Bootstrap 4 -->
    <script src="../../../assets/plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js"></script>

    <!--highcharts -->
    <script src="../../../assets/js/highcharts.js"></script>
    <script src="../../../assets/js/exporting.js"></script>
    <script src="../../../assets/js/offline-exporting.js"></script>

</body>
</html>
<?php
$date = date('Y-m-d-H-i-s') ;
$filename = "Risk_and_Performance_Status_Update_Report_Summary".$date;
$html = ob_get_contents();
ob_end_clean();
file_put_contents("{$filename}.html", $html);


//convert HTML to PDF
shell_exec("wkhtmltopdf -O landscape  --footer-html pdffooter.html -q {$filename}.html {$filename}.pdf");
if(file_exists("{$filename}.pdf")){
  header("Content-type:application/pdf");
  header("Content-Disposition:attachment;filename={$filename}.pdf");
  echo file_get_contents("{$filename}.pdf");
  //echo "{$filename}.pdf";
}else{
  exit;
}

?>
