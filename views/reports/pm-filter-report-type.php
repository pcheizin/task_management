<?php
session_start();
include("../../controllers/setup/connect.php");

if(!$_SERVER['REQUEST_METHOD'] == "POST")
{
  exit();
}

if (!isset($_SESSION['email']))
{
     exit("<a href='#' class='login-link'>Please Log in to continue</a>");
}

//GET SELECTED REPORT TYPE
$report_type = mysqli_real_escape_string($dbc,strip_tags($_POST['report_type']));
$report_type_title = ucwords(str_replace("-"," ", $report_type));

if($report_type == 'project-portfolio-report')
{
  //start project portfolio report
    ?>
    <div class="card">
      <div class="card-header">
        <h3 class="card-title"><?php echo $report_type_title;?></h3>
        <div class="card-tools">
          <!-- Maximize Button -->
          <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
        </div>
        <!-- /.card-tools -->
      </div>
      <!-- /.card-header -->
      <div class="card-body">
        <div class="form-group row">
           <label class="col-sm-2 col-form-label">Project</label>
           <div class="col-sm-10">
             <?php
                 //fetch all projects irregardles of status
                 $projects = mysqli_query($dbc,"SELECT * FROM pm_projects");
              ?>
             <select class="custom-select my-1 mr-sm-2 project-<?php echo $report_type;?>" onchange="ProjectReportsData('<?php echo $report_type;?>');">
               <option selected disabled>Choose Project</option>
               <?php
                   while($project_row = mysqli_fetch_array($projects))
                   {
                     ?>
                     <option value="<?php echo $project_row['project_id'] ;?>"><?php echo $project_row['project_name'] ;?></option>
                     <?php
                   }
                ?>
             </select>
           </div>
         </div>
         <!-- start card project content -->

         <!-- end card project content -->
      </div>
      <!-- /.card-body -->
    </div>
    <!-- /.card -->
    <div class="card project-status-data d-none">

    </div>


    <?php
  //end project portfolio report
}
else if ($report_type == 'project-status-report')
{
  //start project status report
?>
<div class="card">
  <div class="card-header">
    <h3 class="card-title"><?php echo $report_type_title;?></h3>
    <div class="card-tools">
      <!-- Maximize Button -->
      <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
    </div>
    <!-- /.card-tools -->
  </div>
  <!-- /.card-header -->
  <div class="card-body">
    <div class="form-group row">
       <label class="col-sm-2 col-form-label">Project</label>
       <div class="col-sm-10">
         <?php
             //fetch all active projects
             $projects = mysqli_query($dbc,"SELECT * FROM pm_projects
                                              WHERE project_id IN
                                              (SELECT project_id FROM pm_projects_update_status WHERE project_status='Active' && changed='no' ORDER BY id DESC)");
          ?>
         <select class="custom-select my-1 mr-sm-2 project-<?php echo $report_type;?>" onchange="ProjectReportsData('<?php echo $report_type;?>');">
           <option selected disabled>Choose Project</option>
           <option value="all">All Active Projects</option>
           <?php
               while($project_row = mysqli_fetch_array($projects))
               {
                 ?>
                 <option value="<?php echo $project_row['project_id'] ;?>"><?php echo $project_row['project_name'] ;?></option>
                 <?php
               }
            ?>
         </select>
       </div>
     </div>
  </div>
  <!-- /.card-body -->
</div>
<!-- /.card -->

<!-- start card project content -->
<div class="card project-status-data d-none">

</div>
<!-- /.card -->
<!-- end card project content -->

<?php
  //end project status report
}
else if ($report_type == 'project-task-report')
{
  //start project task report
  ?>
  <div class="card">
    <div class="card-header">
      <h3 class="card-title">Milestone Report</h3>
      <div class="card-tools">
        <!-- Maximize Button -->
        <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
      </div>
      <!-- /.card-tools -->
    </div>
    <!-- /.card-header -->
    <div class="card-body">
      <div class="form-group row">
         <label class="col-sm-2 col-form-label">Project</label>
         <div class="col-sm-10">
           <?php
               //fetch all projects irregardles of status
               $projects = mysqli_query($dbc,"SELECT * FROM pm_projects");
            ?>
           <select class="custom-select my-1 mr-sm-2 project-<?php echo $report_type;?>" onchange="ProjectReportsData('<?php echo $report_type;?>');">
             <option selected disabled>Choose Project</option>
             <?php
                 while($project_row = mysqli_fetch_array($projects))
                 {
                   ?>
                   <option value="<?php echo $project_row['project_id'] ;?>"><?php echo $project_row['project_name'] ;?></option>
                   <?php
                 }
              ?>
           </select>
         </div>
       </div>
       <!-- start card project content -->

       <!-- end card project content -->
    </div>
    <!-- /.card-body -->
  </div>
  <!-- /.card -->
  <div class="card project-status-data d-none">

  </div>

  <?php

  //end project task report
}
else if ($report_type == 'project-resource-report')
{
  //start project resource report
  ?>
  <div class="card">
    <div class="card-header">
      <h3 class="card-title"><?php echo $report_type_title;?></h3>
      <div class="card-tools">
        <!-- Maximize Button -->
        <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
      </div>
      <!-- /.card-tools -->
    </div>
    <!-- /.card-header -->
    <div class="card-body">
      <div class="form-group row">
         <label class="col-sm-2 col-form-label">Resources</label>
         <div class="col-sm-10">
           <?php
               //fetch all active projects
               $resources = mysqli_query($dbc,"SELECT resource_id,resource_name FROM pm_resources GROUP BY resource_name");
            ?>
           <select class="custom-select my-1 mr-sm-2 select2 project-<?php echo $report_type;?>" onchange="ProjectReportsData('<?php echo $report_type;?>');">
             <option selected disabled>Choose Resource</option>
             <?php
                 while($resource_row = mysqli_fetch_array($resources))
                 {
                   ?>
                   <option value="<?php echo $resource_row['resource_name'] ;?>"><?php echo $resource_row['resource_name'] ;?></option>
                   <?php
                 }
              ?>
           </select>
         </div>
       </div>
    </div>
    <!-- /.card-body -->
  </div>
  <!-- /.card -->
  <!-- start card project content -->
  <div class="card project-status-data d-none">

  </div>
  <!-- /.card -->

  <?php

  //end project resource repirt
}
else if ($report_type == 'project-timesheet-report')
{
  //start project timesheet report
  ?>
  <div class="card">
    <div class="card-header">
      <h3 class="card-title"><?php echo $report_type_title;?></h3>
      <div class="card-tools">
        <!-- Maximize Button -->
        <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
      </div>
      <!-- /.card-tools -->
    </div>
    <!-- /.card-header -->
    <div class="card-body">
      <div class="form-group row">
         <label class="col-sm-2 col-form-label">Resources</label>
         <div class="col-sm-10">
           <?php
               //fetch all active projects
               $resources = mysqli_query($dbc,"SELECT resource_id,resource_name FROM pm_resources GROUP BY resource_name");
            ?>
           <select class="custom-select my-1 mr-sm-2 select2 project-<?php echo $report_type;?>" onchange="ProjectReportsData('<?php echo $report_type;?>');">
             <option selected disabled>Choose Resource</option>
             <?php
                 while($resource_row = mysqli_fetch_array($resources))
                 {
                   ?>
                   <option value="<?php echo $resource_row['resource_name'] ;?>"><?php echo $resource_row['resource_name'] ;?></option>
                   <?php
                 }
              ?>
           </select>
         </div>
       </div>
    </div>
    <!-- /.card-body -->
  </div>
  <!-- /.card -->
  <!-- start card project content -->
  <div class="card project-status-data d-none">

  </div>
  <!-- /.card -->
  <?php
  //end project timesheet report
}

else if ($report_type == 'project-risks-issues-lessons-report')
{
  //start project risk,lessons & issues report
?>
<div class="card">
  <div class="card-header">
    <h3 class="card-title">Project Risks, Issues & Lessons</h3>
    <div class="card-tools">
      <!-- Maximize Button -->
      <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
    </div>
    <!-- /.card-tools -->
  </div>
  <!-- /.card-header -->
  <div class="card-body">
    <div class="form-group row">
       <label class="col-sm-2 col-form-label">Project</label>
       <div class="col-sm-10">
         <?php
             //fetch all active projects
             $projects = mysqli_query($dbc,"SELECT * FROM pm_projects
                                              WHERE project_id IN
                                              (SELECT project_id FROM pm_projects_update_status WHERE project_status='Active' && changed='no' ORDER BY id DESC)");
          ?>
         <select class="custom-select my-1 mr-sm-2 project-<?php echo $report_type;?>" onchange="ProjectReportsData('<?php echo $report_type;?>');">
           <option selected disabled>Choose Project</option>
           <option value="all">All Active Projects</option>
           <?php
               while($project_row = mysqli_fetch_array($projects))
               {
                 ?>
                 <option value="<?php echo $project_row['project_id'] ;?>"><?php echo $project_row['project_name'] ;?></option>
                 <?php
               }
            ?>
         </select>
       </div>
     </div>
  </div>
  <!-- /.card-body -->
</div>
<!-- /.card -->

<!-- start card project content -->
<div class="card project-status-data d-none">

</div>
<!-- /.card -->
<!-- end card project content -->

<?php
  //end project risk,lessons & issues report
}


 ?>
