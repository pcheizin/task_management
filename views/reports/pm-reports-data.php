<?php
session_start();
include("../../controllers/setup/connect.php");

if(!$_SERVER['REQUEST_METHOD'] == "POST")
{
exit();
}

if (!isset($_SESSION['email']))
{
     exit("<a href='#' class='login-link'>Please Log in to continue</a>");
}

//GET SELECTED REPORT TYPE
$report_type = mysqli_real_escape_string($dbc,strip_tags($_POST['report_type']));
$project = mysqli_real_escape_string($dbc,strip_tags($_POST['project']));

if($report_type == 'project-portfolio-report')
{
//start project portfolio report

//fetch selected project
$sql_projects = mysqli_query($dbc,"SELECT * FROM pm_projects WHERE project_id='".$project."'");
$project_row = mysqli_fetch_array($sql_projects);

//projects phase
$project_phase = mysqli_fetch_array(mysqli_query($dbc,"SELECT project_phase FROM pm_projects_update WHERE changed='no' && project_id='".$project."'"));

//projects status
$project_status = mysqli_fetch_array(mysqli_query($dbc,"SELECT project_status FROM pm_projects_update_status WHERE changed='no' && project_id='".$project."'"))
?>
<div class="card-header">
<div class="ribbon-wrapper ribbon-xl float-left">
<div class="ribbon bg-secondary">
<?php echo $project_phase['project_phase'];?>
</div>
</div>
<h3 class="card-title"><button type="button" class="btn btn-tool" data-card-widget="maximize"><i class="fas fa-expand"></i></button> <?php echo $project_row['project_name'];?></h3>

</div>
<div class="card-body table-responsive">
<div class="row">
<div class="col-12 col-md-12 col-lg-8 order-2 order-md-1">
<div class="row">
<div class="col-12 table-resonsive">
  <table class="table" width="100%">
      <thead class="thead-light">
        <tr>
          <th scope="col">Contract Price</th>
          <th scope="col">Due Date</th>
          <th scope="col">Status</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td>
            <?php
            $contract_price = mysqli_query($dbc,"SELECT * FROM pm_budget WHERE project_id='".$project_row['project_id']."'");
            while($row_contract_price = mysqli_fetch_array($contract_price))
            {
              ?>
                (<?php echo $row_contract_price['currency_type'];?>) <?php echo number_format($row_contract_price['amount']);?> <br/>
                <p><small class="text-muted"><?php echo ucwords($row_contract_price['budget_line']);?> Budget </small></p><br/>
              <?php
            }
         ?>
          </td>
          <td><?php echo $project_row['end_date'];?></td>
          <td><?php echo $project_status['project_status'];?></td>
        </tr>
      </tbody>
    </table>
</div>
</div>

<!-- start row for project roadmap -->
<div class="row">
<div class="col-md-12">
  <div class="card">
      <div class="card-header bg-light">
        Project Phase Roadmap

        <div class="card-tools">
          <!-- Maximize Button -->
          <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
        </div>
        <!-- /.card-tools -->

      </div>
      <div class="card-body">
        <?php
        //project phase roadmap
        $roadmap_sql = mysqli_query($dbc,"SELECT * FROM pm_projects_update WHERE project_id='".$project."' ORDER BY id ASC");
        $roadmap_data = array();
        while($row_roadmap_sql = mysqli_fetch_array($roadmap_sql))
              {
              $roadmap_data[] =  array (
                  'date' => $row_roadmap_sql['date_recorded'],
                  'content' => ucwords(str_replace("_"," ",$row_roadmap_sql['project_phase']))
                );

              }

              $json_roadmap_data =  json_encode($roadmap_data);
        ?>
        <div id="my-timeline"></div>


      </div>
  </div>
</div>
</div>

<!-- end row for project roadmap -->

<!-- start row for pie chart -->
<div class="row">
<div class="col-md-12">
  <div class="card">
      <div class="card-header bg-light">
        Task Status Summary

        <div class="card-tools">
          <!-- Maximize Button -->
          <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
        </div>
        <!-- /.card-tools -->

      </div>
      <div class="card-body">
        <?php
        //pie chart for task status
        $task_status = mysqli_query($dbc,"SELECT task_id, count(*) AS tasks,status FROM pm_activity_updates WHERE changed='no'
                                                 && project_id='".$project."'
                                                 GROUP BY status");

        while ($row_task_status = mysqli_fetch_array($task_status))
        {
          $count_tasks[] = $row_task_status['tasks'];
          $task_status_description[] = $row_task_status['status'];
        }
        $counted_tasks = json_encode($count_tasks);
        $task_status_descriptions = json_encode($task_status_description);
        ?>
        <div class="chart">
          <!-- Resource Distribution Chart Canvas -->
          <canvas id="task-status-chart" height="300" style="height: 300px; display: block; width: 577px;" class="chartjs-render-monitor" width="577"></canvas>
          <!--<div id="chart" height="250" style="height: 250px;"></div>-->
        </div>
        <!-- /.chart-responsive -->

      </div>
  </div>
</div>


</div>

<!-- end row for pie chart -->

<!-- start activity timeline -->
<div class="row">
  <div class="col-md-12">
    <div class="card">
        <div class="card-header bg-light">
          Tasks Timeline

          <div class="card-tools">
            <!-- Maximize Button -->
            <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
          </div>
          <!-- /.card-tools -->
        </div>
        <div class="card-body">
          <div id="project-portfolio-timeline-data">
          </div>
          <!--start pagination buttons -->
          <?php
             $limit = 5;
             $sql = "SELECT COUNT(id) FROM pm_activity_updates WHERE project_id='".$project."' ORDER BY id DESC";
             $rs_result = mysqli_query($dbc, $sql);
             $row = mysqli_fetch_row($rs_result);
             $total_records = $row[0];
             $total_pages = ceil($total_records / $limit);
             ?>
          <nav aria-label="...">
             <ul class='pagination text-center pagination-sm table-responsive' id="pagination">
                <?php if(!empty($total_pages)):for($i=1; $i<=$total_pages; $i++):
                   if($i == 1):?>
                <li class='active' class="page-item"  id="<?php echo $i;?>">
                   <a class="page-link" href='pm-tasks-timeline.php?page=<?php echo $i;?>' onclick="PaginateProjectTasks('<?php echo $project;?>','<?php echo $i;?>');"><?php echo $i;?></a>
                </li>
                <?php else:?>
                <li class="page-item" id="<?php echo $i;?>" onclick="PaginateProjectTasks('<?php echo $project;?>','<?php echo $i;?>');"><a class="page-link" href='pm-tasks-timeline.php?page=<?php echo $i;?>'><?php echo $i;?></a></li>
                <?php endif;?>
                <?php endfor;endif;?>
             </ul>
          </nav>
        </div>
      </div>
</div>
<!-- /.col -->
</div>
<!-- end activity timeline -->
<div class="row">
<div class="col-12 table-resonsive">
  <div class="card">
    <div class="card-header bg-light">
      Risks
      <div class="card-tools">
        <!-- Maximize Button -->
        <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
      </div>
      <!-- /.card-tools -->
    </div>
    <div class="card-body table-responsive">
      <table class="table" id="pm-projects-portfolio-risks-table" width="100%">
        <thead class="thead-light">
          <tr>
            <th scope="col">#</th>
            <th scope="col">Risk</th>
            <th scope="col">Impact</th>
            <th scope="col">Project Phase Affected</th>
            <th scope="col">Proximity</th>
            <th scope="col">Rating</th>
            <th scope="col">Risk Owner</th>
            <th scope="col">Mitigations Strategy</th>
            <th scope="col">Actions Applied</th>
          </tr>
        </thead>
        <tbody>
          <?php
               $no = 1;
               $project_risks = mysqli_query($dbc,"SELECT * FROM pm_risks_updates WHERE project_id='".$project."' && changed='no' && risk_id IN
                                                           (SELECT risk_id FROM pm_risks WHERE status='open')");
               while($project_risks_row = mysqli_fetch_array($project_risks))
               {
                 ?>
                 <tr>
                   <th scope="row"><?php echo $no++ ;?></th>
                   <td><?php echo $project_risks_row['risk_description'];?></td>
                   <td><?php echo $project_risks_row['impact'];?></td>
                   <td><?php echo $project_risks_row['phase'];?></td>
                   <td><?php echo $project_risks_row['proximity'];?></td>
                   <td>
                     <?php
                      $overall_score = $project_risks_row['overall_score'];
                      $likelihood_score = $project_risks_row['likelihood_score'];
                      $impact_score = $project_risks_row['impact_score'];
                      if($overall_score < 26 && $overall_score > 19 )
                      {
                        ?>
                        <span class="badge five"><?php echo $overall_score;?> <br/>(L<?php echo $likelihood_score;?>) * (I <?php echo $impact_score;?>)</span>
                        <?php
                      }
                      if($overall_score < 17 && $overall_score > 9 )
                      {
                        ?>
                        <span class="badge four"><?php echo $overall_score;?> <br/>(L<?php echo $likelihood_score;?>) * (I <?php echo $impact_score;?>)</span>
                        <?php
                      }
                      if($overall_score < 10 && $overall_score > 5 )
                      {
                        ?>
                        <span class="badge three"><?php echo $overall_score;?> <br/>(L<?php echo $likelihood_score;?>) * (I <?php echo $impact_score;?>)</span>
                        <?php
                      }
                      if($overall_score < 5 && $overall_score > 2 )
                      {
                        ?>
                        <span class="badge two"><?php echo $overall_score;?> <br/> (L<?php echo $likelihood_score;?>) * (I <?php echo $impact_score;?>)</span>
                        <?php
                      }
                      if($overall_score < 3 && $overall_score > 0 )
                      {
                        ?>
                        <span class="badge one"><?php echo $overall_score;?> <br/>(L<?php echo $likelihood_score;?>) * (I <?php echo $impact_score;?>)</span>
                        <?php
                      }
                      ?>
                    </td>
                   <td><?php echo $project_risks_row['risk_owner'];?></td>
                   <td><?php echo $project_risks_row['mitigation_strategy'];?></td>
                   <td><?php echo $project_risks_row['actions_applied'];?></td>
                 </tr>
                 <?php
               }
           ?>
        </tbody>
      </table>
    </div>
  </div>
</div>
</div>

<!-- start table issue logs -->
<div class="row">
<div class="col-12 table-resonsive">
  <div class="card">
    <div class="card-header bg-light">
      Open Issues
      <div class="card-tools">
        <!-- Maximize Button -->
        <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
      </div>
      <!-- /.card-tools -->
    </div>
    <div class="card-body table-responsive">
      <table class="table" id="pm-projects-portfolio-issue-logs-table" width="100%">
        <thead class="thead-light">
          <tr>
            <th scope="col">#</th>
            <th scope="col">Issue Description</th>
            <th scope="col">Issue Type</th>
            <th scope="col">Date Raised</th>
            <th scope="col">Due</th>
            <th scope="col">Raised by</th>
            <th scope="col">Issue Author</th>
            <th scope="col">Priority</th>
            <th scope="col">Severity</th>
            <th scope="col">Next Action</th>
            <th scope="col">Person Responsible</th>
          </tr>
        </thead>
        <tbody>
          <?php
               $no = 1;
               $project_issues = mysqli_query($dbc,"SELECT * FROM pm_issue_logs_updates WHERE project_id='".$project."' && changed='no' && issue_id IN
                                                           (SELECT issue_id FROM pm_issue_logs WHERE status='open')");
               while($project_issues_row = mysqli_fetch_array($project_issues))
               {
                 ?>
                 <tr>
                   <th scope="row"><?php echo $no++ ;?></th>
                   <td><?php echo $project_issues_row['issue_description'];?></td>
                   <td>
                     <?php
                     $issue_type = str_replace("_", " ", $project_issues_row['issue_type']);
                     echo $issue_type;
                     ?>
                   </td>
                   <td><?php echo $project_issues_row['date_raised'];?></td>
                   <td><?php echo $project_issues_row['due_date'];?></td>
                   <td><?php echo $project_issues_row['raised_by'];?></td>
                   <td><?php echo $project_issues_row['issue_author'];?></td>
                   <td>
                     <?php
                     if($project_issues_row['priority'] == 'High')
                     {
                       ?>
                         <i class="fas fa-arrow-up text-danger"></i> <small class="text-danger"><?php echo $project_issues_row['priority'];?></small>
                       <?php
                     }
                     else if($project_issues_row['priority'] == 'Medium')
                     {
                       ?>
                       <i class="fas fa-arrows-h text-warning"></i> <small class="text-warning"><?php echo $project_issues_row['priority'];?></small>
                       <?php
                     }
                     else if($project_issues_row['priority'] == 'Low')
                     {
                       ?>
                       <i class="fas fa-arrow-down text-success"></i> <small class="text-success"><?php echo $project_issues_row['priority'];?></small>
                       <?php
                     }
                     ?>
                   </td>
                   <td>
                     <?php
                     if($project_issues_row['severity'] == 'Blocker')
                     {
                       ?>
                         <span class="badge five"><?php echo $project_issues_row['severity'];?></span>
                       <?php
                     }
                     else if($project_issues_row['severity'] == 'Critical')
                     {
                       ?>
                         <span class="badge four"><?php echo $project_issues_row['severity'];?></span>
                       <?php
                     }
                     else if($project_issues_row['severity'] == 'Major')
                     {
                       ?>
                         <span class="badge three"><?php echo $project_issues_row['severity'];?></span>
                       <?php
                     }
                     else if($project_issues_row['severity'] == 'Minor')
                     {
                       ?>
                         <span class="badge two"><?php echo $project_issues_row['severity'];?></span>
                       <?php
                     }
                     else if($project_issues_row['severity'] == 'Trival')
                     {
                       ?>
                         <span class="badge one"><?php echo $project_issues_row['severity'];?></span>
                       <?php
                     }
                     ?>
                   </td>
                   <td><?php echo $project_issues_row['next_action'];?></td>
                   <td><?php echo $project_issues_row['person_responsible'];?></td>
                 </tr>
                 <?php
               }
           ?>
        </tbody>
      </table>
    </div>
  </div>
</div>
</div>

<!-- end table issue logs -->

<!--start table lessons learnt -->
<div class="row">
<div class="col-12 table-resonsive">
  <div class="card">
    <div class="card-header bg-light">
      Lessons Learnt
      <div class="card-tools">
        <!-- Maximize Button -->
        <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
      </div>
      <!-- /.card-tools -->
    </div>
    <div class="card-body table-responsive">
      <table class="table" id="pm-projects-portfolio-lessons-table" width="100%">
        <thead class="thead-light">
          <tr>
            <th scope="col">#</th>
            <th scope="col">Lesson Description</th>
            <th scope="col">Lesson Date</th>
            <th scope="col">Phase</th>
            <th scope="col">Lesson Type</th>
            <th scope="col">Impact to Project</th>
            <th scope="col">Related Risk</th>
            <th scope="col">Response</th>
            <th scope="col">Actions Applied</th>
          </tr>
        </thead>
        <tbody>
          <?php
               $no = 1;
               $project_lessons = mysqli_query($dbc,"SELECT * FROM pm_lessons_learnt WHERE project_id='".$project."' ORDER BY id DESC");
               while($project_lessons_row = mysqli_fetch_array($project_lessons))
               {
                 ?>
                 <tr>
                   <th scope="row"><?php echo $no++ ;?></th>
                   <td><?php echo $project_lessons_row['lesson_description'];?></td>
                   <td><?php echo $project_lessons_row['lesson_date'];?></td>
                   <td>
                     <?php
                     echo str_replace("_", " ", $project_lessons_row['phase']);
                     ?>
                   </td>
                   <td><?php echo $project_lessons_row['lesson_type'];?></td>
                   <td><?php echo $project_lessons_row['impact_to_project'];?></td>
                   <td><?php echo $project_lessons_row['related_risk'];?></td>
                   <td><?php echo $project_lessons_row['response'];?></td>
                   <td><?php echo $project_lessons_row['actions_applied'];?></td>

                 </tr>
                 <?php
               }
           ?>
        </tbody>
      </table>
    </div>
  </div>
</div>
</div>
<!-- end table lesssons learnt -->
</div>
<div class="col-12 col-md-12 col-lg-4 order-1 order-md-2">
<h3 class="text-muted">Project Description</h3>
<p class="text-muted"><?php echo $project_row['project_description'];?></p>
<br>
<div class="text-muted">
<p class="text-sm">Stakeholders
   <b class="d-block"> - <?php echo $project_row['project_owner'];?> (Project Owner)</b>
   <b class="d-block"> - <?php echo $project_row['senior_contractor'];?> (Senior Contractor)</b>
</p>
</div>
<h5 class="mt-5 text-muted">Project files</h5>
<ul class="list-unstyled">
<?php
   $sql_project_files = mysqli_query($dbc,"SELECT id,file FROM pm_project_documents WHERE project_id='".$project."'");
   while ($row_files = mysqli_fetch_array($sql_project_files))
   {
     $ext = pathinfo($row_files['file'], PATHINFO_EXTENSION);
     if($ext == "docx" || $ext == "doc")
     {
        $icon = "fa-file-word";
     }
     else if ($ext == "pdf")
     {
        $icon = "fa-file-pdf";
     }
     else if ($ext == "csv" || $ext == "xls" || $ext=="xlsx" || $ext == "xlsm")
     {
       $icon = "fa-file-excel";
     }
     ?>
     <li>
        <a  href="views/project-management/documents/<?php echo $row_files['file'] ;?>"
            target="_blank" title="Click to view <?php echo $row_files['file'] ;?>"
            class="btn-link text-secondary">
            <i class="far fa-fw <?php echo $icon;?>"></i><?php echo $row_files['file'] ;?>
        </a>
     </li>
     <?php
   }
?>
</ul>

</div>
</div>
</div>
<!-- /.card-body -->

<script>
//project roadmap
var myEvents = <?php echo $json_roadmap_data ;?>;
$('#my-timeline').roadmap(myEvents,{
eventsPerSlide: 3,// default: 6
slide: 1,
prevArrow:'<i class="fal fa-caret-left"></i>',
nextArrow:'<i class="fal fa-caret-right"></i>'
});

//end project roadmap

//START TASK STATUS CHART
var pieChartCanvas = $('#task-status-chart').get(0).getContext('2d');
var pieData        = {
labels: <?php echo $task_status_descriptions;?>,
datasets: [
{
fill: false,
data: <?php echo $counted_tasks;?>,
//backgroundColor : ['#f56954', '#00a65a', '#f39c12', '#00c0ef', '#3c8dbc', '#d2d6de'],
}
]
}
var pieOptions     = {
legend: {
display: true,
position: 'bottom',
labels: {
fontColor: '#333',
usePointStyle:true
}
},
plugins: {

colorschemes: {

scheme: 'brewer.DarkTwo8'

}

}
}
//Create pie or douhnut chart
// You can switch between pie and douhnut using the method below.
var pieChart = new Chart(pieChartCanvas, {
type: 'pie',
data: pieData,
options: pieOptions
})


//END TASK STATUS CHART
</script>

<?php
//end project portfolio report
}
else if ($report_type == 'project-status-report')
{
//start project status report
if($project == "all")
{
//fetch all active projects
$sql_projects = mysqli_query($dbc,"SELECT * FROM pm_projects
                         WHERE project_id IN
                         (SELECT project_id FROM pm_projects_update_status WHERE project_status='Active' && changed='no' ORDER BY id DESC)");

}
else
{
//fetch selected project
$sql_projects = mysqli_query($dbc,"SELECT * FROM pm_projects WHERE project_id='".$project."'");
}

?>
<div class="card-header">
<h3 class="card-title">Reports</h3>
<div class="card-tools">
<!-- Maximize Button -->
<button type="button" class="btn btn-tool" data-card-widget="maximize"><i class="fas fa-expand"></i></button>
</div>
<!-- /.card-tools -->
</div>
<!-- /.card-header -->
<div class="card-body table-responsive">
<table class="table table-striped table-hover table-bordered" id="pm-project-status-report-table" width="100%">
<thead>
<tr>
<th scope="col">#</th>
<th scope="col">Project</th>
<th scope="col">Status</th>
<th scope="col">Phase</th>
<th scope="col">Tasks</th>
<th scope="col">Contract Price</th>
<th scope="col">Milestone Payments</th>
<th scope="col">Project Owner</th>
<th scope="col">Contractors</th>
<th scope="col">Funding Agency</th>
<th scope="col">Start Date</th>
<th scope="col">End Date</th>
</tr>
</thead>
<tbody>
<?php
$no = 1;
while($project_row = mysqli_fetch_array($sql_projects))
{
?>
<tr>
<th scope="row"><?php echo $no++;?></th>
<td><?php echo $project_row['project_name'];?></td>
<td>
    <?php
        $project_status = mysqli_fetch_array(mysqli_query($dbc,"SELECT project_status FROM pm_projects_update_status WHERE project_id='".$project_row['project_id']."' && changed='no'"));
        echo $project_status['project_status'];
     ?>
</td>
<td>
    <?php
        $project_phase = mysqli_fetch_array(mysqli_query($dbc,"SELECT project_phase FROM pm_projects_update WHERE project_id='".$project_row['project_id']."' && changed='no'"));
        echo $project_phase['project_phase'];
     ?>
</td>
<td>
  <?php
      $tasks = mysqli_query($dbc,"SELECT status,comments,task_id FROM pm_activity_updates WHERE project_id='".$project_row['project_id']."' && changed='no'");
      ?>
      <ol type="i">
        <?php
        while($tasks_row = mysqli_fetch_array($tasks))
        {
          $task_name = mysqli_fetch_array(mysqli_query($dbc,"SELECT activity_name FROM pm_activities WHERE task_id='".$tasks_row['task_id']."'"));
          ?>
          <li>
            <?php echo $task_name['activity_name'];?> <br/>
            <p><small class="text-muted">Status: (<?php echo $tasks_row['status'];?>)</small></p><br/>
            <?php
                if($tasks_row['comments'] == '')
                {
                  //nothing to display
                }
                else
                {
                  ?>
                  <small class="text-muted">Comments: (<?php echo $tasks_row['comments'];?>)</small><br/>
                  <?php
                }

             ?>

          </li>
          <?php
        }
     ?>
      </ol>
</td>
<td>
  <ol type="i">
    <?php
    $contract_price = mysqli_query($dbc,"SELECT * FROM pm_budget WHERE project_id='".$project_row['project_id']."'");
    while($row_contract_price = mysqli_fetch_array($contract_price))
    {
      ?>
      <li>
        (<?php echo $row_contract_price['currency_type'];?>) <?php echo number_format($row_contract_price['amount']);?> <br/>
        <p><small class="text-muted"><?php echo ucwords($row_contract_price['budget_line']);?> Budget </small></p><br/>
      </li>
      <?php
    }
 ?>
  </ol>
</td>
<td>
  <ol type="i">
    <?php
    $paid = mysqli_query($dbc,"SELECT * FROM pm_milestone_payment1 WHERE project_id='".$project_row['project_id']."'");
    while($row_paid = mysqli_fetch_array($paid))
    {
      $check_milestone_paid = mysqli_fetch_array(mysqli_query($dbc,"SELECT * FROM pm_milestone_payment0
                                                WHERE milestone_payment_id='".$row_paid['milestone_payment_id']."'"));


      $difference = $row_paid['anticipated_cost'] - $row_paid['invoice_paid'];
      $difference = number_format($difference);
      ?>
      <li>
        (<?php echo $row_paid['currency'];?>) <?php echo number_format($row_paid['invoice_paid']);?> <small><?php echo ucwords($row_paid['budget_line']);?> Budget</small> <br/>
        <p><small class="text-muted">Invoice No: (<?php echo $row_paid['invoice_number'];?>)</small></p><br/>
      </li>
      <?php
    }
 ?>
  </ol>
</td>
<td>
    <?php echo $project_row['project_owner'];?>
</td>
<td>
    <?php echo $project_row['senior_contractor'];?>
</td>
<td>
  <?php
      $funding = mysqli_query($dbc,"SELECT * FROM pm_budget WHERE project_id='".$project_row['project_id']."' GROUP BY funding_agency");
      while($funding_agency = mysqli_fetch_array($funding))
      {
          echo $funding_agency['funding_agency'];
      }
   ?>
</td>
<td>
    <?php echo $project_row['start_date'];?>
</td>
<td>
    <?php echo $project_row['end_date'];?>
</td>

</tr>
<?php
}
?>
</tbody>
</table>
</div>
<!-- /.card-body -->
<?php
//end project status report
}
else if ($report_type == 'project-task-report')
{
//start project task report
?>
<div class="card-header">
  <?php   $project_name = mysqli_fetch_array(mysqli_query($dbc,"SELECT project_name FROM pm_projects WHERE project_id='".$project."'")); ?>
<h3 class="card-title">Milestone Report for <?php echo $project_name['project_name'];?></h3>
<div class="card-tools">
<!-- Maximize Button -->
<button type="button" class="btn btn-tool" data-card-widget="maximize"><i class="fas fa-expand"></i></button>
</div>
<!-- /.card-tools -->
</div>
<!-- /.card-header -->
<div class="card-body table-responsive">
  <div class="card d-none">
    <div class="card-header">
      <h3 class="card-title">Filter Dates</h3>
      <div class="card-tools">
        <!-- collapse Button -->
        <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
      </div>
      <!-- /.card-tools -->
    </div>
    <!-- /.card-header -->
    <div class="card-body" style="display:none;">
      <form id="task-range-form">
        <div class="form-group row">
           <label class="col-sm-2 col-form-label">Range:</label>
           <div class="col-sm-10 form-inline">
             <div class="row">
               <div class="col-sm-4 form-group">
                 <label>From</label>
                 <div class="input-group mb-3">
                   <div class="input-group-prepend">
                     <span class="input-group-text"><i class="fal fa-calendar-day"></i></span>
                   </div>
                   <input type="text" class="form-control pull-right range_start_date" name="range_start_date" required>
                 </div>
               </div>
               <div class="col-sm-4 form-group">
                 <label>To</label>
                 <div class="input-group mb-3">
                   <div class="input-group-prepend">
                     <span class="input-group-text"><i class="fal fa-calendar-day"></i></span>
                   </div>
                   <input type="text" class="form-control pull-right range_end_date" name="range_end_date" required>
                 </div>
               </div>
               <div class="col-sm-4 form-group">
                 <button type="submit" class="btn btn-primary mb-0 mt-1"><i class="fal fa-filter"></i> Filter</button>
                 <button type="reset" class="btn btn-link mb-0 mt-1"> reset</button>
               </div>
             </div>
           </div>
         </div>
      </form>
    </div>
    <!-- /.card-body -->
  </div>

  <form action="views/reports/pdf/pdf-pm-project-milestone-report.php" method="post" id="pdf-project-milestone-report-form">
    <input type="hidden" name="project_id" value="<?php echo $project;?>">
    <button type="submit" class="btn btn-success"><i class="fa fa-file-pdf"></i> Generate PDF</button>
  </form>

  <div class="row">
    <div class="col-lg-12">
      <div class="card mb-3">
        <div class="card-header">Overview
          <div class="card-tools">
            <!-- collapse Button -->
            <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
          </div>
          <!-- /.card-tools -->
        </div>
        <div class="card-body">
          <?php
              $sql_milestones = mysqli_query($dbc,"SELECT * FROM pm_milestones WHERE project_id='".$project."'");
              $total_milestones = mysqli_num_rows($sql_milestones);

              $sql_tasks = mysqli_query($dbc,"SELECT * FROM pm_activities WHERE project_id='".$project."'");
              $total_tasks = mysqli_num_rows($sql_tasks);

              $sql_resources = mysqli_query($dbc,"SELECT * FROM pm_resources WHERE project_id='".$project."' GROUP BY resource_name");
              $total_resources = mysqli_num_rows($sql_resources);


           ?>
          <div class="row">
            <div class="col-md-4">
              <div class="info-box">
                   <span class="info-box-icon bg-info elevation-1"><i class="fas fa-pennant"></i></span>
                   <div class="info-box-content">
                      <span class="info-box-text">Milestones</span>
                      <span class="info-box-number"><?php echo $total_milestones;?></span>
                   </div>
                   <!-- /.info-box-content -->
                </div>
              </div>
              <div class="col-md-4">
                <div class="info-box">
                     <span class="info-box-icon bg-info elevation-1"><i class="fas fa-tasks"></i></span>
                     <div class="info-box-content">
                        <span class="info-box-text">Tasks</span>
                        <span class="info-box-number"><?php echo $total_tasks;?></span>
                     </div>
                     <!-- /.info-box-content -->
                  </div>
                </div>
                <div class="col-md-4">
                  <div class="info-box">
                       <span class="info-box-icon bg-info elevation-1"><i class="fas fa-users"></i></span>
                       <div class="info-box-content">
                          <span class="info-box-text">Resources</span>
                          <span class="info-box-number"><?php echo $total_resources;?></span>
                       </div>
                       <!-- /.info-box-content -->
                    </div>
                  </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="row">
    <div class="col-lg-12">
      <div class="card mb-3">
        <div class="card-header">Milestones
          <div class="card-tools">
            <!-- collapse Button -->
            <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
          </div>
          <!-- /.card-tools -->
        </div>
        <div class="card-body">
          <div class="row">
              <div class="col-lg-12 table-responsive">
                <table class="table">
                  <thead class="thead-light table-bordered">
                    <tr>
                      <th scope="col">#</th>
                      <th scope="col">Milestone</th>
                      <th scope="col">Due</th>
                      <th scope="col">Tasks</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php
                    $no = 1;
                      while($milestone_row = mysqli_fetch_array($sql_milestones))
                      {
                        ?>
                        <tr>
                          <th scope="row"><?php echo $no++;?></th>
                          <td><?php echo $milestone_row['milestone_name'];?></td>
                          <td>

                              <?php
                              $todays_date = date('d-M-Y');

                              $date1 = new DateTime($milestone_row['end_date']); //inclusive
                             $date2 = new DateTime($todays_date); //exclusive
                             $diff = $date2->diff($date1);

                             $days = $diff->format("%R");
                             if($days == "-")
                             {
                               ?>
                                <span class="badge badge-danger"><?php echo $milestone_row['end_date'];?> <br/><br/> (<?php echo $diff->format("%R%a") . " Days Ago";?>)</span>
                               <?php
                             }
                             else
                             {
                               ?>
                               <span class="badge badge-info"><?php echo $milestone_row['end_date'];?> <br/><br/> (<?php echo $diff->format("%R%a") . " More Days";?>)</span>
                               <?php
                             }

                               ?>
                          </td>
                          <td>
                            <table class="table">
                                <thead  class="thead-light">
                                    <tr>
                                    <th scope="col">#</th>
                                    <th scope="col">Task</th>
                                    <th scope="col">Due</th>
                                    <th scope="col">Status</th>
                                    <th scope="col">Resources</th>
                                    </tr>
                                </thead>
                                <tbody>
                                  <?php
                                  $count = 1;
                                    $sql_milestone_tasks = mysqli_query($dbc,"SELECT * FROM pm_activities WHERE milestone_id='".$milestone_row['id']."'");
                                    while($tasks = mysqli_fetch_array($sql_milestone_tasks))
                                    {
                                      ?>
                                      <tr>
                                        <th scope="row"><?php echo $count++;?></th>
                                        <td><?php echo $tasks['activity_name'];?></td>
                                        <td><?php echo $tasks['end_date'];?></td>
                                        <td>
                                            <?php
                                                $status = mysqli_fetch_array(mysqli_query($dbc,"SELECT status,color_code FROM pm_activity_updates WHERE task_id='".$tasks['task_id']."' && changed='no'"));
                                             ?>
                                             <span class="badge <?php echo $status['color_code'];?>"><?php echo $status['status'];?></span>
                                        </td>
                                        <td>
                                            <?php
                                              $count_resource = 1;
                                                $resources = mysqli_query($dbc,"SELECT resource_name FROM pm_resources WHERE activity_id='".$tasks['task_id']."' GROUP BY resource_name");
                                                while($resource_name = mysqli_fetch_array($resources))
                                                {
                                                  echo $count_resource++ . ". ". $resource_name['resource_name'] . "<br/>";
                                                }


                                             ?>
                                        </td>
                                      </td>
                                      </tr>
                                      <?php
                                    }
                                   ?>
                                </tbody>
                            </table>
                        </td>
                      </tr>
                        <?php
                      }

                     ?>
                </tbody>
              </table>

              </div>
          </div>
        </div>
      </div>
    </div>
  </div>

</div>
<?php

//end project task report
}
else if ($report_type == 'project-resource-report')
{
//start project resource report
  ?>
  <!-- start row for project resource -->
  <div class="card-header bg-light">
     Resource Report for <?php echo $project ;?>
     <div class="card-tools">
        <!-- Maximize Button -->
        <button type="button" class="btn btn-tool" data-card-widget="maximize"><i class="fas fa-expand"></i></button>
     </div>
     <!-- /.card-tools -->
  </div>
  <div class="card-body table-responsive">
    <div class="row">
      <div class="col-12 col-sm-6 col-md-6">
        <div class="card bg-light">
          <div class="card-header text-muted border-bottom-0">
            Resource Task Summary
          </div>
          <div class="card-body pt-0">
            <div class="row">
              <div class="col-12">
                <?php
                $not_started = mysqli_fetch_array(mysqli_query($dbc,"SELECT task_id, count(*) AS tasks,status FROM pm_activity_updates WHERE changed='no' &&
                                                         task_id IN
                                                         (SELECT activity_id FROM pm_resources WHERE resource_name='".$project."')
                                                         && status='Not Started'"));
                $not_started = $not_started['tasks'];

                $behind = mysqli_fetch_array(mysqli_query($dbc,"SELECT task_id, count(*) AS tasks,status FROM pm_activity_updates WHERE changed='no' &&
                                           task_id IN
                                           (SELECT activity_id FROM pm_resources WHERE resource_name='".$project."')
                                           && status='In Progress Behind Schedule' GROUP BY status"));
                $behind = $behind['tasks'];

               $within = mysqli_fetch_array(mysqli_query($dbc,"SELECT task_id, count(*) AS tasks,status FROM pm_activity_updates WHERE changed='no' &&
                                                                       task_id IN
                                                                      (SELECT activity_id FROM pm_resources WHERE resource_name='".$project."')
                                                                      && status='In Progress Within Schedule'"));
                $within = $within['tasks'];

                $completed = mysqli_fetch_array(mysqli_query($dbc,"SELECT task_id, count(*) AS tasks,status FROM pm_activity_updates WHERE changed='no' &&
                                                           task_id IN
                                                          (SELECT activity_id FROM pm_resources WHERE resource_name='".$project."')
                                                          && status='Completed'"));
                $completed = $completed['tasks'];

                $continous = mysqli_fetch_array(mysqli_query($dbc,"SELECT task_id, count(*) AS tasks,status FROM pm_activity_updates WHERE changed='no' &&
                                                      task_id IN
                                                (SELECT activity_id FROM pm_resources WHERE resource_name='".$project."')
                                                && status='Continous' "));
                $continous = $continous['tasks'];

                $repriotized = mysqli_fetch_array(mysqli_query($dbc,"SELECT task_id, count(*) AS tasks,status FROM pm_activity_updates WHERE changed='no' &&
                                                                            task_id IN
                                                                           (SELECT activity_id FROM pm_resources WHERE resource_name='".$project."')
                                                                           && status='Repriotized'"));
                $repriotized = $repriotized['tasks'];

                $total_tasks_per_resource = mysqli_num_rows(mysqli_query($dbc,"SELECT id FROM pm_activities WHERE task_id IN
                                                                       (SELECT activity_id FROM pm_resources WHERE resource_name='".$project."')"));
                 ?>

                 <div class="chart">
                   <!-- Resource Distribution Chart Canvas -->
                   <canvas id="task-status-report-chart" height="300" style="height: 300px; display: block; width: 577px;" class="chartjs-render-monitor" width="577"></canvas>
                   <!--<div id="chart" height="250" style="height: 250px;"></div>-->
                 </div>
                 <!-- /.chart-responsive -->

              </div>
            </div>
          </div>
          <div class="card-footer">
            <div class="text-right">
            </div>
          </div>
        </div>
      </div>


      <div class="col-12 col-sm-6 col-md-6">
        <div class="card bg-light">
          <div class="card-header text-muted border-bottom-0">
            Projects Assigned
          </div>
          <div class="card-body pt-0">
            <div class="row">
              <div class="col-12">
                <table class="table">
                  <thead>
                    <tr>
                      <th scope="col">#</th>
                      <th scope="col">Project</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php
                        $sql_projects_assigned = mysqli_query($dbc,"SELECT project_id FROM pm_resources WHERE resource_name='".$project."' GROUP BY project_id");
                        $no = 1;
                        while($assigned_project = mysqli_fetch_array($sql_projects_assigned))
                        {
                          $project_name = mysqli_fetch_array(mysqli_query($dbc,"SELECT project_name FROM pm_projects WHERE project_id='".$assigned_project['project_id']."'"));
                          ?>
                          <tr>
                            <th scope="row"><?php echo $no++;?></th>
                            <td><?php echo $project_name['project_name'];?></td>
                          </tr>
                          <?php
                        }
                     ?>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
          <div class="card-footer">
            <div class="text-right">
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="row">

            <div class="col-12 col-sm-12">
              <div class="card bg-light">
                <div class="card-header text-muted border-bottom-0">
                  Tasks
                </div>
                <div class="card-body pt-0">
                  <div class="row">
                    <div class="col-12 table-responsive">
                      <table class="table table-striped" id="tasks-with-resources-reports-table" width="100%">
                        <thead>
                          <tr>
                            <th scope="col">#</th>
                            <th scope="col">Task</th>
                            <th scope="col">Other Resources</th>
                            <th scope="col">Due</th>
                            <th scope="col">Status</th>
                          </tr>
                        </thead>
                        <tbody>
                          <?php
                              $no = 1;
                              $sql_tasks = mysqli_query($dbc,"SELECT * FROM pm_activities WHERE task_id IN
                                                                (SELECT activity_id FROM pm_resources WHERE resource_name='".$project."')");

                              while($row_tasks = mysqli_fetch_array($sql_tasks))
                              {
                                ?>
                                <tr>
                                  <th scope="row"><?php echo $no++;?></th>
                                  <td><?php echo $row_tasks['activity_name'];?></td>
                                  <td>
                                      <?php
                                        $count = 1;
                                        $other_resources = mysqli_query($dbc,"SELECT resource_name FROM pm_resources WHERE activity_id='".$row_tasks['task_id']."' &&
                                                                                      resource_name!='".$project."' GROUP BY resource_name");

                                        if(mysqli_num_rows($other_resources) > 0)
                                        {
                                          while($resource = mysqli_fetch_array($other_resources))
                                          {
                                             echo $count++ ." . ". $resource['resource_name'] . "<br/>";
                                          }
                                        }
                                        else
                                        {
                                          echo "No other resources";
                                        }



                                       ?>
                                  </td>
                                  <td>
                                    <?php
                                    $todays_date = date('d-M-Y');

                                    $date1 = new DateTime($row_tasks['end_date']); //inclusive
                                   $date2 = new DateTime($todays_date); //exclusive
                                   $diff = $date2->diff($date1);

                                   $days = $diff->format("%R");
                                   if($days == "-")
                                   {
                                     ?>
                                      <span class="badge badge-danger"><?php echo $row_tasks['end_date'];?> <br/><br/> (<?php echo $diff->format("%R%a") . " Days Ago";?>)</span>
                                     <?php
                                   }
                                   else
                                   {
                                     ?>
                                     <span class="badge badge-info"><?php echo $row_tasks['end_date'];?> <br/><br/> (<?php echo $diff->format("%R%a") . " More Days";?>)</span>
                                     <?php
                                   }
                                     ?>
                                  </td>
                                  <td>
                                    <?php
                                        $status = mysqli_fetch_array(mysqli_query($dbc,"SELECT status,color_code FROM pm_activity_updates WHERE changed='no' && task_id='".$row_tasks['task_id']."'"));
                                    ?>
                                    <span class="badge <?php echo $status['color_code'];?>"><?php echo $status['status'];?></span>

                                  </td>
                                </tr>
                                <?php
                              }
                           ?>
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
                <div class="card-footer">
                  <div class="text-right">
                  </div>
                </div>
              </div>
            </div>
    </div>
  </div>


  <!-- end row for project resource -->
    <script>
    //START TASK STATUS CHART
    var radarCanvas = $('#task-status-report-chart').get(0).getContext('2d');
    var radarData        = {
      labels: ['Not Started', 'In Progress Behind Schedule', 'In Progress Within Schedule', 'Completed','Continous','Repriotized'],
datasets: [{
    data: [<?php echo $not_started ;?>,<?php echo $behind ;?>,<?php echo $within ;?>,<?php echo $completed ;?>,<?php echo $continous;?>,<?php echo $repriotized ;?>]
}]


    }

    var radarOptions     = {
      plugins: {

        colorschemes: {

          scheme: 'brewer.DarkTwo8'

        }

      },
      legend: {
            display: false,
      },
      scale: {
    angleLines: {
        display: true
    },
    ticks: {
        suggestedMax: <?php echo $total_tasks_per_resource ;?>
    }
}

    }
    //Create pie or douhnut chart
    // You can switch between pie and douhnut using the method below.
    var radarChart = new Chart(radarCanvas, {
      type: 'radar',
      data: radarData,
      options: radarOptions
    })


    //END TASK STATUS CHART
    </script>

  <?php

//end project resource repirt
}
else if ($report_type == 'project-timesheet-report')
{
//start project timesheet report
//get event dates from pm_activites
$resource_name = mysqli_real_escape_string($dbc,strip_tags($_POST['project']));
$a = mysqli_query($dbc,"SELECT * FROM pm_activities WHERE task_id IN
                (SELECT activity_id FROM pm_resources WHERE resource_name='".$resource_name."')");
$d1 = array();
while($row_tasks = mysqli_fetch_array($a))
{
$date0= date_create($row_tasks['start_date']);
$date1= date_format($date0,"Y-m-d");


$date00= date_create($row_tasks['end_date']);
$date11= date_format($date00,"Y-m-d");

$d1[] =  array (
'startDate' =>  $date1,
'endDate' => $date11,
'summary' => $row_tasks['activity_name']
);

}

$calendar =  json_encode($d1);

?>
<div class="card-header">
<h3 class="card-title">Timesheet Report for <?php echo $resource_name;?></h3>
<div class="card-tools">
<!-- Maximize Button -->
<button type="button" class="btn btn-tool" data-card-widget="maximize"><i class="fas fa-expand"></i></button>
</div>
<!-- /.card-tools -->
</div>
<!-- /.card-header -->
<div class="card-body table-responsive timesheet-report">
</div>

<script>
//task calendar
$(".timesheet-report").simpleCalendar({
fixedStartDay: false,
disableEmptyDetails: true,
events:  <?php echo $calendar;?>,

});

$(document).on("click",'.btn-next, .btn-prev', function(e){
e.preventDefault();
})

</script>
<!-- /.card-body -->

<?php

//end project timesheet report
}
else if ($report_type == 'project-risks-issues-lessons-report')
{
//start project risk,issues & lessons report
if($project == "all")
{
//fetch all active projects
$sql_projects = mysqli_query($dbc,"SELECT * FROM pm_projects
                         WHERE project_id IN
                         (SELECT project_id FROM pm_projects_update_status WHERE project_status='Active' && changed='no' ORDER BY id DESC)");

//active risks for pie chart
$all_risks_sql = mysqli_query($dbc,"SELECT risk_id FROM pm_risks");
$all_risks = mysqli_num_rows($all_risks_sql);

$active_risks_sql = mysqli_query($dbc,"SELECT risk_id FROM pm_risks WHERE status='open'");
$active_risks = mysqli_num_rows($active_risks_sql);

$closed_risks_sql = mysqli_query($dbc,"SELECT risk_id FROM pm_risks WHERE status='closed'");
$closed_risks = mysqli_num_rows($closed_risks_sql);


$ratings_sql = mysqli_query($dbc,"SELECT risk_id, count(*) AS ratings,overall_score FROM pm_risks_updates WHERE changed='no' GROUP BY overall_score");

while($row_ratings = mysqli_fetch_array($ratings_sql))
{
 $ratings[] = $row_ratings['ratings'];
 $overall_score[] = $row_ratings['overall_score'];
}
$ratings_json = json_encode($ratings);
$overall_score_json = json_encode($overall_score);


//start issues
$high_issue_priority_sql = mysqli_query($dbc,"SELECT priority FROM pm_issue_logs_updates WHERE changed='no' && priority='High'");
$high_count = mysqli_num_rows($high_issue_priority_sql);

$medium_issue_priority_sql = mysqli_query($dbc,"SELECT priority FROM pm_issue_logs_updates WHERE changed='no' && priority='Medium'");
$medium_count = mysqli_num_rows($medium_issue_priority_sql);

$low_issue_priority_sql = mysqli_query($dbc,"SELECT priority FROM pm_issue_logs_updates WHERE changed='no' && priority='Low'");
$low_count = mysqli_num_rows($low_issue_priority_sql);

$total_issues_sql = mysqli_query($dbc,"SELECT priority FROM pm_issue_logs_updates WHERE changed='no'");
$total_issues = mysqli_num_rows($total_issues_sql);
//end issues

//pie chart for issue type distribution
$issue_types_sql = mysqli_query($dbc,"SELECT issue_id, count(*) AS issues,issue_type FROM pm_issue_logs_updates WHERE changed='no'
                                          GROUP BY issue_type");

while ($row_issue_type = mysqli_fetch_array($issue_types_sql))
{
  $count_issue_types[] = $row_issue_type['issues'];
  $issue_type_description[] = str_replace("_"," ",$row_issue_type['issue_type']);
}
$count_issue_types_json = json_encode($count_issue_types);
$issue_type_description_json = json_encode($issue_type_description);

//end pie chart for issue type distribution


//doughnut chart for issue severity distribution
$issue_severity_sql = mysqli_query($dbc,"SELECT issue_id, count(*) AS issues,severity FROM pm_issue_logs_updates WHERE changed='no'
                                          GROUP BY severity");

while ($row_issue_severity = mysqli_fetch_array($issue_severity_sql))
{
  $count_issue_severity[] = $row_issue_severity['issues'];
  $issue_severity_description[] = $row_issue_severity['severity'];
}
$count_issue_severity_json = json_encode($count_issue_severity);
$issue_severity_description_json = json_encode($issue_severity_description);

//end doughnut chart for issue severity distribution

//start pie chart for lesson type distribution
$lesson_type_sql = mysqli_query($dbc,"SELECT lesson_learnt_id, count(*) AS lessons,lesson_type FROM pm_lessons_learnt
                                                      GROUP BY lesson_type");

while ($row_lesson_type = mysqli_fetch_array($lesson_type_sql))
{
  $count_lesson_type[] = $row_lesson_type['lessons'];
  $lesson_type_description[] = $row_lesson_type['lesson_type'];
}
$count_lesson_type_json = json_encode($count_lesson_type);
$lesson_type_description_json = json_encode($lesson_type_description);


//end pie chart for leson distribution

//start doughnut chart for lesson project phase distribution
$lesson_project_phase_sql = mysqli_query($dbc,"SELECT lesson_learnt_id, count(*) AS lessons,phase FROM pm_lessons_learnt GROUP BY phase");

while ($row_lesson_project_phase = mysqli_fetch_array($lesson_project_phase_sql))
{
  $count_lesson_project_phase[] = $row_lesson_project_phase['lessons'];
  $lesson_project_phase_description[] = str_replace("_"," ",ucwords($row_lesson_project_phase['phase']));
}
$count_lesson_project_phase_json = json_encode($count_lesson_project_phase);
$lesson_project_phase_description_json = json_encode($lesson_project_phase_description);


//end doughnut chart for lesson project phase distribution


//start doughnut chart for lesson associated risk distribution
$lesson_risk_sql = mysqli_query($dbc,"SELECT lesson_learnt_id, count(*) AS lessons,related_risk FROM pm_lessons_learnt GROUP BY related_risk");

while ($row_lesson_risk = mysqli_fetch_array($lesson_risk_sql))
{
  $related_risk = mysqli_fetch_array(mysqli_query($dbc,"SELECT risk_description FROM pm_risks_updates WHERE changed='no' &&
                                                   risk_id='".$row_lesson_risk['related_risk']."'"));

  $count_lesson_risk[] = $row_lesson_risk['lessons'];
  $lesson_risk_description[] = $related_risk['risk_description'];
}
$count_lesson_risk_json = json_encode($count_lesson_risk);
$lesson_risk_description_json = json_encode($lesson_risk_description);

//end doughnut chart for lesson associated risk distribution

}
else
{
//fetch selected project
$sql_projects = mysqli_query($dbc,"SELECT * FROM pm_projects WHERE project_id='".$project."'");

//active risks for pie chart
$risks_sql = mysqli_query($dbc,"SELECT risk_id,count(*) AS active_risks FROM pm_risks WHERE status='open' && project_id='".$project."'");
while($row_active_risks = mysqli_fetch_array($risks_sql))
{
  $active_risks[] = $row_active_risks['active_risks'];
}
$active_risks_json = json_encode($active_risks);

$ratings_sql = mysqli_query($dbc,"SELECT risk_id, count(*) AS ratings,overall_score FROM pm_risks_updates WHERE changed='no' && project_id='".$project."' GROUP BY overall_score");

while($row_ratings = mysqli_fetch_array($ratings_sql))
{
 $ratings[] = $row_ratings['ratings'];
 $overall_score[] = $row_ratings['overall_score'];
}
 $ratings_json = json_encode($ratings);
 $overall_score_json = json_encode($overall_score);





 //start issues
 $high_issue_priority_sql = mysqli_query($dbc,"SELECT priority FROM pm_issue_logs_updates WHERE changed='no' && priority='High' && project_id='".$project."'");
 $high_count = mysqli_num_rows($high_issue_priority_sql);

 $medium_issue_priority_sql = mysqli_query($dbc,"SELECT priority FROM pm_issue_logs_updates WHERE changed='no' && priority='Medium' && project_id='".$project."'");
 $medium_count = mysqli_num_rows($medium_issue_priority_sql);

 $low_issue_priority_sql = mysqli_query($dbc,"SELECT priority FROM pm_issue_logs_updates WHERE changed='no' && priority='Low' && project_id='".$project."'");
 $low_count = mysqli_num_rows($low_issue_priority_sql);

 $total_issues_sql = mysqli_query($dbc,"SELECT priority FROM pm_issue_logs_updates WHERE changed='no'  && project_id='".$project."'");
 $total_issues = mysqli_num_rows($total_issues_sql);
 //end issues


 //pie chart for issue type distribution
 $issue_types_sql = mysqli_query($dbc,"SELECT issue_id, count(*) AS issues,issue_type FROM pm_issue_logs_updates WHERE changed='no'
                                          && project_id='".$project."' GROUP BY issue_type");

 while ($row_issue_type = mysqli_fetch_array($issue_types_sql))
 {
   $count_issue_types[] = $row_issue_type['issues'];
   $issue_type_description[] = str_replace("_"," ",$row_issue_type['issue_type']);
 }
 $count_issue_types_json = json_encode($count_issue_types);
 $issue_type_description_json = json_encode($issue_type_description);

 //end pie chart for issue type distribution

 //doughnut chart for issue severity distribution
 $issue_severity_sql = mysqli_query($dbc,"SELECT issue_id, count(*) AS issues,severity FROM pm_issue_logs_updates WHERE changed='no'
                                           && project_id='".$project."' GROUP BY severity");

 while ($row_issue_severity = mysqli_fetch_array($issue_severity_sql))
 {
   $count_issue_severity[] = $row_issue_severity['issues'];
   $issue_severity_description[] = $row_issue_severity['severity'];
 }
 $count_issue_severity_json = json_encode($count_issue_severity);
 $issue_severity_description_json = json_encode($issue_severity_description);

 //end doughnut chart for issue severity distribution

 //start pie chart for lesson type distribution
 $lesson_type_sql = mysqli_query($dbc,"SELECT lesson_learnt_id, count(*) AS lessons,lesson_type FROM pm_lessons_learnt WHERE
                                           project_id='".$project."' GROUP BY lesson_type");

 while ($row_lesson_type = mysqli_fetch_array($lesson_type_sql))
 {
   $count_lesson_type[] = $row_lesson_type['lessons'];
   $lesson_type_description[] = $row_lesson_type['lesson_type'];
 }
 $count_lesson_type_json = json_encode($count_lesson_type);
 $lesson_type_description_json = json_encode($lesson_type_description);


 //end pie chart for lesson type distribution


 //start doughnut chart for lesson project phase distribution
 $lesson_project_phase_sql = mysqli_query($dbc,"SELECT lesson_learnt_id, count(*) AS lessons,phase FROM pm_lessons_learnt WHERE
                                           project_id='".$project."' GROUP BY phase");

 while ($row_lesson_project_phase = mysqli_fetch_array($lesson_project_phase_sql))
 {
   $count_lesson_project_phase[] = $row_lesson_project_phase['lessons'];
   $lesson_project_phase_description[] = str_replace("_"," ",ucwords($row_lesson_project_phase['phase']));
 }
 $count_lesson_project_phase_json = json_encode($count_lesson_project_phase);
 $lesson_project_phase_description_json = json_encode($lesson_project_phase_description);


 //end doughnut chart for lesson project phase distribution

 //start doughnut chart for lesson associated risk distribution
 $lesson_risk_sql = mysqli_query($dbc,"SELECT lesson_learnt_id, count(*) AS lessons,related_risk FROM pm_lessons_learnt WHERE
                                           project_id='".$project."' GROUP BY related_risk");

 while ($row_lesson_risk = mysqli_fetch_array($lesson_risk_sql))
 {
   $related_risk = mysqli_fetch_array(mysqli_query($dbc,"SELECT risk_description FROM pm_risks_updates WHERE changed='no' &&
                                                    risk_id='".$row_lesson_risk['related_risk']."'"));

   $count_lesson_risk[] = $row_lesson_risk['lessons'];
   $lesson_risk_description[] = $related_risk['risk_description'];
 }
 $count_lesson_risk_json = json_encode($count_lesson_risk);
 $lesson_risk_description_json = json_encode($lesson_risk_description);

 //end doughnut chart for lesson associated risk distribution

}

if($project == "all")
{
  $project_risks_lessons_issues = "For All Projects";
}
else
{
  $project_risks_lessons_issues_row = mysqli_fetch_array(mysqli_query($dbc,"SELECT project_name FROM pm_projects WHERE project_id='".$project."'"));
  $project_risks_lessons_issues = "For ".$project_risks_lessons_issues_row['project_name'];
}

?>
<div class="card-header">
<h3 class="card-title">Project Risks, Issues & Lessons <?php echo $project_risks_lessons_issues;?></h3>
<div class="card-tools">
<!-- Maximize Button -->
<button type="button" class="btn btn-tool" data-card-widget="maximize"><i class="fas fa-expand"></i></button>
</div>
<!-- /.card-tools -->
</div>
<!-- /.card-header -->
<div class="card-body table-responsive">

  <div class="row">
    <div class="col-md-12">
      <div class="card">
        <div class="card-header">
          <h5 class="card-title">Risk Summary</h5>

          <div class="card-tools">
            <button type="button" class="btn btn-tool" data-card-widget="collapse">
              <i class="fas fa-minus"></i>
            </button>
          </div>
        </div>
        <!-- /.card-header -->
        <div class="card-body">
          <div class="row">
              <form action="views/reports/pdf/pdf-pm-risk-report.php" target="_blank" id="download-project-risk-report-form">
                  <input type="hidden" name="pm-pdf-report-type" value="project-risks-issues-lessons-report">
                  <button type="submit" class="btn btn-success risk-pdf-download-button">
                      <i class="fas fa-file-pdf"></i> PDF Download
                  </button>
              </form>

          </div>
          <div class="row">
            <!-- /.col -->
            <div class="col-md-4"></div>
            <div class="col-md-4">
              <p class="text-center">
                <strong>Risk Rating Distribution</strong><br/>
              </p>
              <div class="table-responsive">
                <div class="chart">
                  <canvas id="risk-rating-distribution-chart" width="477" height="300" style=" display: block;" class="chartjs-render-monitor"</canvas>
                </div>
              </div>
            </div>
            <!-- /.col -->
            <div class="col-md-4"></div>
          </div>
          <!-- /.row -->
          <div class="row">
            <div class="col-12 table-responsive">
              <table class="table" id="pm-projects-portfolio-risks-table" width="100%">
                <thead class="thead-light">
                  <tr>
                    <th scope="col">#</th>
                    <th scope="col">Risk</th>
                    <th scope="col">Impact</th>
                    <th scope="col">Project</th>
                    <th scope="col">Project Phase Affected</th>
                    <th scope="col">Proximity</th>
                    <th scope="col">Rating</th>
                    <th scope="col">Prior Rating</th>
                    <th scope="col">Risk Owner</th>
                    <th scope="col">Mitigations Strategy</th>
                    <th scope="col">Actions Applied</th>
                  </tr>
                </thead>
                <tbody>
                  <?php
                       $no = 1;
                       if($project == "all")
                       {
                         $project_risks = mysqli_query($dbc,"SELECT * FROM pm_risks_updates WHERE changed='no' && risk_id IN
                                                                     (SELECT risk_id FROM pm_risks WHERE status='open')");
                       }
                       else
                       {
                         $project_risks = mysqli_query($dbc,"SELECT * FROM pm_risks_updates WHERE project_id='".$project."' && changed='no' && risk_id IN
                                                                     (SELECT risk_id FROM pm_risks WHERE status='open')");
                       }

                       while($project_risks_row = mysqli_fetch_array($project_risks))
                       {
                         ?>
                         <tr>
                           <th scope="row"><?php echo $no++ ;?></th>
                           <td><?php echo $project_risks_row['risk_description'];?></td>
                           <td><?php echo $project_risks_row['impact'];?></td>
                           <td>
                             <?php
                                  $project_name_title = mysqli_fetch_array(mysqli_query($dbc,"SELECT project_name FROM pm_projects
                                                                      WHERE project_id='".$project_risks_row['project_id']."'"));
                              echo $project_name_title['project_name'];?>

                           </td>
                           <td><?php echo $project_risks_row['phase'];?></td>
                           <td><?php echo $project_risks_row['proximity'];?></td>
                           <td>
                             <?php
                              $overall_score = $project_risks_row['overall_score'];
                              $likelihood_score = $project_risks_row['likelihood_score'];
                              $impact_score = $project_risks_row['impact_score'];
                              if($overall_score < 26 && $overall_score > 19 )
                              {
                                ?>
                                <span class="badge five"><?php echo $overall_score;?> <br/>(L<?php echo $likelihood_score;?>) * (I <?php echo $impact_score;?>)</span>
                                <?php
                              }
                              if($overall_score < 17 && $overall_score > 9 )
                              {
                                ?>
                                <span class="badge four"><?php echo $overall_score;?> <br/>(L<?php echo $likelihood_score;?>) * (I <?php echo $impact_score;?>)</span>
                                <?php
                              }
                              if($overall_score < 10 && $overall_score > 5 )
                              {
                                ?>
                                <span class="badge three"><?php echo $overall_score;?> <br/>(L<?php echo $likelihood_score;?>) * (I <?php echo $impact_score;?>)</span>
                                <?php
                              }
                              if($overall_score < 5 && $overall_score > 2 )
                              {
                                ?>
                                <span class="badge two"><?php echo $overall_score;?> <br/> (L<?php echo $likelihood_score;?>) * (I <?php echo $impact_score;?>)</span>
                                <?php
                              }
                              if($overall_score < 3 && $overall_score > 0 )
                              {
                                ?>
                                <span class="badge one"><?php echo $overall_score;?> <br/>(L<?php echo $likelihood_score;?>) * (I <?php echo $impact_score;?>)</span>
                                <?php
                              }
                              ?>
                            </td>
                            <td>
                                <?php
                                    $prior_rating = mysqli_fetch_array(mysqli_query($dbc,"SELECT likelihood_score,impact_score,overall_score FROM
                                                                                pm_risks_updates WHERE risk_id='".$project_risks_row['risk_id']."'
                                                                                ORDER BY id DESC LIMIT 1,1"));

                                    if($overall_score > $prior_rating['overall_score'])
                                    {
                                      if($prior_rating['overall_score'] == NULL)
                                      {
                                        ?>
                                        <span class="text-danger">
                                                <i class="fa fa-caret-up"></i>
                                              0 <br/>
                                              (L<?php echo $prior_rating['likelihood_score'];?>) * (I <?php echo $prior_rating['impact_score'];?>)

                                        </span>
                                        <?php
                                      }
                                      else
                                      {
                                        ?>
                                        <span class="text-danger">
                                                <i class="fa fa-caret-up"></i>
                                              <?php echo $prior_rating['overall_score'];?> <br/>
                                              (L<?php echo $prior_rating['likelihood_score'];?>) * (I <?php echo $prior_rating['impact_score'];?>)

                                        </span>
                                        <?php
                                      }
                                      ?>
                                      <?php
                                    }
                                    else if ($overall_score == $prior_rating['overall_score'])
                                    {
                                      ?>
                                      <span class="text-warning">
                                              <i class="fa fa-arrows-h"></i>
                                            <?php echo $prior_rating['overall_score'];?> <br/>
                                            (L<?php echo $prior_rating['likelihood_score'];?>) * (I <?php echo $prior_rating['impact_score'];?>)

                                      </span>
                                      <?php
                                    }
                                    else if ($overall_score < $prior_rating['overall_score'])
                                    {
                                      ?>
                                      <span class="text-success">
                                              <i class="fa fa-caret-down"></i>
                                            <?php echo $prior_rating['overall_score'];?> <br/>
                                            (L<?php echo $prior_rating['likelihood_score'];?>) * (I <?php echo $prior_rating['impact_score'];?>)

                                      </span>
                                      <?php
                                    }

                                 ?>
                            </td>
                           <td><?php echo $project_risks_row['risk_owner'];?></td>
                           <td><?php echo $project_risks_row['mitigation_strategy'];?></td>
                           <td><?php echo $project_risks_row['actions_applied'];?></td>
                         </tr>
                         <?php
                       }
                   ?>
                </tbody>
              </table>
            </div>
          </div>
        </div>
        <!-- ./card-body -->
      </div>
      <!-- /.card -->
    </div>
    <!-- /.col -->
  </div>
  <!-- /.row -->

  <div class="row">
    <div class="col-md-12">
      <div class="card">
        <div class="card-header">
          <h5 class="card-title">Open Issues</h5>

          <div class="card-tools">
            <button type="button" class="btn btn-tool" data-card-widget="collapse">
              <i class="fas fa-minus"></i>
            </button>
          </div>
        </div>
        <!-- /.card-header -->
        <div class="card-body">
          <div class="row">
            <!-- /.col -->
            <div class="col-md-4">
              <p class="text-center">
                <strong>Issue Type Distribution</strong><br/>
              </p>
              <div class="table-responsive">
                <div class="chart">
                  <canvas id="issue-type-distribution-chart" width="477" height="300" style=" display: block;" class="chartjs-render-monitor"</canvas>
                </div>
              </div>
            </div>
            <!-- /.col -->
            <!-- /.col -->
            <div class="col-md-4">
              <p class="text-center">
                <strong>Issue Severity Distribution</strong><br/>
              </p>
              <div class="table-responsive">
                <div class="chart">
                  <canvas id="issue-severity-distribution-chart" width="477" height="300" style=" display: block;" class="chartjs-render-monitor"</canvas>
                </div>
              </div>
            </div>
            <!-- /.col -->

            <!-- /.col -->
            <div class="col-md-4">
              <p class="text-center">
                <strong>Issue Priority Distribution</strong><br/>
              </p>
              <div class="table-responsive">
                <div class="progress-group">
                   High
                   <span class="float-right"><b><?php echo $high_count;?></b>/<?php echo $total_issues;?></span>
                   <div class="progress progress-sm">
                     <?php
                      $percentage = ($high_count/$total_issues) * 100;
                      ?>
                      <div class="progress-bar bg-danger" style="width: <?php echo $percentage;?>%"></div>
                   </div>
                </div>
                <!-- /.progress-group -->
                <div class="progress-group">
                   Medium
                   <span class="float-right"><b><?php echo $medium_count;?></b>/<?php echo $total_issues;?></span>
                   <div class="progress progress-sm">
                     <?php
                      $percentage = ($medium_count/$total_issues) * 100;
                      ?>
                      <div class="progress-bar bg-warning" style="width: <?php echo $percentage;?>%"></div>
                   </div>
                </div>
                <!-- /.progress-group -->
                <!-- /.progress-group -->
                <div class="progress-group">
                   Low
                   <span class="float-right"><b><?php echo $low_count;?></b>/<?php echo $total_issues;?></span>
                   <div class="progress progress-sm">
                     <?php
                      $percentage = ($low_count/$total_issues) * 100;
                      ?>
                      <div class="progress-bar bg-success" style="width: <?php echo $percentage;?>%"></div>
                   </div>
                </div>
                <!-- /.progress-group -->
              </div>
            </div>
            <!-- /.col -->

          </div>
          <!-- /.row -->

          <div class="row">
            <div class="col-12 table-responsive">
              <table class="table" id="pm-projects-portfolio-issue-logs-table" width="100%">
                <thead class="thead-light">
                  <tr>
                    <th scope="col">#</th>
                    <th scope="col">Issue Description</th>
                    <th scope="col">Issue Type</th>
                    <th scope="col">Date Raised</th>
                    <th scope="col">Project</th>
                    <th scope="col">Due</th>
                    <th scope="col">Raised by</th>
                    <th scope="col">Issue Author</th>
                    <th scope="col">Priority</th>
                    <th scope="col">Severity</th>
                    <th scope="col">Next Action</th>
                    <th scope="col">Person Responsible</th>
                  </tr>
                </thead>
                <tbody>
                  <?php
                       $no = 1;
                       if($project == "all")
                       {
                         $project_issues = mysqli_query($dbc,"SELECT * FROM pm_issue_logs_updates WHERE changed='no' && issue_id IN
                                                                     (SELECT issue_id FROM pm_issue_logs WHERE status='open')");
                       }
                       else
                       {
                         $project_issues = mysqli_query($dbc,"SELECT * FROM pm_issue_logs_updates WHERE project_id='".$project."' && changed='no' && issue_id IN
                                                                     (SELECT issue_id FROM pm_issue_logs WHERE status='open')");
                       }

                       while($project_issues_row = mysqli_fetch_array($project_issues))
                       {
                         ?>
                         <tr>
                           <th scope="row"><?php echo $no++ ;?></th>
                           <td><?php echo $project_issues_row['issue_description'];?></td>
                           <td>
                             <?php
                             $issue_type = str_replace("_", " ", $project_issues_row['issue_type']);
                             echo $issue_type;
                             ?>
                           </td>
                           <td><?php echo $project_issues_row['date_raised'];?></td>
                           <td>
                             <?php
                                  $project_name_title = mysqli_fetch_array(mysqli_query($dbc,"SELECT project_name FROM pm_projects
                                                                      WHERE project_id='".$project_issues_row['project_id']."'"));
                              echo $project_name_title['project_name'];?>
                           </td>
                           <td><?php echo $project_issues_row['due_date'];?></td>
                           <td><?php echo $project_issues_row['raised_by'];?></td>
                           <td><?php echo $project_issues_row['issue_author'];?></td>
                           <td>
                             <?php
                             if($project_issues_row['priority'] == 'High')
                             {
                               ?>
                                 <i class="fas fa-arrow-up text-danger"></i> <small class="text-danger"><?php echo $project_issues_row['priority'];?></small>
                               <?php
                             }
                             else if($project_issues_row['priority'] == 'Medium')
                             {
                               ?>
                               <i class="fas fa-arrows-h text-warning"></i> <small class="text-warning"><?php echo $project_issues_row['priority'];?></small>
                               <?php
                             }
                             else if($project_issues_row['priority'] == 'Low')
                             {
                               ?>
                               <i class="fas fa-arrow-down text-success"></i> <small class="text-success"><?php echo $project_issues_row['priority'];?></small>
                               <?php
                             }
                             ?>
                           </td>
                           <td>
                             <?php
                             if($project_issues_row['severity'] == 'Blocker')
                             {
                               ?>
                                 <span class="badge five"><?php echo $project_issues_row['severity'];?></span>
                               <?php
                             }
                             else if($project_issues_row['severity'] == 'Critical')
                             {
                               ?>
                                 <span class="badge four"><?php echo $project_issues_row['severity'];?></span>
                               <?php
                             }
                             else if($project_issues_row['severity'] == 'Major')
                             {
                               ?>
                                 <span class="badge three"><?php echo $project_issues_row['severity'];?></span>
                               <?php
                             }
                             else if($project_issues_row['severity'] == 'Minor')
                             {
                               ?>
                                 <span class="badge two"><?php echo $project_issues_row['severity'];?></span>
                               <?php
                             }
                             else if($project_issues_row['severity'] == 'Trival')
                             {
                               ?>
                                 <span class="badge one"><?php echo $project_issues_row['severity'];?></span>
                               <?php
                             }
                             ?>
                           </td>
                           <td><?php echo $project_issues_row['next_action'];?></td>
                           <td><?php echo $project_issues_row['person_responsible'];?></td>
                         </tr>
                         <?php
                       }
                   ?>
                </tbody>
              </table>
            </div>
          </div>


        </div>
        <!-- ./card-body -->
      </div>
      <!-- /.card -->
    </div>
    <!-- /.col -->
  </div>
  <!-- /.row -->



  <div class="row">
    <div class="col-md-12">
      <div class="card">
        <div class="card-header">
          <h5 class="card-title">Lessons Learnt</h5>

          <div class="card-tools">
            <button type="button" class="btn btn-tool" data-card-widget="collapse">
              <i class="fas fa-minus"></i>
            </button>
          </div>
        </div>
        <!-- /.card-header -->
        <div class="card-body">
          <div class="row">
            <!-- /.col -->
            <div class="col-md-4">
              <p class="text-center">
                <strong>Lesson Type Distribution</strong><br/>
              </p>
              <div class="table-responsive">
                <div class="chart">
                  <canvas id="lesson-type-distribution-chart" width="477" height="300" style=" display: block;" class="chartjs-render-monitor"</canvas>
                </div>
              </div>
            </div>
            <!-- /.col -->
            <!-- /.col -->
            <div class="col-md-4">
              <p class="text-center">
                <strong>Affected Project Phase</strong><br/>
              </p>
              <div class="table-responsive">
                <div class="chart">
                  <canvas id="affected-project-phase-distribution-chart" width="477" height="300" style=" display: block;" class="chartjs-render-monitor"</canvas>
                </div>
              </div>
            </div>
            <!-- /.col -->

            <!-- /.col -->
            <div class="col-md-4">
              <p class="text-center">
                <strong>Associated Risk</strong><br/>
              </p>
              <div class="table-responsive">
                <div class="chart">
                  <canvas id="associated-risk-distribution-chart" width="477" height="300" style=" display: block;" class="chartjs-render-monitor"</canvas>
                </div>
              </div>
            </div>
            <!-- /.col -->

          </div>
          <!-- /.row -->

          <div class="row">
           <div class="col-12 table-responsive">
              <table class="table" id="pm-projects-portfolio-lessons-table" width="100%">
                 <thead class="thead-light">
                    <tr>
                       <th scope="col">#</th>
                       <th scope="col">Lesson Description</th>
                       <th scope="col">Lesson Date</th>
                       <th scope="col">Phase</th>
                       <th scope="col">Lesson Type</th>
                       <th scope="col">Project</th>
                       <th scope="col">Impact to Project</th>
                       <th scope="col">Related Risk</th>
                       <th scope="col">Response</th>
                       <th scope="col">Actions Applied</th>
                    </tr>
                 </thead>
                 <tbody>
                    <?php
                       $no = 1;
                       if($project == "all")
                       {
                         $project_lessons = mysqli_query($dbc,"SELECT * FROM pm_lessons_learnt ORDER BY id DESC");
                       }
                       else
                       {
                         $project_lessons = mysqli_query($dbc,"SELECT * FROM pm_lessons_learnt WHERE project_id='".$project."' ORDER BY id DESC");
                       }

                       while($project_lessons_row = mysqli_fetch_array($project_lessons))
                       {
                         ?>
                    <tr>
                       <th scope="row"><?php echo $no++ ;?></th>
                       <td><?php echo $project_lessons_row['lesson_description'];?></td>
                       <td><?php echo $project_lessons_row['lesson_date'];?></td>
                       <td>
                          <?php
                             echo str_replace("_", " ", $project_lessons_row['phase']);
                             ?>
                       </td>
                       <td><?php echo $project_lessons_row['lesson_type'];?></td>
                       <td>
                          <?php
                             $project_name_title = mysqli_fetch_array(mysqli_query($dbc,"SELECT project_name FROM pm_projects
                                                                 WHERE project_id='".$project_lessons_row['project_id']."'"));
                             echo $project_name_title['project_name'];?>
                       </td>
                       <td><?php echo $project_lessons_row['impact_to_project'];?></td>
                       <td>
                          <?php
                             $related_risk = mysqli_fetch_array(mysqli_query($dbc,"SELECT risk_description FROM pm_risks_updates
                                                                                     WHERE changed='no'
                                                                                     && risk_id='".$project_lessons_row['related_risk']."'"));

                             echo $related_risk['risk_description'];
                             ?>
                       </td>
                       <td><?php echo $project_lessons_row['response'];?></td>
                       <td><?php echo $project_lessons_row['actions_applied'];?></td>
                    </tr>
                    <?php
                       }
                       ?>
                 </tbody>
              </table>
           </div>
        </div>
        </div>
        <!-- ./card-body -->
      </div>
      <!-- /.card -->
    </div>
    <!-- /.col -->
  </div>
  <!-- /.row -->

</div>
<!-- /.card-body -->

<script>
//STARt risk rating distribution CHART
var riskChartCanvas = $('#risk-rating-distribution-chart').get(0).getContext('2d');
var riskData        = {
labels: <?php echo $overall_score_json;?>,
datasets: [
{
fill: false,
data: <?php echo $ratings_json;?>,
//backgroundColor : ['#f56954', '#00a65a', '#f39c12', '#00c0ef', '#3c8dbc', '#d2d6de'],
}
]
}
var riskOptions     = {
legend: {
display: true,
position: 'bottom',
labels: {
fontColor: '#333',
usePointStyle:true
}
},
plugins: {

colorschemes: {

scheme: 'brewer.DarkTwo8'

}

}
}
//Create pie or douhnut chart
// You can switch between pie and douhnut using the method below.
var riskChart = new Chart(riskChartCanvas, {
type: 'doughnut',
data: riskData,
options: riskOptions
})


//END risk rating distribution CHART



//start issue type distribution chart
var pieChartCanvas = $('#issue-type-distribution-chart').get(0).getContext('2d');
var pieData        = {
  labels: <?php echo $issue_type_description_json;?>,
  datasets: [
    {
      fill: false,
      data: <?php echo $count_issue_types_json;?>,
      //backgroundColor : ['#f56954', '#00a65a', '#f39c12', '#00c0ef', '#3c8dbc', '#d2d6de'],
    }
  ]
}
var pieOptions     = {
  legend: {
    display: true,
    position: 'bottom',
    labels: {
        fontColor: '#333',
        usePointStyle:true
    }
  },
  plugins: {

    colorschemes: {

      scheme: 'brewer.DarkTwo8'

    }

  }
}
//Create pie or douhnut chart
// You can switch between pie and douhnut using the method below.
var pieChart = new Chart(pieChartCanvas, {
  type: 'pie',
  data: pieData,
  options: pieOptions
})

//end issue type distribution chart




//start issue severity distribution chart
var pieChartCanvas = $('#issue-severity-distribution-chart').get(0).getContext('2d');
var pieData        = {
  labels: <?php echo $issue_severity_description_json;?>,
  datasets: [
    {
      fill: false,
      data: <?php echo $count_issue_severity_json;?>,
      //backgroundColor : ['#f56954', '#00a65a', '#f39c12', '#00c0ef', '#3c8dbc', '#d2d6de'],
    }
  ]
}
var pieOptions     = {
  legend: {
    display: true,
    position: 'bottom',
    labels: {
        fontColor: '#333',
        usePointStyle:true
    }
  },
  plugins: {

    colorschemes: {

      scheme: 'brewer.SetOne9'

    }

  }
}
//Create pie or douhnut chart
// You can switch between pie and douhnut using the method below.
var pieChart = new Chart(pieChartCanvas, {
  type: 'doughnut',
  data: pieData,
  options: pieOptions
})

//end issue severity distribution chart






//start lesson type distribution chart
var pieChartCanvas = $('#lesson-type-distribution-chart').get(0).getContext('2d');
var pieData        = {
  labels: <?php echo $lesson_type_description_json;?>,
  datasets: [
    {
      fill: false,
      data: <?php echo $count_lesson_type_json;?>,
      //backgroundColor : ['#f56954', '#00a65a', '#f39c12', '#00c0ef', '#3c8dbc', '#d2d6de'],
    }
  ]
}
var pieOptions     = {
  legend: {
    display: true,
    position: 'bottom',
    labels: {
        fontColor: '#333',
        usePointStyle:true
    }
  },
  plugins: {

    colorschemes: {

      scheme: 'office.Concourse6'

    }

  }
}
//Create pie or douhnut chart
// You can switch between pie and douhnut using the method below.
var pieChart = new Chart(pieChartCanvas, {
  type: 'pie',
  data: pieData,
  options: pieOptions
})

//end lesson type distribution chart




//start lesson project phase distribution chart
var pieChartCanvas = $('#affected-project-phase-distribution-chart').get(0).getContext('2d');
var pieData        = {
  labels: <?php echo $lesson_project_phase_description_json;?>,
  datasets: [
    {
      fill: false,
      data: <?php echo $count_lesson_project_phase_json;?>,
      //backgroundColor : ['#f56954', '#00a65a', '#f39c12', '#00c0ef', '#3c8dbc', '#d2d6de'],
    }
  ]
}
var pieOptions     = {
  legend: {
    display: true,
    position: 'bottom',
    labels: {
        fontColor: '#333',
        usePointStyle:true
    }
  },
  plugins: {

    colorschemes: {

      scheme: 'office.Frame6'

    }

  }
}
//Create pie or douhnut chart
// You can switch between pie and douhnut using the method below.
var pieChart = new Chart(pieChartCanvas, {
  type: 'doughnut',
  data: pieData,
  options: pieOptions
})

//end lesson project phase distribution chart



//start lesson risk distribution chart
var pieChartCanvas = $('#associated-risk-distribution-chart').get(0).getContext('2d');
var pieData        = {
  labels: <?php echo $lesson_risk_description_json;?>,
  datasets: [
    {
      fill: false,
      data: <?php echo $count_lesson_risk_json;?>,
      //backgroundColor : ['#f56954', '#00a65a', '#f39c12', '#00c0ef', '#3c8dbc', '#d2d6de'],
    }
  ]
}
var pieOptions     = {
  legend: {
    display: true,
    position: 'bottom',
    labels: {
        fontColor: '#333',
        usePointStyle:true
    }
  },
  plugins: {

    colorschemes: {

      scheme: 'tableau.ClassicBlueRed12'

    }

  }
}
//Create pie or douhnut chart
// You can switch between pie and douhnut using the method below.
var pieChart = new Chart(pieChartCanvas, {
  type: 'doughnut',
  data: pieData,
  options: pieOptions
})

//end lesson risk distribution chart

</script>
<?php
//end project risk,issues & lessons report
}


?>
