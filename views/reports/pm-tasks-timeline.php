<?php
session_start();
include("../../controllers/setup/connect.php");

$project = mysqli_real_escape_string($dbc,strip_tags($_POST['project']));

  $limit = 5;
  if (isset($_GET["page"])) { $page  = $_GET["page"]; } else { $page=1; };
  $start_from = ($page-1) * $limit;

  $sql = "SELECT * FROM pm_activity_updates WHERE project_id='".$project."' ORDER BY id DESC LIMIT $start_from, $limit";

  $first_log_from_limit = mysqli_fetch_array(mysqli_query($dbc,$sql));
  $rs_result = mysqli_query($dbc, $sql);

    ?>
    <!-- The timeline -->
    <div class="timeline timeline-inverse">
      <?php
        $last_log = mysqli_fetch_array(mysqli_query($dbc,"SELECT * FROM pm_activity_updates
                                            WHERE project_id='".$project."'
                                             ORDER BY id  DESC LIMIT 1"));
       ?>
      <!-- timeline time label -->
      <div class="time-label">
        <span class="bg-success"><i class="far fa-calendar-check"></i>
          <?php
          $date=date_create($last_log['time_recorded']);
          echo date_format($date,"d-M-Y H:i a");

           ?>
        </span>
      </div>
      <!-- /.timeline-label -->
      <!-- timeline item -->
      <?php
         while($logs= mysqli_fetch_array($rs_result)){
           $activity_name = mysqli_fetch_array(mysqli_query($dbc,"SELECT activity_name FROM pm_activities WHERE task_id='".$logs['task_id']."'"));
             ?>
             <div>
               <i class="fas fa-clock bg-primary"></i>


               <div class="timeline-item hvr-overline-from-center" style="width: 90%;">
                 <span class="time"><i class="far fa-calendar-day"></i> <?php echo $logs['date_recorded'];?></span>

                 <h3 class="timeline-header">
                    <a href="#"><?php echo $activity_name['activity_name'];?></a>
                     <i class="far fa-dot-circle"></i>
                     <span class="text-primary"><u><?php echo $logs['status'];?></u></span></h3>

                 <div class="timeline-body">
                    The task status for <strong> <?php echo $activity_name['activity_name'];?></strong> was updated to <strong><?php echo $logs['status'];?></strong> by <?php echo $logs['recorded_by'];?>
                 </div>
                 <div class="timeline-footer">
                   <?php
                      if($logs['comments'] == "")
                      {
                        //no comments
                      }
                      else
                      {
                        ?>
                          <span>Comments : <?php echo $logs['comments'];?></span>
                        <?php
                      }

                    ?>
                 </div>
               </div>
             </div>
             <!-- END timeline item -->

             <?php
         }


?>
