<?php
session_start();
include("../../controllers/setup/connect.php");
if (!isset($_SESSION['email']))
{
     exit("<a href='#' class='login-link'>Please Log in to continue</a>");
}
?>

<div class="input-group mb-4">
  <div class="input-group-prepend">
    <span class="input-group-text bg-white" id="search-addon"><i class="fad fa-search"></i></span>
  </div>
  <input type="text" class="form-control" name="search" id="search" placeholder="Search">
  <span id="search-loader"></span>
</div>
<div id="timeline-data">
</div>
<!--start pagination buttons -->
<?php
$limit = 5;
$sql = "SELECT COUNT(id) FROM activity_logs WHERE Email='".$_SESSION['email']."' ORDER BY id DESC";
$rs_result = mysqli_query($dbc, $sql);
$row = mysqli_fetch_row($rs_result);
$total_records = $row[0];
$total_pages = ceil($total_records / $limit);
?>

<nav aria-label="...">
<ul class='pagination text-center pagination-sm table-responsive' id="pagination">
<?php if(!empty($total_pages)):for($i=1; $i<=$total_pages; $i++):
    if($i == 1):?>
          <li class='active' class="page-item"  id="<?php echo $i;?>">
            <a class="page-link" href='TimeLinePaginated.php?page=<?php echo $i;?>'><?php echo $i;?></a>
          </li>
    <?php else:?>
    <li class="page-item" id="<?php echo $i;?>"><a class="page-link" href='TimeLinePaginated.php?page=<?php echo $i;?>'><?php echo $i;?></a></li>
  <?php endif;?>
<?php endfor;endif;?>
</ul>
</nav>

<!-- end pagination buttons -->
