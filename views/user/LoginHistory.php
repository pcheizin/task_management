<?php
session_start();
include("../../controllers/setup/connect.php");
if (!isset($_SESSION['email']))
{
     exit("<a href='#' class='login-link'>Please Log in to continue</a>");
}
?>

<div class="row">
    <div class="col-12">
      <div class="card card-primary card-outline">
        <!-- /.card-header -->
        <div class="card-body table-responsive p-0">
          <table id="user-profile-sign-in-logs-table" class="table table-bordered table-striped" width="100%">
            <thead>
            <tr>
              <th>No</th>
              <th>IP Address</th>
              <th>Time Signed In</th>
              <th>Time Signed Out</th>
            </tr>
            </thead>
            <tbody>
              <?php
                  $no = 1;
                  $sql = mysqli_query($dbc,"SELECT * FROM sign_in_logs WHERE email='".$_SESSION['email']."' && id > 4722 && user_type='default' ORDER BY id DESC");
                  while($row = mysqli_fetch_array($sql))
                  {
                    ?>
                    <tr>
                      <td><?php echo $no++ ;?></td>
                      <td><?php echo $row['ip_address'];?></td>
                      <td><?php echo $row['time_signed_in'];?></td>
                      <td><?php echo $row['time_signed_out'];?></td>
                    </tr>
                    <?php
                  }
               ?>
            </tbody>
            <tfoot>
            <tr>
              <th>No</th>
              <th>IP Address</th>
              <th>Time Signed In</th>
              <th>Time Signed Out</th>
            </tr>
            </tfoot>
          </table>
        </div>
        <!-- /.card-body -->
      </div>
      <!-- /.card -->
    </div>
  </div>
  <!-- /.row -->
