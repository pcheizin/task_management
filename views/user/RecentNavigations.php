<?php
session_start();
include("../../controllers/setup/connect.php");
if (!isset($_SESSION['email']))
{
     exit("<a href='#' class='login-link'>Please Log in to continue</a>");
}
?>
<div class="row">
    <div class="col-12">
      <div class="card card-primary card-outline">
        <!-- /.card-header -->
        <div class="card-body table-responsive p-0">
          <table id="user-profile-navigations-table" class="table table-hover table-bordered table-striped" width="100%">
            <thead>
            <tr>
              <th>No</th>
              <th>Page Name</th>
              <th>Time Navigated</th>
            </tr>
            </thead>
            <tbody>
              <?php
                  $no = 1;
                  $sql = mysqli_query($dbc,"SELECT * FROM page_requests WHERE requested_by='".$_SESSION['email']."' && user_type='default' ORDER BY id DESC");
                  while($row = mysqli_fetch_array($sql))
                  {
                    if($row['page_id'] =='login-link')
                    {
                      $page_id =  "#";
                    }
                    else
                    {
                     $page_id =  $row['page_id'];
                    }

                    ?>
                    <tr>
                      <td><?php echo $no++ ;?></td>
                      <td class="<?php echo $page_id;?> text-primary" style="cursor:pointer;"><?php echo $row['page_name'];?></td>
                      <td><?php echo $row['time_requested'];?></td>
                    </tr>
                    <?php
                  }
               ?>
            </tbody>
            <tfoot>
            <tr>
              <th>No</th>
              <th>Name</th>
              <th>Time Navigated</th>
            </tr>
            </tfoot>
          </table>
        </div>
        <!-- /.card-body -->
      </div>
      <!-- /.card -->
    </div>
  </div>
  <!-- /.row -->
