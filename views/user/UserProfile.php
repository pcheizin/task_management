<?php
session_start();
include("../../controllers/setup/connect.php");
if($_SERVER['REQUEST_METHOD'] == "POST")
{
  if (!isset($_SESSION['email']))
  {
       exit("<a href='#' class='login-link'>Please Log in to continue</a>");
  }
  
  if(isset($_SESSION['email']))
  {
    ?>
<nav aria-label="breadcrumb">
      <ol class="breadcrumb">
        <li class="breadcrumb-item active" aria-current="page">User Profile</li>
      </ol>
  </nav>
<div class="row">
    <!-- Profile Image -->
    <div class="card card-primary card-outline col-md-4 mr-4 ml-2">
      <div class="card-body box-profile">
        <div class="text-center">
          <?php

          $profile_pic = mysqli_fetch_array(mysqli_query($dbc,"SELECT * FROM staff_users WHERE Email ='".$_SESSION['email']."'"));
          if(isset($_SESSION['profile_picture']))
          {
            ?>
            <img class="profile-user-img img-fluid img-circle"
                 src="data:image/jpeg;base64,<?php echo base64_encode($_SESSION['profile_picture']); ?>"
                 alt="User profile picture">
            <?php
          }
          else
          {
            ?>
            <img class="profile-user-img img-fluid img-circle"
                 src="assets/img/<?php echo $profile_pic['emp_photo']; ?>"
                 alt="User profile picture">
            <?php
          }


           ?>

        </div>

        <h3 class="profile-username text-center"><?php echo $_SESSION['name'] ;?><br/><small class="text-muted"><?php echo $_SESSION['email'] ;?></small></h3>


        <p class="text-muted text-center"><?php echo $_SESSION['designation'] ;?></p>

        <ul class="list-group list-group-unbordered mb-3">
          <li class="list-group-item">
            <b>Employee No:</b> <a class="float-right"><?php echo $_SESSION['staff_id'] ;?></a>
          </li>
          <li class="list-group-item">
            <b>Department:</b> <a class="float-right"><?php echo $_SESSION['department'] ;?> | <?php echo $_SESSION['department_code'] ;?></a>
          </li>
          <li class="list-group-item">
            <b>Access Level:</b> <a class="float-right"><?php echo $_SESSION['access_level'] ;?></a>
          </li>
        </ul>

        <!--<a href="#" class="btn btn-primary btn-block"><b>Follow</b></a>-->
      </div>
      <!-- /.card-body -->
    </div>
    <!-- /.card -->

    <div class="col-md-7">
      <div class="card">
        <div class="card-header p-2">
          <ul class="nav nav-tabs">
            <li class="nav-item my-activity-logs-link"><a class="nav-link" href="#timeline" data-toggle="tab"><i class="fad fa-history"></i> My Activity Logs</a></li>
            <li class="nav-item my-recent-navigations-link"><a class="nav-link" href="#navigation" data-toggle="tab"><i class="fad fa-route"></i> My Recent Navigations</a></li>
            <li class="nav-item my-login-history-link"><a class="nav-link" href="#access-logs" data-toggle="tab"><i class="fad fa-history"></i> My Login History</a></li>
          </ul>
        </div><!-- /.card-header -->
        <div class="card-body">
          <div class="tab-content">
            <div class="tab-pane show active" id="timeline">

          </div>
            <!-- /.tab-pane -->

            <div class="tab-pane" id="navigation">

            </div>


            <div class="tab-pane" id="access-logs">

            </div>
          </div>
          <!-- /.tab-content -->
        </div><!-- /.card-body -->
      </div>
      <!-- /.nav-tabs-custom -->
    </div>
    <!-- /.col -->

<!-- /.card -->


</div>
    <?php
  }
  else
  {
    echo "unauthorised";
  }
}
else
{
  echo "form not submitted";
}


 ?>
